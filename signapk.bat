@SET NEED_ADB_RUN=""
@SET NEED_PAUSE_COMMAND_LINE=1

@if "%1" neq "" @set NEED_PAUSE_COMMAND_LINE=0
@if "%2" neq "" @set NEED_PAUSE_COMMAND_LINE=0

@if "%1" == "run" (
	@set NEED_ADB_RUN="true"
	@SET APK_BASE_NAME=%2
) else (
	@SET APK_BASE_NAME=%1
)

@SET APK_DEFAULT_BUILD_DIR= app/build/outputs/apk/release

@if "%APK_BASE_NAME%" == "" @set APK_BASE_NAME=RiftOneDTV

java -jar signapk.jar platform.x509.pem platform.pk8 %APK_DEFAULT_BUILD_DIR%/%APK_BASE_NAME%.apk %APK_BASE_NAME%-signed.apk

@if %NEED_ADB_RUN% == "true" (
	adb install -r -d %APK_BASE_NAME%-signed.apk

	TIMEOUT /T 1 /NOBREAK

	adb shell am start  com.riftone.riftonekhadas/com.riftone.riftonekhadas.ui.SplashActivity
)

@if %NEED_PAUSE_COMMAND_LINE% == 1 (
	@TIMEOUT /T 3 /NOBREAK
)

