package com.riftone.riftonekhadas.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riftone.riftonekhadas.R;

import java.util.ArrayList;

public class CustomListAdapter2 extends BaseAdapter {
    private Context context; //context
    private ArrayList<Integer> items; //data source of the list adapter

    //public constructor
    public CustomListAdapter2(Context context, ArrayList<Integer> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Integer getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.adapter_view_layout, parent, false);
        }

        // get current item to be displayed
        //Item currentItem = (ClipData.Item) getItem(position);

        // get the TextView for item name and item description
        TextView textViewItemName = (TextView)
                convertView.findViewById(R.id.textOp);
        ImageView imageView = convertView.findViewById(R.id.image_view_list);
        //TextView textViewItemDescription = (TextView)
        //convertView.findViewById(R.id.textOp);

        //sets the text for item name and item description from the current item object
        Animation fadeOut = new AlphaAnimation(1, 0);
        imageView.setImageResource(getItem(position));
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(50);
        fadeOut.setDuration(100);
        textViewItemName.setAnimation(fadeOut);
        //imageView.setAnimation(fadeOut);
        //textViewItemDescription.setText(currentItem.getItemDescription());

        // returns the view for the current row
        return convertView;
    }
}
