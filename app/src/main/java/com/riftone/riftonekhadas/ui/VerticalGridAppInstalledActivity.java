package com.riftone.riftonekhadas.ui;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.riftone.riftonekhadas.R;

public class VerticalGridAppInstalledActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_grid_app_installed);
    }
}
