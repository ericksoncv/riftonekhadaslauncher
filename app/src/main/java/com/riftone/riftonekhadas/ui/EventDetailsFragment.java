package com.riftone.riftonekhadas.ui;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.leanback.app.DetailsFragment;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.DetailsOverviewRow;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.ListRowPresenter;
import androidx.leanback.widget.OnActionClickedListener;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.SparseArrayObjectAdapter;

import com.riftone.riftonekhadas.common.Utils;
import com.riftone.riftonekhadas.data.home.EventProvider;
import com.riftone.riftonekhadas.model.Event;
import com.riftone.riftonekhadas.ui.background.PicassoBackgroundManager;
import com.riftone.riftonekhadas.ui.presenter.CustomDetailsOverviewRowPresenter;
import com.riftone.riftonekhadas.ui.presenter.EventDetailsDescriptionPresenter;
import com.riftone.riftonekhadas.ui.presenter.EventPresenter;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

public class EventDetailsFragment extends DetailsFragment {
    private static final String TAG = VideoDetailsFragment.class.getSimpleName();

    private static final int DETAIL_THUMB_WIDTH = 250;
    private static final int DETAIL_THUMB_HEIGHT = 290;


    private static final String EVENT = "Event";

    private static final int ACTION_PLAY_VIDEO = 0;

    private CustomDetailsOverviewRowPresenter mDorPresenter;
    private PicassoBackgroundManager mPicassoBackgroundManager;

    private Event mSelectedEvent;
    private DetailsRowBuilderTask mDetailsRowBuilderTask;

    /* Attribute */
    private ArrayObjectAdapter mAdapter;
    private ClassPresenterSelector mClassPresenterSelector;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        mDorPresenter = new CustomDetailsOverviewRowPresenter(new EventDetailsDescriptionPresenter(), getActivity());

        mPicassoBackgroundManager = new PicassoBackgroundManager(getActivity());
        mSelectedEvent = (Event) getActivity().getIntent().getSerializableExtra(EVENT);


        mDetailsRowBuilderTask = (DetailsRowBuilderTask) new DetailsRowBuilderTask().execute(mSelectedEvent);

        setOnItemViewClickedListener(new ItemViewClickedListener());

        mPicassoBackgroundManager.updateBackgroundWithDelay(mSelectedEvent.getCoverUrl());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mClassPresenterSelector = new ClassPresenterSelector();
        mClassPresenterSelector.addClassPresenter(DetailsOverviewRow.class, mDorPresenter);

        mClassPresenterSelector.addClassPresenter(ListRow.class, new ListRowPresenter());

        mAdapter = new ArrayObjectAdapter(mClassPresenterSelector);
        setAdapter(mAdapter);
    }

    @Override
    public void onStop() {
        mDetailsRowBuilderTask.cancel(true);
        super.onStop();
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Event) {

            }
        }
    }

    private class DetailsRowBuilderTask extends AsyncTask<Event, Integer, DetailsOverviewRow> {
        @Override
        protected DetailsOverviewRow doInBackground(Event... params) {
            Log.v(TAG, "DetailsRowBuilderTask doInBackground");
            int width, height;
            width = DETAIL_THUMB_WIDTH;
            height = DETAIL_THUMB_HEIGHT;

            DetailsOverviewRow row = new DetailsOverviewRow(mSelectedEvent);
            try {
                // Bitmap loading must be done in background thread in Android.
                Bitmap poster = Picasso.with(getActivity())
                        .load(mSelectedEvent.getPosterUrl())
                        .resize(Utils.convertDpToPixel(getActivity().getApplicationContext(), width),
                                Utils.convertDpToPixel(getActivity().getApplicationContext(), height))
                        .centerCrop()
                        .get();
                row.setImageBitmap(getActivity(), poster);
            } catch (IOException e) {
                Log.w(TAG, e.toString());
            }
            return row;
        }

        @Override
        protected void onPostExecute(DetailsOverviewRow row) {
            Log.v(TAG, "DetailsRowBuilderTask onPostExecute");
            /* 1st row: DetailsOverviewRow */

            /* action setting*/
            SparseArrayObjectAdapter sparseArrayObjectAdapter = new SparseArrayObjectAdapter();
            sparseArrayObjectAdapter.set(0, new Action(ACTION_PLAY_VIDEO, "Play Video"));
            sparseArrayObjectAdapter.set(1, new Action(1, "Action 2", "label"));
            sparseArrayObjectAdapter.set(2, new Action(2, "Action 3", "label"));

            row.setActionsAdapter(sparseArrayObjectAdapter);

            mDorPresenter.setOnActionClickedListener(new OnActionClickedListener() {
                @Override
                public void onActionClicked(Action action) {
                    if (action.getId() == ACTION_PLAY_VIDEO) {
                        /*Intent intent = new Intent(getActivity(), PlaybackOverlayActivity.class);
                        intent.putExtra(DetailsActivity.MOVIE, mSelectedMovie);
                        intent.putExtra(getResources().getString(R.string.should_start), true);
                        startActivity(intent);*/
                    }
                }
            });

            /* 2nd row: ListRow */
            ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new EventPresenter());
            ArrayList<Event> mItems = EventProvider.getEventItems();
            for (Event event : mItems) {
                listRowAdapter.add(event);
            }
            HeaderItem headerItem = new HeaderItem(0, "Related Event");


            mAdapter = new ArrayObjectAdapter(mClassPresenterSelector);
            /* 1st row */
            mAdapter.add(row);
            /* 2nd row */
            mAdapter.add(new ListRow(headerItem, listRowAdapter));

            /* 3rd row */
            //adapter.add(new ListRow(headerItem, listRowAdapter));
            setAdapter(mAdapter);
        }
    }

}