package com.riftone.riftonekhadas.ui;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.leanback.app.VerticalGridFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;

import com.riftone.riftonekhadas.common.Utils;
import com.riftone.riftonekhadas.model.App;
import com.riftone.riftonekhadas.ui.presenter.AppPresenter;

import java.util.List;

public class VerticalGridAppInstalledFragment extends VerticalGridFragment {
    private static final String TAG = VerticalGridAppInstalledFragment.class.getSimpleName();
    private static final int NUM_COLUMNS = 10;
    private ArrayObjectAdapter mRowsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        super.onCreate(savedInstanceState);

        setTitle("Apps");
        setupFragment();
        setupEventListeners();
    }

    private void setupFragment() {
        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

        mRowsAdapter = new ArrayObjectAdapter(new AppPresenter());
        PackageManager packageManager = getActivity().getPackageManager();
        List<ApplicationInfo> list = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);

        /* Add movie items */
        for (int i = 0; i < list.size(); i++) {

            // ignora aplicaçoes indisejaveis ... app defaut do sistema
            if (null == packageManager.getLaunchIntentForPackage(list.get(i).packageName)) {
                continue;
            }

            App apps = new App();
            apps.setLabel(list.get(i).loadLabel(packageManager).toString());
            apps.setPackageInstall(list.get(i).packageName);
            apps.setIcon(list.get(i).loadIcon(packageManager));
            mRowsAdapter.add(apps);

        }
        setAdapter(mRowsAdapter);
    }

    private void setupEventListeners() {
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
        setOnItemViewClickedListener(new ItemViewClickedListener());
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            if(item instanceof App){
                App app = (App) item;
                Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(app.getPackageInstall());
                startActivity(launchIntent);
            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                   RowPresenter.ViewHolder rowViewHolder, Row row) {
            // each time the item is selected, code inside here will be executed.
            if (item instanceof App) {                    // GridItemPresenter
                MainActivityWithMenuLeft.picassoBackgroundManager.updateBackgroundWithDelay(Utils.END_POINT +"riftplayapp/webApp/images/defaultBackgroundTV3.jpg");
            }
        }
    }

    Handler someHandler;
    @Override
    public void onDestroy() {
        super.onDestroy();


        if(someHandler!=null)
            someHandler.removeCallbacksAndMessages(null);
        someHandler =null;

    }

    @Override
    public void onPause() {
        super.onPause();

        if(someHandler!=null)
            someHandler.removeCallbacksAndMessages(null);
        someHandler =null;
    }

    @Override
    public void onResume() {
        super.onResume();

        setTitle("Apps");
        setupFragment();
        setupEventListeners();

    }
}
