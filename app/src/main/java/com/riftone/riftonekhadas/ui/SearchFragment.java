package com.riftone.riftonekhadas.ui;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.ListRowPresenter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.SpeechRecognitionCallback;

import com.riftone.riftonekhadas.common.Utils;
import com.riftone.riftonekhadas.data.home.ChannelProvider;
import com.riftone.riftonekhadas.data.home.EventProvider;
import com.riftone.riftonekhadas.data.home.MovieProvider;
import com.riftone.riftonekhadas.data.home.SerieProvider;
import com.riftone.riftonekhadas.data.home.VideoProvider;
import com.riftone.riftonekhadas.model.Channel;
import com.riftone.riftonekhadas.model.Event;
import com.riftone.riftonekhadas.model.Movie;
import com.riftone.riftonekhadas.model.Serie;
import com.riftone.riftonekhadas.model.Video;
import com.riftone.riftonekhadas.ui.presenter.ChannelPresenter;
import com.riftone.riftonekhadas.ui.presenter.EventPresenter;
import com.riftone.riftonekhadas.ui.presenter.MoviePresenter;
import com.riftone.riftonekhadas.ui.presenter.SeriePresenter;
import com.riftone.riftonekhadas.ui.presenter.VideoPresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchFragment extends androidx.leanback.app.SearchFragment implements androidx.leanback.app.SearchFragment.SearchResultProvider {
    private static final String TAG = SearchFragment.class.getSimpleName();

    private static final int REQUEST_SPEECH = 0x00000010;
    private static final long SEARCH_DELAY_MS = 1000L;

    private ArrayObjectAdapter mRowsAdapter;

    private ArrayList<Channel> channelSearchItems = ChannelProvider.getChannelItems();
    private ArrayList<Movie> movieSearchItems = MovieProvider.getMovieItems();
    private ArrayList<Serie> serieSearchItems = SerieProvider.getSerieItems();
    private ArrayList<Event> eventSearchItems = EventProvider.getEventItems();
    private ArrayList<Video> videoSearchItems = VideoProvider.getVideoItems();

    private final Handler mHandler = new Handler();
    private final Runnable mDelayedLoad = new Runnable() {
        @Override
        public void run() {
            loadRowsChannels();
            loadRowsMovies();
            loadRowsSeries();
            loadRowsVideos();
            loadRowsEvents();
        }
    };
    private String mQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());

        setSearchResultProvider(this);

        if (!Utils.hasPermission(getActivity(), Manifest.permission.RECORD_AUDIO)) {
            // SpeechRecognitionCallback is not required and if not provided recognition will be handled
            // using internal speech recognizer, in which case you must have RECORD_AUDIO permission
            setSpeechRecognitionCallback(new SpeechRecognitionCallback() {
                @Override
                public void recognizeSpeech() {
                    Log.v(TAG, "recognizeSpeech");
                    try {
                        startActivityForResult(getRecognizerIntent(), REQUEST_SPEECH);
                    } catch (ActivityNotFoundException e) {
                        Log.e(TAG, "Cannot find activity for speech recognizer", e);
                    }
                }
            });
        }

        setOnItemViewClickedListener(new ItemViewClickedListener());
    }

    public boolean hasResults() {
        return mRowsAdapter.size() > 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v(TAG, "onActivityResult requestCode=" + requestCode +
                " resultCode=" + resultCode +
                " data=" + data);

        switch (requestCode) {
            case REQUEST_SPEECH:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        setSearchQuery(data, true);
                        break;
                }
        }
    }

    @Override
    public ObjectAdapter getResultsAdapter() {
        Log.d(TAG, "getResultsAdapter");
        Log.d(TAG, mRowsAdapter.toString());

        // It should return search result here,
        // but static Movie Item list will be returned here now for practice.
        /*ArrayList<Channel> channelSearchItems = ChannelProvider.getChannelItems();
        ArrayList<Movie> movieSearchItems = MovieProvider.getMovieItems();
        ArrayList<Serie> serieSearchItems = SerieProvider.getSerieItems();
        ArrayList<Event> eventSearchItems = EventProvider.getEventItems();
        ArrayList<Video> videoSearchItems = VideoProvider.getVideoItems();

        ArrayObjectAdapter listChannelRowAdapter = new ArrayObjectAdapter(new ChannelPresenter());
        ArrayObjectAdapter listMovieRowAdapter = new ArrayObjectAdapter(new MoviePresenter());
        ArrayObjectAdapter listSerieRowAdapter = new ArrayObjectAdapter(new SeriePresenter());
        ArrayObjectAdapter listVideoRowAdapter = new ArrayObjectAdapter(new VideoPresenter());
        ArrayObjectAdapter listEventRowAdapter = new ArrayObjectAdapter(new EventPresenter());

        listChannelRowAdapter.addAll(0, channelSearchItems);
        HeaderItem headerSearchChannel = new HeaderItem("Search channels results");
        mRowsAdapter.add(new ListRow(headerSearchChannel, listChannelRowAdapter));

        listMovieRowAdapter.addAll(0, movieSearchItems);
        HeaderItem headerSearchMovie = new HeaderItem("Search movies results");
        mRowsAdapter.add(new ListRow(headerSearchMovie, listMovieRowAdapter));

        listSerieRowAdapter.addAll(0, serieSearchItems);
        HeaderItem headerSearchSerie = new HeaderItem("Search series results");
        mRowsAdapter.add(new ListRow(headerSearchSerie, listSerieRowAdapter));

        listVideoRowAdapter.addAll(0, videoSearchItems);
        HeaderItem headerSearchVideo = new HeaderItem("Search videos results");
        mRowsAdapter.add(new ListRow(headerSearchVideo, listVideoRowAdapter));

        listEventRowAdapter.addAll(0, eventSearchItems);
        HeaderItem headerSearchEvent = new HeaderItem("Search events results");
        mRowsAdapter.add(new ListRow(headerSearchEvent, listEventRowAdapter));*/

        return mRowsAdapter;
    }


    @Override
    public boolean onQueryTextChange(String newQuery){
        Log.i(TAG, String.format("Search Query Text Change %s", newQuery));
        loadQueryWithDelay(newQuery, SEARCH_DELAY_MS);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.i(TAG, String.format("Search Query Text Submit %s", query));
        // No need to delay(wait) loadQuery, since the query typing has completed.
        loadQueryWithDelay(query, 0);
        return true;
    }

    /**
     * Starts  method after delay.
     * @param query the word to be searched
     * @param delay the time to wait until loadRows will be executed (milliseconds).
     */
    private void loadQueryWithDelay(String query, long delay) {
        mHandler.removeCallbacks(mDelayedLoad);
        if (!TextUtils.isEmpty(query) && !query.equals("nil")) {
            mQuery = query;
            mHandler.postDelayed(mDelayedLoad, delay);
        }
    }


    private void loadRowsChannels() {
        // offload processing from the UI thread
        new AsyncTask<String, Void, ListRow>() {
            private final String query = mQuery;

            @Override
            protected void onPreExecute() {
                mRowsAdapter.clear();
            }

            @Override
            protected ListRow doInBackground(String... params) {
                final List<Channel> result = new ArrayList<>();
                for (Channel channel : channelSearchItems) {
                    // Main logic of search is here.
                    // Just check that "query" is contained in Title or Category or not.
                    if (channel.getName().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))
                            || channel.getCategory().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))) {
                        result.add(channel);
                    }
                }
                ArrayObjectAdapter listChannelRowAdapter = new ArrayObjectAdapter(new ChannelPresenter());
                listChannelRowAdapter.addAll(0, result);
                HeaderItem headerSearchChannel = new HeaderItem("Search channels results");
                return new ListRow(headerSearchChannel, listChannelRowAdapter);
            }

            @Override
            protected void onPostExecute(ListRow listRow) {
                if(listRow.getAdapter().size() > 0){
                    mRowsAdapter.add(listRow);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadRowsMovies() {
        // offload processing from the UI thread
        new AsyncTask<String, Void, ListRow>() {
            private final String query = mQuery;

            @Override
            protected void onPreExecute() {
                mRowsAdapter.clear();
            }

            @Override
            protected ListRow doInBackground(String... params) {
                final List<Movie> result = new ArrayList<>();
                for (Movie movie : movieSearchItems) {
                    // Main logic of search is here.
                    // Just check that "query" is contained in Title or Genres or not.
                    if (movie.getTitle().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))
                            || movie.getGenres().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))) {
                        result.add(movie);
                    }
                }
                ArrayObjectAdapter listMovieRowAdapter = new ArrayObjectAdapter(new MoviePresenter());
                listMovieRowAdapter.addAll(0, result);
                HeaderItem headerSearchMovies = new HeaderItem("Search movies results");
                return new ListRow(headerSearchMovies, listMovieRowAdapter);

            }

            @Override
            protected void onPostExecute(ListRow listRow) {
                Log.d("MovieSearch", String.valueOf(listRow.getAdapter().size()));
                if(listRow.getAdapter().size() > 0){
                    mRowsAdapter.add(listRow);
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadRowsSeries() {
        // offload processing from the UI thread
        new AsyncTask<String, Void, ListRow>() {
            private final String query = mQuery;

            @Override
            protected void onPreExecute() {
                mRowsAdapter.clear();
            }

            @Override
            protected ListRow doInBackground(String... params) {
                final List<Serie> result = new ArrayList<>();
                for (Serie serie : serieSearchItems) {
                    // Main logic of search is here.
                    // Just check that "query" is contained in Title or Genres or not.
                    if (serie.getName().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))
                            || serie.getGenres().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))) {
                        result.add(serie);
                    }
                }
                ArrayObjectAdapter listSerieRowAdapter = new ArrayObjectAdapter(new SeriePresenter());
                listSerieRowAdapter.addAll(0, result);
                HeaderItem headerSearchSerie = new HeaderItem("Search serie results");
                return new ListRow(headerSearchSerie, listSerieRowAdapter);
            }

            @Override
            protected void onPostExecute(ListRow listRow) {
                if(listRow.getAdapter().size() > 0){
                    mRowsAdapter.add(listRow);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadRowsVideos() {
        // offload processing from the UI thread
        new AsyncTask<String, Void, ListRow>() {
            private final String query = mQuery;

            @Override
            protected void onPreExecute() {
                mRowsAdapter.clear();
            }

            @Override
            protected ListRow doInBackground(String... params) {
                final List<Video> result = new ArrayList<>();
                for (Video video : videoSearchItems) {
                    // Main logic of search is here.
                    // Just check that "query" is contained in Title or Category or not.
                    if (video.getTitle().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))
                            || video.getCategory().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))) {
                        result.add(video);
                    }
                }
                ArrayObjectAdapter listVideoRowAdapter = new ArrayObjectAdapter(new VideoPresenter());
                listVideoRowAdapter.addAll(0, result);
                HeaderItem headerSearchVideo = new HeaderItem("Search videos results");
                return new ListRow(headerSearchVideo, listVideoRowAdapter);
            }

            @Override
            protected void onPostExecute(ListRow listRow) {
                if(listRow.getAdapter().size() > 0){
                    mRowsAdapter.add(listRow);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadRowsEvents() {
        // offload processing from the UI thread
        new AsyncTask<String, Void, ListRow>() {
            private final String query = mQuery;

            @Override
            protected void onPreExecute() {
                mRowsAdapter.clear();
            }

            @Override
            protected ListRow doInBackground(String... params) {
                final List<Event> result = new ArrayList<>();
                for (Event event : eventSearchItems) {
                    // Main logic of search is here.
                    // Just check that "query" is contained in Title or Description or not.
                    if (event.getName().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))
                            || event.getCategory().toLowerCase(Locale.ENGLISH)
                            .contains(query.toLowerCase(Locale.ENGLISH))) {
                        result.add(event);
                    }
                }
                ArrayObjectAdapter listEventRowAdapter = new ArrayObjectAdapter(new EventPresenter());
                listEventRowAdapter.addAll(0, result);
                HeaderItem headerSearchEvent = new HeaderItem("Search events results");
                return new ListRow(headerSearchEvent, listEventRowAdapter);
            }

            @Override
            protected void onPostExecute(ListRow listRow) {
                if(listRow.getAdapter().size() > 0){
                    mRowsAdapter.add(listRow);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Channel) {
                Channel channel = (Channel) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), ChannelDetailsActivity.class);
                intent.putExtra(ChannelDetailsActivity.CHANNEL, channel);

                getActivity().startActivity(intent);
            }else if (item instanceof Movie) {
                Movie movie = (Movie) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
                intent.putExtra(MovieDetailsActivity.MOVIE, movie);

                getActivity().startActivity(intent);
            }else if (item instanceof Video) {
                Video video = (Video) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);

                getActivity().startActivity(intent);
            }else if (item instanceof Serie) {
                Serie serie = (Serie) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), SerieDetailsActivity.class);
                intent.putExtra(SerieDetailsActivity.SERIE, serie);

                getActivity().startActivity(intent);
            }else if (item instanceof Event) {
                Event event = (Event) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
                intent.putExtra(EventDetailsActivity.EVENT, event);

                getActivity().startActivity(intent);
            } else {
                Toast.makeText(getActivity(), ((String) item), Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

}
