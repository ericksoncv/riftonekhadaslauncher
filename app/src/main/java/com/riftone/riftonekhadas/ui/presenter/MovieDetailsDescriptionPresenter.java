package com.riftone.riftonekhadas.ui.presenter;

import androidx.leanback.widget.AbstractDetailsDescriptionPresenter;

import com.riftone.riftonekhadas.model.Movie;

public class MovieDetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {
    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Movie movie = (Movie) item;

        if (movie != null) {
            viewHolder.getTitle().setText(movie.getTitle());
            viewHolder.getSubtitle().setText(movie.getGenres());
            viewHolder.getBody().setText(movie.getDescription());
        }
    }
}

