package com.riftone.riftonekhadas.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.leanback.app.BrowseFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.ListRowPresenter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;

import com.riftone.riftonekhadas.R;
import com.riftone.riftonekhadas.common.Utils;
import com.riftone.riftonekhadas.data.home.ChannelProvider;
import com.riftone.riftonekhadas.data.home.EventProvider;
import com.riftone.riftonekhadas.data.home.MovieProvider;
import com.riftone.riftonekhadas.data.home.SerieProvider;
import com.riftone.riftonekhadas.data.home.VideoProvider;
import com.riftone.riftonekhadas.model.Channel;
import com.riftone.riftonekhadas.model.Event;
import com.riftone.riftonekhadas.model.ModelCarView;
import com.riftone.riftonekhadas.model.Movie;
import com.riftone.riftonekhadas.model.Serie;
import com.riftone.riftonekhadas.model.Video;
import com.riftone.riftonekhadas.ui.presenter.ChannelPresenter;
import com.riftone.riftonekhadas.ui.presenter.EventPresenter;
import com.riftone.riftonekhadas.ui.presenter.MoviePresenter;
import com.riftone.riftonekhadas.ui.presenter.SeriePresenter;
import com.riftone.riftonekhadas.ui.presenter.VideoPresenter;
import com.superdtv.DtvMainActivity;
//import com.superdtv.DtvMainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class HomeFragment extends BrowseFragment {
    private static final String TAG = HomeFragment.class.getSimpleName();

    private static final int GRID_ITEM_WIDTH = 240;
    private static final int GRID_ITEM_HEIGHT = 70;
    private static final String GRID_STRING_TV_DIGITAL = "TV Digital";
    private static final String GRID_STRING_PLAYER = "Player";
    private static final String GRID_STRING_APPs = "Play Store";
    private static final String GRID_RIFT_UPDATE = "riftOne Update";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHeadersState(HEADERS_DISABLED);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        setupUIElements();
        setupUiSearch();
        loadRows();

        setupEventListeners();

    }

    Handler someHandler;

    private void setupUIElements() {

        setTitle("Home"); // Badge, when set, takes precedent

        // over title

        setHeadersTransitionOnBackEnabled(true);
        // set fastLane (or headers) background color
        setBrandColor(getResources().getColor(R.color.fastlane_background));
        // set search icon color
        setSearchAffordanceColor(getResources().getColor(R.color.default_background_transparent));
    }

    private void setupUiSearch(){

        setOnSearchClickedListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);

            }
        });

    }

    private ArrayObjectAdapter mRowsAdapter;

    private void loadRows() {

        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());

        GridItemPresenter mGridPresenter = new GridItemPresenter();
        ArrayObjectAdapter gridRowAdapter = new ArrayObjectAdapter(mGridPresenter);
        gridRowAdapter.add(new ModelCarView(GRID_STRING_TV_DIGITAL,R.drawable.tv_digital));
        gridRowAdapter.add(new ModelCarView(GRID_STRING_PLAYER,R.drawable.player_video));
        gridRowAdapter.add(new ModelCarView(GRID_STRING_APPs,R.drawable.play_store));
        gridRowAdapter.add(new ModelCarView(GRID_RIFT_UPDATE,R.drawable.refresh));

        mRowsAdapter.add(new ListRow(null, gridRowAdapter));

        /* ChannelsPresenter */
        HeaderItem channelsPresenterHeader = new HeaderItem(1,"Channels");
        ChannelPresenter channelsPresenter = new ChannelPresenter();
        ArrayObjectAdapter channelsRowAdapter = new ArrayObjectAdapter(channelsPresenter);

        ArrayList<Channel> channelItems = ChannelProvider.getChannelItems();
        for (Channel channel : channelItems) {
            channelsRowAdapter.add(channel);
        }

        mRowsAdapter.add(new ListRow(channelsPresenterHeader, channelsRowAdapter));

        /* MoviesPresenter */
        HeaderItem moviesPresenterHeader = new HeaderItem(2,"Movies");
        MoviePresenter moviesPresenter = new MoviePresenter();
        ArrayObjectAdapter moviesRowAdapter = new ArrayObjectAdapter(moviesPresenter);

        ArrayList<Movie> moviesItems = MovieProvider.getMovieItems();
        for (Movie movie : moviesItems) {
            moviesRowAdapter.add(movie);
        }

        mRowsAdapter.add(new ListRow(moviesPresenterHeader, moviesRowAdapter));

        /* SeriesPresenter */
        HeaderItem seriesPresenterHeader = new HeaderItem(4,"Series");
        SeriePresenter seriesPresenter = new SeriePresenter();
        ArrayObjectAdapter seriesRowAdapter = new ArrayObjectAdapter(seriesPresenter);

        ArrayList<Serie> seriesItems = SerieProvider.getSerieItems();
        for (Serie serie : seriesItems) {
            seriesRowAdapter.add(serie);
        }

        mRowsAdapter.add(new ListRow(seriesPresenterHeader, seriesRowAdapter));

        /* VideosPresenter */
        HeaderItem videosPresenterHeader = new HeaderItem(3,"Others Videos");
        VideoPresenter videosPresenter = new VideoPresenter();
        ArrayObjectAdapter videosRowAdapter = new ArrayObjectAdapter(videosPresenter);

        ArrayList<Video> videoItems = VideoProvider.getVideoItems();
        for (Video video : videoItems) {
            videosRowAdapter.add(video);
        }

        mRowsAdapter.add(new ListRow(videosPresenterHeader, videosRowAdapter));

        /* EventsPresenter */
        HeaderItem eventsPresenterHeader = new HeaderItem(4,"Events");
        EventPresenter eventsPresenter = new EventPresenter();
        ArrayObjectAdapter eventsRowAdapter = new ArrayObjectAdapter(eventsPresenter);

        ArrayList<Event> eventItems = EventProvider.getEventItems();
        for (Event event : eventItems) {
            eventsRowAdapter.add(event);
        }

        mRowsAdapter.add(new ListRow(eventsPresenterHeader, eventsRowAdapter));


        /* set */
        setAdapter(mRowsAdapter);
    }

    private void setupEventListeners() {
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
        setOnItemViewClickedListener(new ItemViewClickedListener());
    }
    //envia para proxima tela
    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            // each time the item is clicked, code inside here will be executed.
            if (item instanceof ModelCarView) {

                ModelCarView modelCarView = (ModelCarView) item;

                if (modelCarView.getTextCard() == GRID_STRING_TV_DIGITAL) {
                    /*Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.superdtv");
                    startActivity(launchIntent);**/
                    Intent intent=new Intent(getActivity(), DtvMainActivity.class);
                    startActivity(intent);
                } else if (modelCarView.getTextCard() == GRID_STRING_PLAYER) {
                    Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.droidlogic.videoplayer");
                    startActivity(launchIntent);
                } else if (modelCarView.getTextCard() == GRID_STRING_APPs) {
                    Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.android.vending");
                    startActivity(launchIntent);
                }else if (modelCarView.getTextCard() == GRID_RIFT_UPDATE) {
                    //Intent intent=new Intent(getContext(),UpdateRiftone.class);
                    //startActivity(intent);
                }
            }else if (item instanceof Channel) {
                Channel channel = (Channel) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), ChannelDetailsActivity.class);
                intent.putExtra(ChannelDetailsActivity.CHANNEL, channel);

                getActivity().startActivity(intent);
            }else if (item instanceof Movie) {
                Movie movie = (Movie) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
                intent.putExtra(MovieDetailsActivity.MOVIE, movie);

                getActivity().startActivity(intent);
            }else if (item instanceof Video) {
                Video video = (Video) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);

                getActivity().startActivity(intent);
            }else if (item instanceof Serie) {
                Serie serie = (Serie) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), SerieDetailsActivity.class);
                intent.putExtra(SerieDetailsActivity.SERIE, serie);

                getActivity().startActivity(intent);
            }else if (item instanceof Event) {
                Event event = (Event) item;
                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
                intent.putExtra(EventDetailsActivity.EVENT, event);

                getActivity().startActivity(intent);
            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                   RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof String) {                    // GridItemPresenter
                MainActivityWithMenuLeft.picassoBackgroundManager.updateBackgroundWithDelay(Utils.END_POINT +"riftplayapp/webApp/images/defaultBackgroundTV3.jpg");
            }else{
                MainActivityWithMenuLeft.picassoBackgroundManager.updateBackgroundWithDelay(Utils.END_POINT +"riftplayapp/webApp/images/defaultBackgroundTV3.jpg");
            }
        }
    }

    private void initTimer(){
        someHandler = new Handler();
        someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                someHandler.postDelayed(this, 1000);
                setTitle(new SimpleDateFormat("HH:mm", Locale.US).format(new Date()));
            }
        }, 10);

    }

    private class GridItemPresenter extends Presenter {
        private int mSelectedBackgroundColor = -1;//R.color.selected;
        private int mDefaultBackgroundColor = -1;//R.color.default_background;
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            mDefaultBackgroundColor =
                    ContextCompat.getColor(parent.getContext(), R.color.default_background);
            mSelectedBackgroundColor =
                    ContextCompat.getColor(parent.getContext(), R.color.selected);

            MyCardView cardView = new MyCardView(parent.getContext()) {
                @Override
                public void setSelected(boolean selected) {
                    updateCardBackgroundColor(this, selected);
                    super.setSelected(selected);
                }
            };

            cardView.setFocusable(true);
            cardView.setFocusableInTouchMode(true);
            updateCardBackgroundColor(cardView, false);
            return new ViewHolder(cardView);
        }

        private void updateCardBackgroundColor(MyCardView view, boolean selected) {
            int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;

            // Both background colors should be set because the view's
            // background is temporarily visible during animations.
            view.setBackgroundColor(color);
            view.findViewById(R.id.card_view).setBackgroundColor(color);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            ModelCarView model= (ModelCarView) item; // assumes you have a class named MyCardModel with corresponding properties for the view
            MyCardView cardView = (MyCardView ) viewHolder.view;

            cardView.setMainImage(model.getImageCard());
            cardView.setTitleText(model.getTextCard());
        }

        @Override
        public void onUnbindViewHolder(ViewHolder viewHolder) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


        if(someHandler!=null)
            someHandler.removeCallbacksAndMessages(null);
        someHandler =null;

    }

    @Override
    public void onPause() {
        super.onPause();

        if(someHandler!=null)
            someHandler.removeCallbacksAndMessages(null);
        someHandler =null;
    }

    @Override
    public void onResume() {
        super.onResume();

        initTimer();
    }
}
