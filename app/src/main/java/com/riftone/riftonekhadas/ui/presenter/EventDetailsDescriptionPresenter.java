package com.riftone.riftonekhadas.ui.presenter;

import androidx.leanback.widget.AbstractDetailsDescriptionPresenter;

import com.riftone.riftonekhadas.model.Event;

public class EventDetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Event event = (Event) item;

        if (event != null) {
            viewHolder.getTitle().setText(event.getName());
            viewHolder.getSubtitle().setText(event.getCategory());
            viewHolder.getBody().setText(event.getDescription());
        }

    }
}