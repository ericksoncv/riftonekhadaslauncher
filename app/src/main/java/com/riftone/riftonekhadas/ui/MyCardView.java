package com.riftone.riftonekhadas.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.BaseCardView;

import com.riftone.riftonekhadas.R;

public class MyCardView extends BaseCardView {
    private ImageView _logoImageView;
    private TextView _titleView;

    public MyCardView(Context context) {
        super(context);
        buildCardView();
    }

    protected void buildCardView() {
        // Make sure this view is clickable and focusable
        setClickable(true);
        setFocusable(true);
        setFocusableInTouchMode(true);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.card_view, this);

        _logoImageView = (ImageView) findViewById(R.id.logo_image);
        _titleView = (TextView) findViewById(R.id.title_text);
    }

    /**
     * Sets the image drawable.
     */
    public void setMainImage(int drawable) {
        _logoImageView.setImageResource(drawable);
    }

    /**
     * Sets the logo image drawable
     */
    public void setLogoImage(Drawable drawable) {
        _logoImageView.setImageDrawable(drawable);
    }

    /**
     * Sets the title text.
     */
    public void setTitleText(CharSequence text) {
        if (_titleView == null) {
            return;
        }
        _titleView.setText(text);
    }
}
