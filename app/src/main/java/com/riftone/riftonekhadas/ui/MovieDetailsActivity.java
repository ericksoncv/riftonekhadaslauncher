package com.riftone.riftonekhadas.ui;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.riftone.riftonekhadas.R;

public class MovieDetailsActivity extends FragmentActivity {

    public static final String MOVIE = "Movie";
    public static final String SHARED_ELEMENT_NAME = "hero";
    public static final String NOTIFICATION_ID = "ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
    }
}
