package com.riftone.riftonekhadas.ui.presenter;

import androidx.leanback.widget.AbstractDetailsDescriptionPresenter;

import com.riftone.riftonekhadas.model.Video;

public class VideoDetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Video video = (Video) item;

        if (video != null) {
            viewHolder.getTitle().setText(video.getTitle());
            viewHolder.getSubtitle().setText(video.getCategory());
            viewHolder.getBody().setText(video.getDescription());
        }

    }
}
