package com.riftone.riftonekhadas.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.riftone.riftonekhadas.R;

public class SplashActivity extends LeanbackActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Log.d(TAG,"Splash Created");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
                Intent mainIntent;
                if(!sharedPreferences.getBoolean(OnboardingFragment.COMPLETED_ONBOARDING, false)) {
                    // This is the first time running the app, let's go to onboarding
                    mainIntent = new Intent(SplashActivity.this, OnboardingActivity.class);
                }else{
                    /* Create an Intent that will start the Main-Activity-With-MenuLeft.*/
                    mainIntent = new Intent(SplashActivity.this, MainActivityWithMenuLeft.class);
                }
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, 3000);
    }
}
