package com.riftone.riftonekhadas.ui;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.riftone.riftonekhadas.R;

public class VideoDetailsActivity extends FragmentActivity {

    public static final String VIDEO = "Video";
    public static final String SHARED_ELEMENT_NAME = "hero";
    public static final String NOTIFICATION_ID = "ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
    }
}
