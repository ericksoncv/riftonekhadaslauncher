package com.riftone.riftonekhadas.ui.presenter;

import androidx.leanback.widget.AbstractDetailsDescriptionPresenter;

import com.riftone.riftonekhadas.model.Serie;

public class SerieDetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Serie serie = (Serie) item;

        if (serie != null) {
            viewHolder.getTitle().setText(serie.getName());
            viewHolder.getSubtitle().setText(serie.getGenres());
            viewHolder.getBody().setText(serie.getDescription());
        }

    }
}

