package com.riftone.riftonekhadas.ui;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.riftone.riftonekhadas.R;
import com.riftone.riftonekhadas.common.CustomListAdapter;
import com.riftone.riftonekhadas.common.CustomListAdapter2;
import com.riftone.riftonekhadas.common.ResizeWidthAnimation;
import com.riftone.riftonekhadas.ui.background.PicassoBackgroundManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class MainActivityWithMenuLeft extends FragmentActivity {
    public static PicassoBackgroundManager picassoBackgroundManager = null;

    private ArrayList<String> arrayOpText = new ArrayList<>();
    private ArrayList<Integer> arrayImgOp = new ArrayList<>();

    private ListView listView;
    private ImageView logoTopMenu;
    private WebView advertisingPoster;
    private TextView OfflineTxtLabel;
    private LinearLayout leftFrame;
    private AppCompatImageView fragImag;

    private final int LEFT_FRAGMENT_WIDTH_PXMAX = 340;
    private final int LEFT_FRAGMENT_WIDTH_PXMIN = 100;
    private final String OPTION_HOME = "Home";
    private final String OPTION_CHANNELS = "Channels";
    private final String OPTION_MOVIES = "Movies";
    private final String OPTION_SERIES = "Series";
    private final String OPTION_OTHERS = "Others";
    private final String OPTION_EVENTS = "Events";
    private final String OPTION_MY_SUBSCRIPTION = "My Subscription";
    private final String OPTION_APPs = "APPs";
    private final String OPTION_SETTINGS = "Settings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_with_menu_left);

        setUpMainActivity();

        mainMenuNavigation();


    }

    private void setUpMainActivity(){
        leftFrame = findViewById(R.id.menuLateral);
        logoTopMenu = findViewById(R.id.logoPage);
        OfflineTxtLabel = findViewById(R.id.isOfflineText);
        listView = findViewById(R.id.OpListView);
        fragImag = findViewById(R.id.imagemDeFundo);
        //advertisingPoster = findViewById(R.id.publicidade);

        if(picassoBackgroundManager==null) {
            picassoBackgroundManager = new PicassoBackgroundManager(this);
        }

        //advertisingPoster.setVisibility(View.INVISIBLE);
        String html = "<iframe id='a68ad42b' name='a68ad42b' src='http://ads.riftplay.tv/adsriftplay/www/delivery/afr.php?resize=1&amp;refresh=10&amp;zoneid=5&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='350' height='490' allowtransparency='true' allow='autoplay'><a href='http://ads.riftplay.tv/adsriftplay/www/delivery/ck.php?n=a269b6b3&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://ads.riftplay.tv/adsriftplay/www/delivery/avw.php?zoneid=5&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a269b6b3' border='0' alt='' /></a></iframe>";
        //advertisingPoster.getSettings().setJavaScriptEnabled(true);
        //advertisingPoster.loadData(html,"text/html",null);

        arrayOpText.add(OPTION_HOME);
        arrayImgOp.add(R.drawable.home);
        OfflineTxtLabel.setVisibility(View.INVISIBLE);
        arrayOpText.add(OPTION_CHANNELS);
        arrayImgOp.add(R.drawable.tv);
        arrayOpText.add(OPTION_MOVIES);
        arrayImgOp.add(R.drawable.cinema);
        arrayOpText.add(OPTION_SERIES);
        arrayImgOp.add(R.drawable.video_player);
        arrayOpText.add(OPTION_OTHERS);
        arrayImgOp.add(R.drawable.video);
        arrayOpText.add(OPTION_EVENTS);
        arrayImgOp.add(R.drawable.events);
        arrayOpText.add(OPTION_MY_SUBSCRIPTION);
        arrayImgOp.add(R.drawable.notification);
        arrayOpText.add(OPTION_APPs);
        arrayImgOp.add(R.drawable.apk);
        arrayOpText.add(OPTION_SETTINGS);
        arrayImgOp.add(R.drawable.settings);
    }

    private void mainMenuNavigation(){
        listView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // got focus logic
                    ResizeWidthAnimation anim = new ResizeWidthAnimation(leftFrame, LEFT_FRAGMENT_WIDTH_PXMAX);
                    anim.setDuration(700);
                    leftFrame.startAnimation(anim);
                    logoTopMenu.setBackgroundResource(R.drawable.logorift);
                    listView.setAdapter(new CustomListAdapter(getBaseContext(),arrayImgOp,arrayOpText));
                }else {
                    ResizeWidthAnimation anim = new ResizeWidthAnimation(leftFrame, LEFT_FRAGMENT_WIDTH_PXMIN);
                    anim.setDuration(700);
                    leftFrame.startAnimation(anim);
                    logoTopMenu.setBackgroundResource(R.drawable.logo_o_rift);
                    listView.setAdapter(new CustomListAdapter2(getBaseContext(),arrayImgOp));
                }
            }
        });

        HomeFragment defaultContainer = new HomeFragment();

        loadFragment(defaultContainer);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //advertisingPoster.setVisibility(View.INVISIBLE);

                switch(arrayOpText.get(position)){

                    case OPTION_HOME:

                        //advertisingPoster.setVisibility(View.VISIBLE);
                        HomeFragment homeFragment = new HomeFragment();

                        loadFragment(homeFragment);

                        break;
                    case OPTION_CHANNELS:

                        /*ChannelsFragment contentContainer = new ChannelsFragment();

                        loadFragment(contentContainer);*/

                        break;
                    case OPTION_MOVIES:

                        /*MoviesFragment contentContainer = new MoviesFragment();

                        loadFragment(contentContainer);*/


                        break;
                    case OPTION_SERIES:

                        /*SeriesFragment contentContainer = new SeriesFragment();

                        loadFragment(contentContainer);*/

                        break;
                    case OPTION_EVENTS:

                        /*EventsFragment contentContainer = new EventsFragment();

                        loadFragment(contentContainer);*/

                        break;
                    case OPTION_OTHERS:

                        /*OthersVideoFragment contentContainer = new OthersVideoFragment();

                        loadFragment(contentContainer);*/

                        break;
                    case OPTION_MY_SUBSCRIPTION:

                        /*MySubscriptionFragment contentContainer = new MySubscriptionFragment();

                        loadFragment(contentContainer);*/

                        break;
                    case OPTION_APPs:

                        VerticalGridAppInstalledFragment verticalGridAppInstalledFragment = new VerticalGridAppInstalledFragment();

                        loadFragment(verticalGridAppInstalledFragment);

                        break;
                    case OPTION_SETTINGS:

                        Intent dialogIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);

                        break;
                }
            }
        });
    }

    private void loadFragment(Object fragment){

        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.conteudo_mostrado, (Fragment) fragment);

        transaction.commit();
    }

    @Override
    public boolean onSearchRequested() {
        startActivity(new Intent(this, SearchActivity.class));
        return true;
    }
}
