package com.riftone.riftonekhadas.data.home;

import com.riftone.riftonekhadas.model.Channel;
import com.riftone.riftonekhadas.model.Event;

import java.util.ArrayList;

public class EventProvider {
    private static final String TAG = EventProvider.class.getSimpleName();

    private static ArrayList<Event> mItems = null;

    public static ArrayList<Event> getEventItems(){
        if(mItems == null){
            mItems = new ArrayList<Event>();
            Channel channel = new Channel();
            channel.setId(100);
            channel.setName("GreenSport");
            channel.setDescription("description1");
            channel.setBrandImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/12308511_416095861916695_5747840403381292548_n.png?_nc_cat=106&_nc_sid=85a577&_nc_eui2=AeF3-jWufKdxcQEY5JsQs2lXwv554oenmVbC_nnih6eZVj3HXnFJ5gqVRANf_NvH2NFDiYjVHH7l9sqbAttZcTuu&_nc_ohc=PfWT6Bkgri0AX8Bp2-W&_nc_ht=scontent.frai1-1.fna&oh=bb6840e1f096e976ca805caa3d6f10b8&oe=5EF86E97");
            channel.setBackImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/47067050_913740945485515_1703652920316133376_n.jpg?_nc_cat=109&_nc_sid=dd9801&_nc_eui2=AeGyd6eqU3vbm3sGpeZF_gHl2fu-ydriy_rZ-77J2uLL-vX6sQ7BUbbZN1vKoq8Qiw-eiYW6NYI70nxX9ju_gPqk&_nc_ohc=48FnKAUomOgAX-AUKgY&_nc_ht=scontent.frai1-1.fna&oh=03ed8aead67e3b1fd09c842d5b2f5deb&oe=5EF99DAA");
            channel.setSlogan("Slogan1");
            channel.setType("F");
            channel.setLanguage("Portugues");
            channel.setCategory("Cat1,Cat2,Cat3");
            channel.setSrcVideoTeaser("Local");
            channel.setVideoTeaserUrl("http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4");
            channel.setSrc24H("External");
            channel.setUrl24H("https://link.24h.streaming");
            channel.setOwner(1);
            channel.setSubscribed(false);

            Event event1 = new Event();
            event1.setId(101);
            event1.setName("Event 1");
            event1.setDescription("Description 1");
            event1.setPosterUrl("https://thumbs.dreamstime.com/z/jazz-music-festival-lettering-silhouette-poster-hall-advertisement-bill-black-musician-abstract-vector-illustration-72397674.jpg");
            event1.setCoverUrl("https://s.studiobinder.com/wp-content/uploads/2017/12/How-to-Make-a-Movie-Poster-Free-Movie-Poster-Credits-Template-1.jpg");
            event1.setTeaserSrc("Yt");
            event1.setTeaserUrl("vUOgaJ-F-Sg");
            event1.setTypeEvent("Live");
            event1.setCountry("Cape Verde");
            event1.setCity("Praia");
            event1.setLanguage("F");
            event1.setTimeZone("GMT -1");
            event1.setStartDate("20/10/2020");
            event1.setEndDate("20/10/2020");
            event1.setFreeSubAcess(1);
            event1.setLimiteUser(1000);
            event1.setPrice(10F);
            event1.setCurrency("USD");
            event1.setCategory("Cat1,Cat2,Cat3");
            event1.setChannel(channel);

            mItems.add(event1);

            Event event2 = new Event();
            event2.setId(101);
            event2.setName("Event 2");
            event2.setDescription("Description 2");
            event2.setPosterUrl("https://st2.depositphotos.com/1561359/10933/v/950/depositphotos_109336472-stock-illustration-music-festival-poster-design.jpg");
            event2.setCoverUrl("https://s.studiobinder.com/wp-content/uploads/2017/12/How-to-Make-a-Movie-Poster-Free-Movie-Poster-Credits-Template-1.jpg");
            event2.setTeaserSrc("Yt");
            event2.setTeaserUrl("vUOgaJ-F-Sg");
            event2.setTypeEvent("Live");
            event2.setCountry("Cape Verde");
            event2.setCity("Praia");
            event2.setLanguage("F");
            event2.setTimeZone("GMT -1");
            event2.setStartDate("20/10/2020");
            event2.setEndDate("20/10/2020");
            event2.setFreeSubAcess(1);
            event2.setLimiteUser(1000);
            event2.setPrice(10F);
            event2.setCurrency("USD");
            event2.setCategory("Cat1,Cat2,Cat3");
            event2.setChannel(channel);

            mItems.add(event2);

            Event event3 = new Event();
            event3.setId(101);
            event3.setName("Event 3");
            event3.setDescription("Description 3");
            event3.setPosterUrl("https://thumbs.dreamstime.com/z/template-rock-concert-poster-vector-flyer-113256089.jpg");
            event3.setCoverUrl("https://s.studiobinder.com/wp-content/uploads/2017/12/How-to-Make-a-Movie-Poster-Free-Movie-Poster-Credits-Template-1.jpg");
            event3.setTeaserSrc("Yt");
            event3.setTeaserUrl("vUOgaJ-F-Sg");
            event3.setTypeEvent("Live");
            event3.setCountry("Cape Verde");
            event3.setCity("Praia");
            event3.setLanguage("F");
            event3.setTimeZone("GMT -1");
            event3.setStartDate("20/10/2020");
            event3.setEndDate("20/10/2020");
            event3.setFreeSubAcess(1);
            event3.setLimiteUser(1000);
            event3.setPrice(10F);
            event3.setCurrency("USD");
            event3.setCategory("Cat1,Cat2,Cat3");
            event3.setChannel(channel);

            mItems.add(event3);
        }
        return mItems;
    }
}
