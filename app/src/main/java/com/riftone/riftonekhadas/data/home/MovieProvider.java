package com.riftone.riftonekhadas.data.home;

import com.riftone.riftonekhadas.model.Channel;
import com.riftone.riftonekhadas.model.Movie;

import java.util.ArrayList;

public class MovieProvider {
    private static final String TAG = ChannelProvider.class.getSimpleName();

    private static ArrayList<Movie> mItems = null;

    public static ArrayList<Movie> getMovieItems(){
        if(mItems == null){
            mItems = new ArrayList<Movie>();
            Channel channel = new Channel();
            channel.setId(100);
            channel.setName("GreenSport");
            channel.setDescription("description1");
            channel.setBrandImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/12308511_416095861916695_5747840403381292548_n.png?_nc_cat=106&_nc_sid=85a577&_nc_eui2=AeF3-jWufKdxcQEY5JsQs2lXwv554oenmVbC_nnih6eZVj3HXnFJ5gqVRANf_NvH2NFDiYjVHH7l9sqbAttZcTuu&_nc_ohc=PfWT6Bkgri0AX8Bp2-W&_nc_ht=scontent.frai1-1.fna&oh=bb6840e1f096e976ca805caa3d6f10b8&oe=5EF86E97");
            channel.setBackImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/47067050_913740945485515_1703652920316133376_n.jpg?_nc_cat=109&_nc_sid=dd9801&_nc_eui2=AeGyd6eqU3vbm3sGpeZF_gHl2fu-ydriy_rZ-77J2uLL-vX6sQ7BUbbZN1vKoq8Qiw-eiYW6NYI70nxX9ju_gPqk&_nc_ohc=48FnKAUomOgAX-AUKgY&_nc_ht=scontent.frai1-1.fna&oh=03ed8aead67e3b1fd09c842d5b2f5deb&oe=5EF99DAA");
            channel.setSlogan("Slogan1");
            channel.setType("F");
            channel.setLanguage("Portugues");
            channel.setCategory("Cat1,Cat2,Cat3");
            channel.setSrcVideoTeaser("Local");
            channel.setVideoTeaserUrl("http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4");
            channel.setSrc24H("External");
            channel.setUrl24H("https://link.24h.streaming");
            channel.setOwner(1);
            channel.setSubscribed(false);

            Movie movie1 = new Movie();
            movie1.setId(101);
            movie1.setTitle("Big Buck Bunny");
            movie1.setPoster("https://upload.wikimedia.org/wikipedia/commons/c/c5/Big_buck_bunny_poster_big.jpg");
            movie1.setCover("https://slotuniverses.com/wp-content/uploads/sites/12030/upload_fed1091b34dcf8203c0729c4faa62315.png");
            movie1.setTeaser_src("Yt");
            movie1.setTeaser_url("VWB4D8UOtsU");
            movie1.setDescription("Description 1");
            movie1.setGenres("Genre1,Genre2,Genre3");
            movie1.setCountry("Nederland");
            movie1.setLanguage("English");
            movie1.setType_upload("Local");
            movie1.setUrl_video("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
            movie1.setTypeOffer("F");
            movie1.setPrice(50F);
            movie1.setFree_sub_access(1);
            movie1.setPublish_date("23/06/2018");
            movie1.setCanSee(true);
            movie1.setChannel(channel);

            mItems.add(movie1);

            Movie movie2 = new Movie();
            movie2.setId(102);
            movie2.setTitle("Elephant Dream");
            movie2.setPoster("https://images-na.ssl-images-amazon.com/images/I/51VFM8DK1CL._SY445_.jpg");
            movie2.setCover("https://vhx.imgix.net/cribbtv/assets/0a351d2b-2fb4-4706-b2fe-f74ac3706cba/Elephants%20Dream%20Artwork.jpg?auto=format%2Ccompress&fit=crop&h=720&w=1280");
            movie2.setTeaser_src("Yt");
            movie2.setTeaser_url("kPdv44HtEoA");
            movie2.setDescription("The first Blender Open Movie from 2006");
            movie2.setGenres("Genre1,Genre2,Genre3");
            movie2.setCountry("Nederland");
            movie2.setLanguage("English");
            movie2.setType_upload("Local");
            movie2.setUrl_video("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4");
            movie2.setTypeOffer("F");
            movie2.setPrice(50F);
            movie2.setFree_sub_access(1);
            movie2.setPublish_date("22/04/2006");
            movie2.setCanSee(true);
            movie2.setChannel(channel);

            mItems.add(movie2);

            Movie movie3 = new Movie();
            movie3.setId(103);
            movie3.setTitle("Tears Of Steel");
            movie3.setPoster("https://images.justwatch.com/poster/11532044/s592");
            movie3.setCover("https://content.jwplatform.com/v2/media/2bzJyM27/poster.jpg?width=720");
            movie3.setTeaser_src("Yt");
            movie3.setTeaser_url("WwHux5QZfC8");
            movie3.setDescription("Tears of Steel -- project Mango -- is Blender Foundation's fourth short film project, with as sole");
            movie3.setGenres("Genre1,Genre2,Genre3");
            movie3.setCountry("United Kingdom");
            movie3.setLanguage("English");
            movie3.setType_upload("Local");
            movie3.setUrl_video("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4");
            movie3.setTypeOffer("P");
            movie3.setPrice(50F);
            movie3.setFree_sub_access(1);
            movie3.setPublish_date("13/07/2012");
            movie3.setCanSee(false);
            movie3.setChannel(channel);

            mItems.add(movie3);
        }
        return mItems;
    }
}
