package com.riftone.riftonekhadas.data.home;

import com.riftone.riftonekhadas.model.Channel;
import com.riftone.riftonekhadas.model.Serie;

import java.util.ArrayList;

public class SerieProvider {
    private static final String TAG = ChannelProvider.class.getSimpleName();

    private static ArrayList<Serie> mItems = null;

    public static ArrayList<Serie> getSerieItems(){
        if(mItems == null){
            mItems = new ArrayList<Serie>();
            Channel channel = new Channel();
            channel.setId(100);
            channel.setName("GreenSport");
            channel.setDescription("description1");
            channel.setBrandImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/12308511_416095861916695_5747840403381292548_n.png?_nc_cat=106&_nc_sid=85a577&_nc_eui2=AeF3-jWufKdxcQEY5JsQs2lXwv554oenmVbC_nnih6eZVj3HXnFJ5gqVRANf_NvH2NFDiYjVHH7l9sqbAttZcTuu&_nc_ohc=PfWT6Bkgri0AX8Bp2-W&_nc_ht=scontent.frai1-1.fna&oh=bb6840e1f096e976ca805caa3d6f10b8&oe=5EF86E97");
            channel.setBackImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/47067050_913740945485515_1703652920316133376_n.jpg?_nc_cat=109&_nc_sid=dd9801&_nc_eui2=AeGyd6eqU3vbm3sGpeZF_gHl2fu-ydriy_rZ-77J2uLL-vX6sQ7BUbbZN1vKoq8Qiw-eiYW6NYI70nxX9ju_gPqk&_nc_ohc=48FnKAUomOgAX-AUKgY&_nc_ht=scontent.frai1-1.fna&oh=03ed8aead67e3b1fd09c842d5b2f5deb&oe=5EF99DAA");
            channel.setSlogan("Slogan1");
            channel.setType("F");
            channel.setLanguage("Portugues");
            channel.setCategory("Cat1,Cat2,Cat3");
            channel.setSrcVideoTeaser("Local");
            channel.setVideoTeaserUrl("http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4");
            channel.setSrc24H("External");
            channel.setUrl24H("https://link.24h.streaming");
            channel.setOwner(1);
            channel.setSubscribed(false);

            Serie serie1 = new Serie();
            serie1.setId(101);
            serie1.setName("Jenifa's Diary");
            serie1.setPoster("https://pbs.twimg.com/media/EDkSQs1X4AIkohU.jpg");
            serie1.setCover("https://beyouthifulblog.files.wordpress.com/2016/06/maxresdefault.jpg?w=1688&h=768&crop=1");
            serie1.setTeaser_src("Yt");
            serie1.setTeaser_url("ULgKE4OGBoM");
            serie1.setDescription("Description 1");
            serie1.setGenres("Genre1,Genre2,Genre3");
            serie1.setTypeOffer("F");
            serie1.setPrice(50F);
            serie1.setFree_sub_access(1);
            serie1.setPublish_date("23/06/2018");
            serie1.setCanSee(true);
            serie1.setChannel(channel);

            mItems.add(serie1);

            Serie serie2 = new Serie();
            serie2.setId(101);
            serie2.setName("Oga! Pastor");
            serie2.setPoster("https://m.media-amazon.com/images/M/MV5BYmI0M2FlMDEtMjRhYS00ZjdjLTk0NmEtZTBiMmUxZGM5NWNiXkEyXkFqcGdeQXVyMzQ3Nzc1MTA@._V1_.jpg");
            serie2.setCover("https://cdn1.everyevery.ng/wp-content/uploads/2019/06/19050425/OgaPastorTeaserThumb.jpg");
            serie2.setTeaser_src("Yt");
            serie2.setTeaser_url("S3uvspl4cJY");
            serie2.setDescription("Description 1");
            serie2.setGenres("Genre1,Genre2,Genre3");
            serie2.setTypeOffer("F");
            serie2.setPrice(50F);
            serie2.setFree_sub_access(1);
            serie2.setPublish_date("21/06/2019");
            serie2.setCanSee(true);
            serie2.setChannel(channel);

            mItems.add(serie2);

            Serie serie3 = new Serie();
            serie3.setId(101);
            serie3.setName("Everything In Between");
            serie3.setPoster("https://i.pinimg.com/474x/eb/c1/5b/ebc15b5d97c88f4149628bb9c97fb64e.jpg");
            serie3.setCover("https://i.ytimg.com/vi/5iB9aKb_IDI/maxresdefault.jpg");
            serie3.setTeaser_src("Yt");
            serie3.setTeaser_url("hzO2BjNna90");
            serie3.setDescription("Description 1");
            serie3.setGenres("Genre1,Genre2,Genre3");
            serie3.setTypeOffer("F");
            serie3.setPrice(50F);
            serie3.setFree_sub_access(1);
            serie3.setPublish_date("01/11/2018");
            serie3.setCanSee(true);
            serie3.setChannel(channel);

            mItems.add(serie3);
        }
        return mItems;
    }
}
