package com.riftone.riftonekhadas.data.home;

import com.riftone.riftonekhadas.model.Channel;
import com.riftone.riftonekhadas.model.Video;

import java.util.ArrayList;

public class VideoProvider {
    private static final String TAG = ChannelProvider.class.getSimpleName();

    private static ArrayList<Video> mItems = null;

    public static ArrayList<Video> getVideoItems(){
        if(mItems == null){
            mItems = new ArrayList<Video>();
            Channel channel = new Channel();
            channel.setId(100);
            channel.setName("GreenSport");
            channel.setDescription("description1");
            channel.setBrandImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/12308511_416095861916695_5747840403381292548_n.png?_nc_cat=106&_nc_sid=85a577&_nc_eui2=AeF3-jWufKdxcQEY5JsQs2lXwv554oenmVbC_nnih6eZVj3HXnFJ5gqVRANf_NvH2NFDiYjVHH7l9sqbAttZcTuu&_nc_ohc=PfWT6Bkgri0AX8Bp2-W&_nc_ht=scontent.frai1-1.fna&oh=bb6840e1f096e976ca805caa3d6f10b8&oe=5EF86E97");
            channel.setBackImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/47067050_913740945485515_1703652920316133376_n.jpg?_nc_cat=109&_nc_sid=dd9801&_nc_eui2=AeGyd6eqU3vbm3sGpeZF_gHl2fu-ydriy_rZ-77J2uLL-vX6sQ7BUbbZN1vKoq8Qiw-eiYW6NYI70nxX9ju_gPqk&_nc_ohc=48FnKAUomOgAX-AUKgY&_nc_ht=scontent.frai1-1.fna&oh=03ed8aead67e3b1fd09c842d5b2f5deb&oe=5EF99DAA");
            channel.setSlogan("Slogan1");
            channel.setType("F");
            channel.setLanguage("Portugues");
            channel.setCategory("Cat1,Cat2,Cat3");
            channel.setSrcVideoTeaser("Local");
            channel.setVideoTeaserUrl("http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4");
            channel.setSrc24H("External");
            channel.setUrl24H("https://link.24h.streaming");
            channel.setOwner(1);
            channel.setSubscribed(false);

            Video video1 = new Video();
            video1.setId(101);
            video1.setTitle("Video 1");
            video1.setDescription("Description 1");
            video1.setPosterUrl("https://1.bp.blogspot.com/-LaQBYtFfa-4/U0SLB8IoXxI/AAAAAAAAAVU/VSM_578-EQc/s1600/Poster+Example+1.jpg");
            video1.setCoverUrl("https://s.studiobinder.com/wp-content/uploads/2017/12/How-to-Make-a-Movie-Poster-Free-Movie-Poster-Credits-Template-1.jpg");
            video1.setSrcTeaser("Yt");
            video1.setUrlTeaser("vUOgaJ-F-Sg");
            video1.setSrcVideo("Local");
            video1.setVideoUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4");
            video1.setCountry("Cape Verde");
            video1.setLanguage("F");
            video1.setCategory("Cat1,Cat2,Cat3");
            video1.setAccessSubs("1");
            video1.setCanSee(true);
            video1.setChannel(channel);

            mItems.add(video1);

            Video video2 = new Video();
            video2.setId(101);
            video2.setTitle("Video 2");
            video2.setDescription("Description 2");
            video2.setPosterUrl("https://d1csarkz8obe9u.cloudfront.net/posterpreviews/trivia-night-blue-poster-design-template-1a030c6c27293628028546c98cb525ed_screen.jpg?ts=1561445695");
            video2.setCoverUrl("https://s.studiobinder.com/wp-content/uploads/2017/12/How-to-Make-a-Movie-Poster-Free-Movie-Poster-Credits-Template-1.jpg");
            video2.setSrcTeaser("Yt");
            video2.setUrlTeaser("vUOgaJ-F-Sg");
            video2.setSrcVideo("Local");
            video2.setVideoUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4");
            video2.setCountry("EUA");
            video2.setLanguage("F");
            video2.setCategory("Cat1,Cat2,Cat3");
            video2.setAccessSubs("1");
            video2.setCanSee(true);
            video2.setChannel(channel);

            mItems.add(video2);

            Video video3 = new Video();
            video3.setId(101);
            video3.setTitle("Video 3");
            video3.setDescription("Description 3");
            video3.setPosterUrl("https://image.slidesharecdn.com/scaleupdesignpostercompetition-160504115943/95/scale-up-design-poster-competition-1-638.jpg?cb=1462363266");
            video3.setCoverUrl("https://s.studiobinder.com/wp-content/uploads/2017/12/How-to-Make-a-Movie-Poster-Free-Movie-Poster-Credits-Template-1.jpg");
            video3.setSrcTeaser("Yt");
            video3.setUrlTeaser("vUOgaJ-F-Sg");
            video3.setSrcVideo("Local");
            video3.setVideoUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4");
            video3.setCountry("France");
            video3.setLanguage("F");
            video3.setCategory("Cat1,Cat2,Cat3");
            video3.setAccessSubs("1");
            video3.setCanSee(true);
            video3.setChannel(channel);

            mItems.add(video3);
        }
        return mItems;
    }
}
