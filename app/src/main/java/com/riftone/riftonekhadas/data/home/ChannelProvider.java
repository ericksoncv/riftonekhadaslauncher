package com.riftone.riftonekhadas.data.home;

import com.riftone.riftonekhadas.model.Channel;

import java.util.ArrayList;

public class ChannelProvider {
    private static final String TAG = ChannelProvider.class.getSimpleName();

    private static ArrayList<Channel> mItems = null;

    public static ArrayList<Channel> getChannelItems(){
        if(mItems == null){
            mItems = new ArrayList<Channel>();

            Channel channel1 = new Channel();
            channel1.setId(100);
            channel1.setName("GreenSport");
            channel1.setDescription("description1");
            channel1.setBrandImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/12308511_416095861916695_5747840403381292548_n.png?_nc_cat=106&_nc_sid=85a577&_nc_eui2=AeF3-jWufKdxcQEY5JsQs2lXwv554oenmVbC_nnih6eZVj3HXnFJ5gqVRANf_NvH2NFDiYjVHH7l9sqbAttZcTuu&_nc_ohc=PfWT6Bkgri0AX8Bp2-W&_nc_ht=scontent.frai1-1.fna&oh=bb6840e1f096e976ca805caa3d6f10b8&oe=5EF86E97");
            channel1.setBackImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/47067050_913740945485515_1703652920316133376_n.jpg?_nc_cat=109&_nc_sid=dd9801&_nc_eui2=AeGyd6eqU3vbm3sGpeZF_gHl2fu-ydriy_rZ-77J2uLL-vX6sQ7BUbbZN1vKoq8Qiw-eiYW6NYI70nxX9ju_gPqk&_nc_ohc=48FnKAUomOgAX-AUKgY&_nc_ht=scontent.frai1-1.fna&oh=03ed8aead67e3b1fd09c842d5b2f5deb&oe=5EF99DAA");
            channel1.setSlogan("Slogan1");
            channel1.setType("F");
            channel1.setLanguage("Portugues");
            channel1.setCategory("Cat1,Cat2,Cat3");
            channel1.setSrcVideoTeaser("Local");
            channel1.setVideoTeaserUrl("http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4");
            channel1.setSrc24H("External");
            channel1.setUrl24H("https://link.24h.streaming");
            channel1.setOwner(1);
            channel1.setSubscribed(false);
            mItems.add(channel1);

            Channel channel2 = new Channel();
            channel2.setId(101);
            channel2.setName("Musica TV");
            channel2.setDescription("description2");
            channel2.setBrandImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/12802835_1066116583439436_1808343785393047301_n.png?_nc_cat=103&_nc_sid=85a577&_nc_eui2=AeHo7DGvqw_6Vkoi3xlQl5rWXAjg3zyr9olcCODfPKv2iVjS5dZ6zYWlh9LOaHd17jWSKbBhR9HOtPyECvuMM6ky&_nc_ohc=aHcxP7QMiJ8AX9xsL9z&_nc_ht=scontent.frai1-1.fna&oh=f16d2bd5b1fb6657f6b90a72b2208f9a&oe=5EF749B3");
            channel2.setBackImageUrl("https://scontent.frai1-1.fna.fbcdn.net/v/t1.0-9/s960x960/34595801_1850832534967833_7588421194362126336_o.jpg?_nc_cat=111&_nc_sid=dd9801&_nc_eui2=AeFP6y3r6bmaGoHJpQAk0jW4z5EkbJrYt4rPkSRsmti3ihpD2DZTEiVtx5WuJtS0GzZ3OzRnc_U0Z4AQek3AuagF&_nc_ohc=M1fFh-BEztAAX9aZCR-&_nc_ht=scontent.frai1-1.fna&_nc_tp=7&oh=6bcc6cecbd7c5f216966b91a4d7c3395&oe=5EF9CB7D");
            channel2.setSlogan("Slogan1");
            channel2.setType("P");
            channel2.setLanguage("Portugues");
            channel2.setCategory("Cat1,Cat2,Cat3");
            channel2.setSrcVideoTeaser("Yt");
            channel2.setVideoTeaserUrl("ghj7WuZQupQ");
            channel2.setSrc24H("External");
            channel2.setUrl24H("https://link.24h.streaming");
            channel2.setOwner(1);
            channel2.setSubscribed(false);
            mItems.add(channel2);

            Channel channel3 = new Channel();
            channel3.setId(101);
            channel3.setName("BBC");
            channel3.setDescription("description3");
            channel3.setBrandImageUrl("https://wiwibloggs.com/wp-content/uploads/2014/05/bbc-logo-red-400x196.jpg");
            channel3.setBackImageUrl("https://cdn.vox-cdn.com/thumbor/WZYcmUzKybSmNn5LzwfRVIyQPKE=/0x0:640x480/1200x800/filters:focal(268x189:370x291)/cdn.vox-cdn.com/uploads/chorus_image/image/65543002/bbc_logo_red.0.jpg");
            channel3.setSlogan("Slogan1");
            channel3.setType("P");
            channel3.setLanguage("English");
            channel3.setCategory("Cat1,Cat2,Cat3");
            channel3.setSrcVideoTeaser("Local");
            channel3.setVideoTeaserUrl("http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Zeitgeist/Zeitgeist%202010_%20Year%20in%20Review.mp4");
            channel3.setSrc24H("External");
            channel3.setUrl24H("https://link.24h.streaming");
            channel3.setOwner(2);
            channel3.setSubscribed(true);
            mItems.add(channel3);
        }
        return mItems;
    }

}
