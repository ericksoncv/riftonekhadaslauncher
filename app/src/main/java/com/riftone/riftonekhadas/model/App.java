package com.riftone.riftonekhadas.model;

import android.graphics.drawable.Drawable;

public class App {
    private static final String TAG = App.class.getSimpleName();

    static final long serialVersionUID = 727566175075960653L;

    private Drawable icon;
    private String label;
    private String packageInstall;
    private int iconSettings;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPackageInstall() {
        return packageInstall;
    }

    public void setPackageInstall(String packageInstall) {
        this.packageInstall = packageInstall;
    }

    public int getIconSettings() {
        return iconSettings;
    }

    public void setIconSettings(int iconSettings) {
        this.iconSettings = iconSettings;
    }
}
