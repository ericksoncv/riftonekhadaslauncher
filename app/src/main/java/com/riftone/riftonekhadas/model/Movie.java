package com.riftone.riftonekhadas.model;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

public class Movie implements Serializable {
    private static final String TAG = Movie.class.getSimpleName();

    private Integer id;
    private String title;
    private String poster;
    private String cover;
    private String teaser_src;
    private String teaser_url;
    private String description;
    private String genres;
    private String country;
    private String language;
    private String type_upload;
    private String url_video;
    private String typeOffer;
    private Float price;
    private Integer free_sub_access;
    private String publish_date;
    private Boolean canSee;

    private Channel channel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public URI getPosterURI(){
        try {
            return new URI(getPoster());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public URI getCoverURI(){
        try {
            return new URI(getCover());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getTeaser_src() {
        return teaser_src;
    }

    public void setTeaser_src(String teaser_src) {
        this.teaser_src = teaser_src;
    }

    public String getTeaser_url() {
        return teaser_url;
    }

    public void setTeaser_url(String teaser_url) {
        this.teaser_url = teaser_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType_upload() {
        return type_upload;
    }

    public void setType_upload(String type_upload) {
        this.type_upload = type_upload;
    }

    public String getUrl_video() {
        return url_video;
    }

    public void setUrl_video(String url_video) {
        this.url_video = url_video;
    }

    public String getTypeOffer() {
        return typeOffer;
    }

    public void setTypeOffer(String typeOffer) {
        this.typeOffer = typeOffer;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getFree_sub_access() {
        return free_sub_access;
    }

    public void setFree_sub_access(Integer free_sub_access) {
        this.free_sub_access = free_sub_access;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public Boolean getCanSee() {
        return canSee;
    }

    public void setCanSee(Boolean canSee) {
        this.canSee = canSee;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public String toString(){
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
