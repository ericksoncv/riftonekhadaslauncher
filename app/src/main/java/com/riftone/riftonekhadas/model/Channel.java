package com.riftone.riftonekhadas.model;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

public class Channel implements Serializable {
    private static final String TAG = Channel.class.getSimpleName();

    static final long serialVersionUID = 727566175075960653L;

    private long id;
    private String name;
    private String brandImageUrl;
    private String backImageUrl;
    private String description;
    private String slogan;
    private String type;
    private String language;
    private String category;
    private String srcVideoTeaser;
    private String videoTeaserUrl;
    private String src24H;
    private String url24H;
    private Integer owner;
    private boolean isSubscribed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandImageUrl() {
        return brandImageUrl;
    }

    public void setBrandImageUrl(String brandImageUrl) {
        this.brandImageUrl = brandImageUrl;
    }

    public URI getBrandImageURI(){
        try {
            return new URI(getBrandImageUrl());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getBackImageUrl() {
        return backImageUrl;
    }

    public void setBackImageUrl(String backImageUrl) {
        this.backImageUrl = backImageUrl;
    }

    public URI getBackImageURI(){
        try {
            return new URI(getBackImageUrl());
        } catch (URISyntaxException e) {
            return null;
        }
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSrcVideoTeaser() {
        return srcVideoTeaser;
    }

    public void setSrcVideoTeaser(String srcVideoTeaser) {
        this.srcVideoTeaser = srcVideoTeaser;
    }

    public String getVideoTeaserUrl() {
        return videoTeaserUrl;
    }

    public void setVideoTeaserUrl(String videoTeaserUrl) {
        this.videoTeaserUrl = videoTeaserUrl;
    }

    public String getSrc24H() {
        return src24H;
    }

    public void setSrc24H(String src24H) {
        this.src24H = src24H;
    }

    public String getUrl24H() {
        return url24H;
    }

    public void setUrl24H(String url24H) {
        this.url24H = url24H;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    @Override
    public String toString(){
        return "Channel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
