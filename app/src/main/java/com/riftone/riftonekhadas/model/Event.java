package com.riftone.riftonekhadas.model;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

public class Event implements Serializable {
    private static final String TAG = Event.class.getSimpleName();

    static final long serialVersionUID = 727566175075960653L;

    private Integer id;
    private String name;
    private String description;
    private String posterUrl;
    private String coverUrl;
    private String teaserSrc;
    private String teaserUrl;
    private String typeEvent;
    private String country;
    private String city;
    private String language;
    private String timeZone;
    private String startDate;
    private String endDate;
    private Integer freeSubAcess;
    private Integer limiteUser;
    private Float price;
    private String currency;
    private String category;

    private Channel channel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public URI getPosterURI(){
        try {
            return new URI(getPosterUrl());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public URI getCoverURI(){
        try {
            return new URI(getCoverUrl());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getTeaserSrc() {
        return teaserSrc;
    }

    public void setTeaserSrc(String teaserSrc) {
        this.teaserSrc = teaserSrc;
    }

    public String getTeaserUrl() {
        return teaserUrl;
    }

    public void setTeaserUrl(String teaserUrl) {
        this.teaserUrl = teaserUrl;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getFreeSubAcess() {
        return freeSubAcess;
    }

    public void setFreeSubAcess(Integer freeSubAcess) {
        this.freeSubAcess = freeSubAcess;
    }

    public Integer getLimiteUser() {
        return limiteUser;
    }

    public void setLimiteUser(Integer limiteUser) {
        this.limiteUser = limiteUser;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public String toString(){
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
