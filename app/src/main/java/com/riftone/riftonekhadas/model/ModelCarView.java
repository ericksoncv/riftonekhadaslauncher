package com.riftone.riftonekhadas.model;

public class ModelCarView {

    private String textCard;
    private Integer imageCard;

    public ModelCarView(String textCard, Integer imageCard) {
        this.textCard = textCard;
        this.imageCard = imageCard;
    }

    public String getTextCard() {
        return textCard;
    }

    public void setTextCard(String textCard) {
        this.textCard = textCard;
    }

    public Integer getImageCard() {
        return imageCard;
    }

    public void setImageCard(Integer imageCard) {
        this.imageCard = imageCard;
    }
}
