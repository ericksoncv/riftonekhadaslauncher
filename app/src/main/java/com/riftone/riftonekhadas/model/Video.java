package com.riftone.riftonekhadas.model;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

public class Video implements Serializable {

    private static final String TAG = Video.class.getSimpleName();

    static final long serialVersionUID = 727566175075960653L;

    private Integer id;
    private String title;
    private String description;
    private String posterUrl;
    private String coverUrl;
    private String srcTeaser;
    private String urlTeaser;
    private String srcVideo;
    private String videoUrl;
    private String country;
    private String language;
    private String category;
    private String accessSubs;
    private boolean canSee;
    private Channel channel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public URI getPosterURI(){
        try {
            return new URI(getPosterUrl());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public URI getCoverURI(){
        try {
            return new URI(getCoverUrl());
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public String getSrcTeaser() {
        return srcTeaser;
    }

    public void setSrcTeaser(String srcTeaser) {
        this.srcTeaser = srcTeaser;
    }

    public String getUrlTeaser() {
        return urlTeaser;
    }

    public void setUrlTeaser(String urlTeaser) {
        this.urlTeaser = urlTeaser;
    }

    public String getSrcVideo() {
        return srcVideo;
    }

    public void setSrcVideo(String srcVideo) {
        this.srcVideo = srcVideo;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAccessSubs() {
        return accessSubs;
    }

    public void setAccessSubs(String accessSubs) {
        this.accessSubs = accessSubs;
    }

    public boolean isCanSee() {
        return canSee;
    }

    public void setCanSee(boolean canSee) {
        this.canSee = canSee;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public String toString(){
        return "Video{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
