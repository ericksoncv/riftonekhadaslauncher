//////////////////////////////////////////////////////////////////////////
/////
// DTV MIDDLE WARE DEFINES, PLEASE DO NOT MAKE ANY CHANGE OF THIS FILE
/////
//////////////////////////////////////////////////////////////////////////

package com.superdtv;

public class MW {
	static {
		System.loadLibrary("com_superdtv_mw"); 
	}

//Ret Type
	public static final int RET_OK = 0;
	public static final int RET_NO_CHANNEL = RET_OK+1;
	public static final int RET_NO_SIGNAL = RET_NO_CHANNEL+1;
	public static final int RET_NO_VIDEO_DATA = RET_NO_SIGNAL+1;
	public static final int RET_NO_AUDIO_DATA = RET_NO_VIDEO_DATA+1;
	public static final int RET_SCRAMBLE_CHANNEL = RET_NO_AUDIO_DATA+1;
	public static final int RET_NO_THIS_CHANNEL = RET_SCRAMBLE_CHANNEL+1;
	public static final int RET_NO_VIDEO_CHANNEL = RET_NO_THIS_CHANNEL+1;
	public static final int RET_NO_AUDIO_CHANNEL = RET_NO_VIDEO_CHANNEL+1;
	public static final int RET_INVALID_TP = 13;
	public static final int RET_EVENT_NOT_FOUND = 16;
	public static final int RET_INVALID_AREA_INDEX = 19;
	public static final int RET_INVALID_SYSTEM_TYPE = RET_INVALID_AREA_INDEX+1;
	public static final int RET_INVALID_TP_INDEX = RET_INVALID_SYSTEM_TYPE+1;
	public static final int RET_INVALID_RECORD_INDEX = RET_INVALID_TP_INDEX+1;
	public static final int RET_MEMORY_ERROR = RET_INVALID_RECORD_INDEX+1;
	
	public static final int RET_INVALID_TYPE = RET_MEMORY_ERROR+1;
	public static final int RET_INVALID_AREA_INDEX_LIST = 28;
	public static final int RET_INVALID_TP_INDEX_LIST = RET_INVALID_AREA_INDEX_LIST+1;
	public static final int RET_INVALID_RECORD_INDEX_LIST = RET_INVALID_TP_INDEX_LIST+1;
	public static final int RET_INVALID_OBJECT = RET_INVALID_RECORD_INDEX_LIST+1;
	public static final int RET_INVALID_SERVICE = RET_INVALID_OBJECT+1;
	public static final int RET_INVALID_DATE_TIME = 35;
	
	public static final int RET_ALLOC_OBJECT_FAILED = RET_INVALID_DATE_TIME+1;
	public static final int RET_GET_OBJECT_FAILED = RET_ALLOC_OBJECT_FAILED+1;
	public static final int RET_GET_BUFFER_FAILED = RET_GET_OBJECT_FAILED+1;
	public static final int RET_SYSTEM_ERROR = RET_GET_BUFFER_FAILED+1;
	public static final int RET_GET_JAVA_METHOD_FAILED = RET_SYSTEM_ERROR+1;
	public static final int RET_FUNCTION_NOT_SUPPORTTED = RET_GET_JAVA_METHOD_FAILED+1;

	public static final int RET_ALREADY_RECORDING = RET_FUNCTION_NOT_SUPPORTTED+1;
	public static final int RET_NO_THIS_RECORD = RET_ALREADY_RECORDING+1;
	public static final int RET_PLAYBACK_FAILED = RET_NO_THIS_RECORD+1;	
	public static final int RET_SERVICE_LOCK = RET_PLAYBACK_FAILED+1;	
	public static final int RET_NO_DATA = RET_SERVICE_LOCK+1;	
	
	public static final int RET_REC_ERR_CANNOT_CREATE_THREAD = RET_NO_DATA+1;
	public static final int RET_REC_ERR_BUSY = RET_REC_ERR_CANNOT_CREATE_THREAD+1;
	public static final int RET_REC_ERR_CANNOT_OPEN_FILE = RET_REC_ERR_BUSY+1;
	public static final int RET_REC_ERR_CANNOT_WRITE_FILE = RET_REC_ERR_CANNOT_OPEN_FILE+1;
	public static final int RET_REC_ERR_CANNOT_ACCESS_FILE = RET_REC_ERR_CANNOT_WRITE_FILE+1;
	public static final int RET_REC_ERR_FILE_TOO_LARGE = RET_REC_ERR_CANNOT_ACCESS_FILE+1;
	public static final int RET_REC_ERR_DVR = RET_REC_ERR_FILE_TOO_LARGE+1;
//Event Type	
	public static final int EVENT_TYPE_PLAY_STATUS = 0;
	public static final int EVENT_TYPE_SEARCH_STATUS = 1;
	public static final int EVENT_TYPE_TUNER_STATUS = 2;
	public static final int EVENT_TYPE_DATE_TIME = 4;
	public static final int EVENT_TYPE_EPG_CURRENT_NEXT_UPDATE = 5;	
	public static final int EVENT_TYPE_EPG_SCHEDULE_UPDATE = 6;	
	public static final int EVENT_TYPE_PVR_STATUS = 8; /*force callback , not need register*/	
	
	public static final int SEARCH_STATUS_SEARCH_START = 0;
	public static final int SEARCH_STATUS_UPDATE_CHANNEL_LIST = 1;	
	public static final int SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO = 6;	
	public static final int SEARCH_STATUS_SAVE_DATA_START = 7;	
	public static final int SEARCH_STATUS_SAVE_DATA_END = 8;	
	public static final int SEARCH_STATUS_SEARCH_END = 9;	

	public static final int PLAY_STATUS_OK = 0;
	public static final int PLAY_STATUS_NO_CHANNEL = PLAY_STATUS_OK+1;	
	public static final int PLAY_STATUS_NO_SIGNAL = PLAY_STATUS_NO_CHANNEL+1;	
	public static final int PLAY_STATUS_NO_VIDEO = PLAY_STATUS_NO_SIGNAL+1;	
	public static final int PLAY_STATUS_NO_AUDIO = PLAY_STATUS_NO_VIDEO+1;	
	
	public static final int PVR_RECORD_STATUS_START = 0;
	public static final int PVR_RECORD_STATUS_END = PVR_RECORD_STATUS_START+1;	
	public static final int PVR_RECORD_STATUS_UPDATE_TIME = PVR_RECORD_STATUS_END+1;	
	public static final int PVR_RECORD_STATUS_ERROR = PVR_RECORD_STATUS_UPDATE_TIME+1;	
	
	public static final int PVR_PLAYBACK_STATUS_INIT = PVR_RECORD_STATUS_ERROR+1;	
	public static final int PVR_PLAYBACK_STATUS_EXIT = PVR_PLAYBACK_STATUS_INIT+1;	
	public static final int PVR_PLAYBACK_STATUS_PLAY = PVR_PLAYBACK_STATUS_EXIT+1;	
	public static final int PVR_PLAYBACK_STATUS_PAUSE = PVR_PLAYBACK_STATUS_PLAY+1;	
	public static final int PVR_PLAYBACK_STATUS_FFFB = PVR_PLAYBACK_STATUS_PAUSE+1;	
	public static final int PVR_PLAYBACK_STATUS_SEARCH_OK = PVR_PLAYBACK_STATUS_FFFB+1;	
	public static final int EPG_EVENT_PLAYING_STATUS_EXPIRED = 0;
	public static final int EPG_EVENT_PLAYING_STATUS_PLAYING = EPG_EVENT_PLAYING_STATUS_EXPIRED+1;	
	public static final int EPG_EVENT_PLAYING_STATUS_NEW = EPG_EVENT_PLAYING_STATUS_PLAYING+1;	

	public static final int AUDIO_CHANNEL_STEREO = 0;
	public static final int AUDIO_CHANNEL_LEFT = AUDIO_CHANNEL_STEREO+1;	
	public static final int AUDIO_CHANNEL_RIGHT = AUDIO_CHANNEL_LEFT+1;	 	
	
	public static final int TV_CH = 0;
	public static final int RADIO_CH = TV_CH+1;	

	public static final int VIDEO_RESOLUTION_TYPE_UNSET = 0;
	public static final int VIDEO_RESOLUTION_TYPE_SD = VIDEO_RESOLUTION_TYPE_UNSET+1;	
	public static final int VIDEO_RESOLUTION_TYPE_HD = VIDEO_RESOLUTION_TYPE_SD+1;	
	public static final int VIDEO_RESOLUTION_TYPE_4K = VIDEO_RESOLUTION_TYPE_HD+1;	
	
	public static final int SEARCH_TYPE_AUTO = 1;	
	public static final int SEARCH_TYPE_MANUAL = 2;	
	public static final int MAIN_TUNER_INDEX = 0;

	public static final int SYSTEM_TYPE_DVB_T = 0;

	public static final int FRONT_END_TYPE_Undef = 0;
	public static final int FRONT_END_TYPE_TER = 3;
	public static final int FRONT_END_TYPE_DVBT_2 = 4;

//Global config types
	public static final int GLOBAL_CONFIG_SET_AUDIO_VOLUME = 8;  


// db
	//service
	
	/*	Return Value : total service count 
    */
	public static native int db_get_total_service_count();

	/*	Return Value : total service count of appointed service type
	*/
	public static native int db_get_service_count(int service_type);

	/*	Return Value : RET_OK
					   RET_ALLOC_OBJECT_FAILED
	                   RET_NO_CHANNEL 
					   RET_INVALID_OBJECT
	*/
	public static native int db_get_current_service_info(mw_data.service o_service_clazz);
	
	/*	Return Value : RET_OK
					   RET_ALLOC_OBJECT_FAILED
	                   RET_NO_THIS_CHANNEL 
					   RET_INVALID_OBJECT
	*/
	public static native int db_get_service_info(int serviceType, int serviceIndex, mw_data.service o_service_clazz);
	
	/*	Return Value : true , true means delete all service data succeed
	                   false, false means delete all service data failed
    */
	public static native boolean db_delete_all_service();

	//ter, tp

	/*	Return Value : tp count of the appointed system_type
	*/	
	public static native int db_ter_get_tp_count(int system_type);

	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
					   RET_INVALID_SYSTEM_TYPE
					   RET_INVALID_TP
	*/	
	public static native int db_ter_get_current_tp_info(int system_type, mw_data.dvb_t_tp o_tp_clazz);
	
	/*	Return Value : RET_OK
					   RET_INVALID_SYSTEM_TYPE
					   RET_INVALID_TP_INDEX
					   RET_INVALID_OBJECT
	*/	
	public static native int db_ter_get_tp_info(int system_type, int area_index, int tpIndex, mw_data.dvb_t_tp o_tp_clazz);
	
	/*	Return Value : RET_OK
					   RET_INVALID_SYSTEM_TYPE
					   RET_INVALID_TP_INDEX
	*/	
	public static native int db_ter_set_current_tp(int system_type, int tpIndex);
	
	/*	Return Value : RET_OK
					   RET_INVALID_SYSTEM_TYPE
					   RET_INVALID_TP_INDEX
					   RET_INVALID_OBJECT
	*/	
	public static native int db_ter_set_tp_info(mw_data.dvb_t_tp i_tp_clazz);

	/*	Return Value : true , true means save area tp data succeed
	                   false, false means save area tp data failed
    */
	public static native boolean db_ter_save_area_tp();

	/*	Return Value : current area index of the system_type
    */
	public static native int db_ter_get_area_index(int system_type);

	/*	Return Value : RET_OK
					   RET_INVALID_AREA_INDEX
					   RET_INVALID_SYSTEM_TYPE
					   RET_INVALID_OBJECT
	*/	
	public static native int db_ter_set_area_index(int system_type, int area_index);

	/*	Return Value : area count of the system_type
	*/	
	public static native int db_ter_get_area_count(int system_type);

	/*	Return Value : None
    */
	public static native void db_load_default_data();

// ts player	

	//play
	
	/*	Return Value : None
    */
	public static native void ts_player_require_play_status();

	/*	Return Value : play status of current ts player
	                        value range : from PLAY_STATUS_OK to PLAY_STATUS_SCRAMBLE
    */
	public static native int ts_player_get_play_status();

	/*	Return Value : None
    */
	public static native void ts_player_stop_play();

	/*	Return Value : None
    */
	public static native void ts_player_stop_play_keep_video_frame();

	/*	Return Value : None
    */
	public static native void ts_player_stop_play_and_tunning();
	
	/*	Return Value : RET_OK
					   RET_NO_THIS_CHANNEL	
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play(int serviceType, int serviceIndex, int timeDelayMs, boolean lockPass);
	
	/*	Return Value : RET_OK
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play_current(boolean lockPass, boolean smallVideoWindow);
	
	/*	Return Value : RET_OK
					   RET_NO_CHANNEL
	*/	
	public static native int ts_player_play_switch_audio_pid(int audioIndex);
	
	/*	Return Value : RET_OK
					   RET_NO_CHANNEL
	*/	
	public static native int ts_player_play_switch_audio_channel(int audioChannel);
	
	/*	Return Value : RET_OK
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play_up(int timeDelayMs, boolean lockPass);
	
	/*	Return Value : RET_OK
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play_down(int timeDelayMs, boolean lockPass);
	
	/*	Return Value : RET_OK
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play_previous(boolean lockPass);
	
	/*	Return Value : RET_OK
					   RET_NO_VIDEO_CHANNEL
					   RET_NO_AUDIO_CHANNEL
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play_switch_tv_radio(boolean lockPass);
	
	/*	Return Value : RET_OK
					   RET_NO_THIS_CHANNEL
					   RET_NO_CHANNEL
					   RET_SERVICE_LOCK
	*/	
	public static native int ts_player_play_by_channel_number(int chnum, boolean lockPass);
	
	/*	Return Value : None
    */
	public static native void ts_player_set_position(int x, int y, int w, int h);
	

// tuner
	
	/*	Return Value : None
    */
	public static native void tuner_require_tuner_status(int tunerIndex);
	
	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
	*/	
	public static native int tuner_get_tuner_status(int tunerIndex, mw_data.tuner_signal_status o_tuner_status_clazz);
	
	/*	Return Value : RET_OK
					   RET_INVALID_SYSTEM_TYPE
					   RET_INVALID_TP_INDEX
	*/	
	public static native int tuner_ter_lock_tp(int tunerIndex, int system_type, int tpIndex, boolean bWaitActualLock);
	
	/*	Return Value : None
    */
	public static native void tuner_unlock_tp(int tunerIndex, int system_type, boolean bWaitActualStopLock);

// search	

	/*	Return Value : RET_OK
					   RET_INVALID_TYPE
					   RET_INVALID_AREA_INDEX
					   RET_INVALID_TP_INDEX
					   RET_INVALID_AREA_INDEX_LIST
					   RET_INVALID_TP_INDEX_LIST
					   RET_MEMORY_ERROR
	*/	
	public static native int search_ter_start(int searchType, int[] areaIndexList, int[] tpIndexList, int maxChannelCountShow);
	
	/*	Return Value : None
    */
	public static native void search_ter_stop(int system_type);


// epg
	/*	Return Value : None
    */
	public static native void epg_require_pf_event();
	
	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
	*/	
	public static native int epg_get_pf_event(mw_data.service i_service_clazz, mw_data.epg_pf_event o_epg_current_event_clazz, mw_data.epg_pf_event o_epg_next_event_clazz);

	/*	Return Value : None
    */
	public static native void epg_set_schedule_event_day_offset(int dayOffset);

	/*	Return Value : event count of given tsId && serviceId && weekDay
	*/	
	public static native int epg_lock_get_schedule_event_count(int dayOffset, mw_data.service i_service_clazz, mw_data.date i_current_date_clazz);

	/*	Return Value : event id
	*/	
	public static native int epg_lock_get_schedule_event_id_by_index(int dayOffset, int index, mw_data.service i_service_clazz, mw_data.date i_current_date_clazz);

	/*	Return Value : None
    */
	public static native void epg_unlock_schedule_event();
	
	/*	Return Value : RET_OK
					   RET_EVENT_NOT_FOUND
					   RET_INVALID_OBJECT
	*/	
	public static native int epg_get_schedule_event_by_event_id(int dayOffset, int event_id, mw_data.service i_service_clazz, mw_data.date i_current_date_clazz, mw_data.epg_schedule_event o_epg_event_clazz);

	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
					   RET_EVENT_NOT_FOUND
					   RET_FUNCTION_NOT_SUPPORTTED
	*/	
	public static native int epg_get_schedule_extended_event_by_event_id(int dayOffset, int index, mw_data.service i_service_clazz, mw_data.date i_current_date_clazz, mw_data.epg_extended_event o_epg_extended_event_clazz);
	
// pvr

	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
	*/	
	public static native int pvr_record_init_record_list(String storage_mount_dir);

	/*	Return Value : None
	*/	
	public static native void pvr_record_exit_record_list();

	/*	Return Value : record count of the inited record list
	*/	
	public static native int pvr_record_get_record_count();

	/*	Return Value : RET_OK
					   RET_ALLOC_OBJECT_FAILED
					   RET_NO_THIS_RECORD
	*/	
	public static native int pvr_record_get_record_info(int recordIndex, mw_data.pvr_record o_pvr_record_clazz);
	
	/*	Return Value : RET_OK
					   RET_INVALID_RECORD_INDEX
					   RET_MEMORY_ERROR
					   RET_INVALID_RECORD_INDEX_LIST
	*/	
	public static native int pvr_record_delete_record(int[] recordIndexList);
	
	/*	Return Value : RET_OK
					   RET_INVALID_RECORD_INDEX
					   RET_INVALID_OBJECT
	*/	
	public static native int pvr_record_change_record_name(int recordIndex, String recordName);
	
	/*	Return Value : RET_OK
					   RET_INVALID_RECORD_INDEX
	*/	
	public static native int pvr_record_set_record_lock(int recordIndex, boolean is_locked);
	
	/*	Return Value : index of available record id
					   RET_INVALID_RECORD_INDEX
	*/	
	public static native int pvr_record_get_avaible_record_id();

	/*	Return Value : RET_OK
					   RET_ALREADY_RECORDING
					   RET_NO_THIS_CHANNEL
					   RET_INVALID_OBJECT
	*/	
	public static native int pvr_record_start(int record_dev_id, int serviceType, int serviceIndex, int duration, String storage_mount_dir, String event_name);

	/*	Return Value : None
	*/	
	public static native void pvr_record_stop(int record_dev_id);

	/*	Return Value : RET_OK
					   RET_PLAYBACK_FAILED
	*/	
	public static native int pvr_playback_start(int recordIndex);

	/*	Return Value : None
	*/	
	public static native void pvr_playback_stop();
	
	/*	Return Value : None
	*/	
	public static native void pvr_playback_pause();
	
	/*	Return Value : None
	*/	
	public static native void pvr_playback_resume();
	
	/*	Return Value : None
	*/	
	public static native void pvr_playback_fast_forward(int speed);
	
	/*	Return Value : None
	*/	
	public static native void pvr_playback_fast_backward(int speed);
	
	/*	Return Value : None
	*/	
	public static native void pvr_playback_seek(int posSeconds, boolean start);
	
	/*	Return Value : None
	*/	
	public static native void pvr_playback_switch_audio(int audioPid, int audioFmt);
	
	
	/*	Return Value : RET_OK
					   RET_INVALID_TYPE
	*/
	public static native int global_set_int_config(int config_type, int config_value);
// mw
	
	/*	Return Value : true means support
					   false means unsupport
	*/
    public static native boolean is_system_supportted(int tunerIndex, int system_type);
	
	
	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
	*/
	public static native int get_date_time(mw_data.date_time o_date_time_clazz);

	/*	Return Value : RET_OK
					   RET_INVALID_OBJECT
	*/
	public static native int get_date(int dayOffset, mw_data.date i_date_clazz, mw_data.date o_date_clazz);
	
	/*	Return Value : RET_OK
					   RET_GET_OBJECT_FAILED
					   RET_GET_JAVA_METHOD_FAILED
					   RET_ALLOC_OBJECT_FAILED
	*/
    public static native int register_event_cb(Object i_clazz);
	
	/*	Return Value : RET_OK
					   RET_INVALID_TYPE
	*/
    public static native int register_event_type(int event_type, boolean enable);
}

//////////////////////////////////////////////////////////////////////////
/////
// DTV MIDDLE WARE DEFINES, PLEASE DO NOT MAKE ANY CHANGE OF THIS FILE
/////
//////////////////////////////////////////////////////////////////////////

