package com.superdtv;

import com.superdtv.*;

import java.util.*;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.os.*;
import android.util.Log;
import android.view.*;
import android.webkit.WebView;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;

import com.superdtv.Fragment.BlankFragment;
import com.superdtv.StorageDevice.DeviceItem;

public class DtvMainActivity extends DtvBaseActivity {
    private static final String TAG = "DtvMainActivity";

    private static final int CHANGE_CHANNEL_DELAYED_TIME = 300;

    public static final int LOCAL_MSG_CLOSE_INFO_BAR = 0;
    public static final int LOCAL_MSG_CLOSE_VOLUME_BAR = LOCAL_MSG_CLOSE_INFO_BAR + 1;
    public static final int LOCAL_MSG_CLOSE_TMP_PLAY_STATUS_INFO = LOCAL_MSG_CLOSE_VOLUME_BAR + 1;
    public static final int LOCAL_MSG_PLAY_BY_CHANNEL_NUMBER = LOCAL_MSG_CLOSE_TMP_PLAY_STATUS_INFO + 1;
    public static final int LOCAL_MSG_SET_SCREEN_MODE = LOCAL_MSG_PLAY_BY_CHANNEL_NUMBER + 1;
    public static final int LOCAL_MSG_GET_DEVICE_INFO = LOCAL_MSG_SET_SCREEN_MODE + 1;
    public static final int LOCAL_MSG_SHOW_LOADING_ICON = LOCAL_MSG_GET_DEVICE_INFO + 1;
    public static final int LOCAL_MSG_DELAY_ENABLE_PLAY_STATUS = LOCAL_MSG_SHOW_LOADING_ICON + 1;
    public static final int LOCAL_MSG_PLAY_BY_F1 = LOCAL_MSG_DELAY_ENABLE_PLAY_STATUS + 1;
    public static final int LOCAL_MSG_STATUS_BG_CHANGE = LOCAL_MSG_PLAY_BY_F1 + 1;

    private StorageDevice storageDeviceInfo;
    private int device_no = -1;
    private boolean bInRecording = false;
    private boolean enableRecord = false;

    private StorageDevicePlugReceiver storageDevicePlugReceiver;
    private StorageDeviceDialog DeviceInfoDia = null;
    private String sCurrentRecordStoreDir;
    private Context mContext = null;
    private Handler localMsgHandler;
    private Handler mwMsgHandler;
    private static final int currentSystemType = MW.SYSTEM_TYPE_DVB_T;
    private mw_data.service serviceInfo = new mw_data.service();
    private Dialog dia_currentDialog;
    private VideoView videoView = null;
    private TextView txvPlayStatus, txvDvbTime, txvLockStatus, txvServiceCount, txvCurrentServiceInfo, txvPvrRecording, txv_infoBar_currentEpg, txv_infoBar_nextEpg;
    private mw_data.date_time dateTime = new mw_data.date_time();
    private mw_data.epg_pf_event epgCurrentEvent = new mw_data.epg_pf_event();
    private mw_data.epg_pf_event epgNextEvent = new mw_data.epg_pf_event();

    private class StorageDevicePlugReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(StorageDevice.ACTION_DEVICE_MOUNTED)) {
                Log.e(TAG, "StorageDevice plug in :  " + intent.getStringExtra(StorageDevice.DEVICE_PATH));
                if (DeviceInfoDia != null) {
                    DeviceInfoDia.refreshStorageDevice();
                }

                Common.makeText(DtvMainActivity.this, R.string.insert, Toast.LENGTH_SHORT);
            } else if (action.equals(StorageDevice.ACTION_DEVICE_REMOVED)) {
                Common.makeText(DtvMainActivity.this, R.string.out, Toast.LENGTH_SHORT);

                Log.e(TAG, "StorageDevice plug out :  " + intent.getStringExtra(StorageDevice.DEVICE_PATH));
                if (DeviceInfoDia != null) {
                    DeviceInfoDia.refreshStorageDevice();
                }

                if (bInRecording) {
                    if (sCurrentRecordStoreDir != null && sCurrentRecordStoreDir.equals(intent.getStringExtra(StorageDevice.DEVICE_PATH))) {
                        Log.e(TAG, "stop recording :  " + intent.getStringExtra(StorageDevice.DEVICE_PATH));

                        RemoveDeviceIsFullMsg();
                        MW.pvr_record_stop(0);
                        bInRecording = false;
                    }
                }
            } else if (action.equals(PlatformSpecial.ACTION_HDMI_PLUGGED)) {
//	            boolean state = intent.getBooleanExtra("state", false);
//        		Log.e(TAG, "HDMI plug status :  "+state);
                {
                    //plug/unplug HDMI cable, the system will switch output mode resolution, so we need to reset the video output size
                    //wait some time for system switch output mode
                    (new Handler()).postDelayed(new Runnable() {
                        public void run() {
                            Common.setVideoViewWindow(videoView);
                        }
                    }, 500);
                }
            }
        }
    }

    private void UpdateCurrentNextEpgInfo() {

        Log.e("EPG", "UpdateCurrentNextEpgInfo");
        if (MW.epg_get_pf_event(serviceInfo, epgCurrentEvent, epgNextEvent) == MW.RET_OK) {
            if (epgCurrentEvent.event_name.length() > 0) {
                Log.e("EPG", " current : " + String.format("%02d:%02d - %02d:%02d  ", epgCurrentEvent.start_hour, epgCurrentEvent.start_minute,
                        epgCurrentEvent.end_hour, epgCurrentEvent.end_minute) + epgCurrentEvent.event_name + " :    " + epgCurrentEvent.play_progress + "%    Rating : " + epgCurrentEvent.parental_rating);
            }

            if (epgNextEvent.event_name.length() > 0) {
                Log.e("EPG", "    next : " + String.format("%02d:%02d - %02d:%02d  ", epgNextEvent.start_hour, epgNextEvent.start_minute,
                        epgNextEvent.end_hour, epgNextEvent.end_minute) + epgNextEvent.event_name);
            }

            if (epgCurrentEvent.event_name.length() > 0) {
                txv_infoBar_currentEpg.setText(String.format("%02d:%02d - %02d:%02d  ", epgCurrentEvent.start_hour, epgCurrentEvent.start_minute,
                        epgCurrentEvent.end_hour, epgCurrentEvent.end_minute) + epgCurrentEvent.event_name);
            } else {
                txv_infoBar_currentEpg.setText(R.string.no_current_event);
            }

            if (epgNextEvent.event_name.length() > 0) {
                txv_infoBar_nextEpg.setText(String.format("%02d:%02d - %02d:%02d  ", epgNextEvent.start_hour, epgNextEvent.start_minute,
                        epgNextEvent.end_hour, epgNextEvent.end_minute) + epgNextEvent.event_name);
            } else {
                txv_infoBar_nextEpg.setText(R.string.no_next_event);
            }
        } else {
            txv_infoBar_currentEpg.setText(R.string.no_current_event);
            txv_infoBar_nextEpg.setText(R.string.no_next_event);
        }

    }

    class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            int event_type = (msg.what >> 16) & 0xFFFF;
            int sub_event_type = msg.what & 0xFFFF;

            switch (event_type) {
                case MW.EVENT_TYPE_PLAY_STATUS: {
                    String playStatus = "Play Status : ";
                    switch (sub_event_type) {
                        case MW.PLAY_STATUS_OK:
                            playStatus += "OK";
                            layoutMessagemError("OK", View.GONE);
                            break;
                        case MW.PLAY_STATUS_NO_CHANNEL:
                            playStatus += "No service";
                            noServiceLayout();
                            break;
                        case MW.PLAY_STATUS_NO_SIGNAL:
                            playStatus += "No signal";
                            layoutMessagemError("No signal", View.VISIBLE);
                            break;
                        case MW.PLAY_STATUS_NO_VIDEO:
                            playStatus += "No video";
                            layoutMessagemError("No video", View.VISIBLE);


                            break;
                        case MW.PLAY_STATUS_NO_AUDIO:
                            playStatus += "No audio";
                            layoutMessagemError("No audio", View.VISIBLE);
                            break;

                        default:
                            playStatus = "Unknown error : " + sub_event_type;
                            layoutMessagemError(playStatus, View.VISIBLE);
                            break;
                    }

                    if (sub_event_type == MW.PLAY_STATUS_OK)
                        enableRecord = true;
                    else
                        enableRecord = false;

                    txvPlayStatus.setText(playStatus);
                }
                break;

                case MW.EVENT_TYPE_TUNER_STATUS: {
                    mw_data.tuner_signal_status paramsInfo = (mw_data.tuner_signal_status) msg.obj;

                    if (paramsInfo != null) {
                        Log.e(TAG, " mwEventCallback : MW.EVENT_TYPE_TUNER_STATUS : " + paramsInfo.locked + " " +
                                paramsInfo.error + " " + sub_event_type + " " + paramsInfo.strength + " " + paramsInfo.quality + " ber : " + paramsInfo.ber);

                        String strLocked = (paramsInfo.locked ? "Locked" : "Unlocked") + "  ";
                        String strStrength = "S : " + paramsInfo.strength + "%  ";
                        String strQuality = "Q : " + paramsInfo.quality + "%  ";

                        String str = strLocked;
                        if (paramsInfo.locked) {
                            str += strStrength;
                            str += strQuality;
                        }

                        txvLockStatus.setText(str);
                    }
                }
                break;

                case MW.EVENT_TYPE_DATE_TIME: {
                    mw_data.date_time paramsInfo = (mw_data.date_time) msg.obj;

                    if (paramsInfo != null) {
                        if (dateTime.minute != paramsInfo.minute || dateTime.hour != paramsInfo.hour ||
                                dateTime.day != paramsInfo.day || dateTime.month != paramsInfo.month ||
                                dateTime.year != paramsInfo.year
                        ) {
                            dateTime.year = paramsInfo.year;
                            dateTime.month = paramsInfo.month;
                            dateTime.day = paramsInfo.day;
                            dateTime.hour = paramsInfo.hour;
                            dateTime.minute = paramsInfo.minute;

                            txvDvbTime.setText(Common.getDateTimeAdjustSystem(DtvMainActivity.this, paramsInfo, false));

                            //for refresh current/next epg information every minute
                            UpdateCurrentNextEpgInfo();
                        }
                    }
                }
                break;

                case MW.EVENT_TYPE_EPG_CURRENT_NEXT_UPDATE: {
                    UpdateCurrentNextEpgInfo();
                }
                break;

                case MW.EVENT_TYPE_PVR_STATUS: {
                    switch (sub_event_type) {
                        case MW.PVR_RECORD_STATUS_START: {
                            Log.e(TAG, "PVR start record ");
                            Common.makeText(DtvMainActivity.this, R.string.start_record, Toast.LENGTH_SHORT);
                            txvPvrRecording.setText(getResources().getString(R.string.pvr_recording) + "  " + "00:00:00");
                            bInRecording = true;
                        }
                        break;

                        case MW.PVR_RECORD_STATUS_END: {
                            Log.e(TAG, "PVR stop record ");

                            Common.makeText(DtvMainActivity.this, R.string.start_record, Toast.LENGTH_SHORT);

                            txvPvrRecording.setText("");

                            bInRecording = false;
                            sCurrentRecordStoreDir = null;
                            Common.makeText(DtvMainActivity.this, R.string.record_end, Toast.LENGTH_SHORT);

                            RemoveDeviceIsFullMsg();
                        }
                        break;

                        case MW.PVR_RECORD_STATUS_UPDATE_TIME: {
                            int hour, minute, second;

                            second = msg.arg1 % 60;
                            minute = (msg.arg1 / 60) % 60;
                            hour = msg.arg1 / 3600;

                            Log.e(TAG, "PVR update time : " + msg.arg1);
                            txvPvrRecording.setText(getResources().getString(R.string.pvr_recording) + "  " + String.format("%02d:%02d:%02d", hour, minute, second));
                        }
                        break;

                        case MW.PVR_RECORD_STATUS_ERROR: {
                            switch (msg.arg1) {
                                case MW.RET_REC_ERR_CANNOT_CREATE_THREAD: {
                                    Log.e(TAG, "PVR record error : create thread failed");
                                }
                                break;

                                case MW.RET_REC_ERR_BUSY: {
                                    Log.e(TAG, "PVR record error : busy");
                                }
                                break;

                                case MW.RET_REC_ERR_CANNOT_OPEN_FILE: {
                                    Log.e(TAG, "PVR record error : can not open file");
                                }
                                break;

                                case MW.RET_REC_ERR_CANNOT_WRITE_FILE: {
                                    storageDeviceInfo.refreshDevice();
                                    DeviceItem item = storageDeviceInfo.getDeviceItem(device_no);

                                    if (item != null) {
                                        if (item.spare != null) {
                                            long spareSize = StorageDevice.getSpareSize(item.spare);
                                            if ((spareSize != -1) && (spareSize < 1024 * 1024 * 2)/*item.spare.equals("0K")||item.spare.equals("0M")||item.spare.equals("0B")*/) {
                                                Common.makeText(DtvMainActivity.this, R.string.spaceisfull, Toast.LENGTH_SHORT);
                                            }
                                        }
                                    } else {
                                        Log.e(TAG, "item  == null");
                                    }
                                    SendDeviceIsFullMsg();
                                    Log.e(TAG, "PVR record error : can not write file");
                                }
                                break;

                                case MW.RET_REC_ERR_CANNOT_ACCESS_FILE: {
                                    Log.e(TAG, "PVR record error : can not access file");
                                }
                                break;

                                case MW.RET_REC_ERR_FILE_TOO_LARGE: {
                                    Log.e(TAG, "PVR record error : file too large");
                                }
                                break;

                                case MW.RET_REC_ERR_DVR: {
                                    Log.e(TAG, "PVR record error : dvr error");
                                }
                                break;

                                default:
                                    Log.e(TAG, "PVR record error : unknown error");
                                    break;
                            }
                        }
                        break;

                        default:
                            break;
                    }
                }
                break;
                default:
                    break;
            }
        }
    }

    public void layoutMessagemError(String mensagem, int visibleStatus) {

        findViewById(R.id.mensagemErrorBox).setVisibility(visibleStatus);
        TextView textView = findViewById(R.id.mensagemError);
        textView.setText(mensagem);
    }

    private void noServiceLayout() {

        Intent intent = new Intent(getBaseContext(), NoServices.class);
        startActivity(intent);
        finish();

        /*setContentView(R.layout.teste);

        findViewById(R.id.manual).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        findViewById(R.id.auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bInRecording) {
                    Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                    return;
                }

                ArrayList<Integer> SelAreaList = new ArrayList();
                ArrayList<Integer> SelTpList = new ArrayList();

                Intent in = new Intent();

                SelAreaList.clear();
                SelAreaList.add(MW.SYSTEM_TYPE_DVB_T);
                SelTpList.clear();
                SelTpList.add(0);

                in.setClass(getBaseContext(), ChannelSearchActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("system_type", MW.SYSTEM_TYPE_DVB_T);
                in.putExtra("search_type", MW.SEARCH_TYPE_AUTO);
                in.putIntegerArrayListExtra("search_region_index", SelAreaList);
                in.putIntegerArrayListExtra("search_tp_index", SelTpList);

                startActivity(in);
            }
        });*/

        //Toast.makeText(getBaseContext(), "TT02", Toast.LENGTH_LONG).show();
    }

    private void refreshCurrentSerivceStatus() {
        if (MW.db_get_current_service_info(serviceInfo) == MW.RET_OK) {
            String service_type, service_index;
            int service_count;

            if (serviceInfo.service_type == MW.TV_CH) {
                service_type = "TV";
                // findViewById(R.id.logoRadio).setVisibility(View.VISIBLE);
            } else {
                service_type = "Radio";

            }

            findViewById(R.id.logoRadio)
                    .setVisibility(
                            service_type.equalsIgnoreCase("Radio") ? View.VISIBLE : View.GONE);


            service_count = MW.db_get_service_count(serviceInfo.service_type);
            service_index = "[ " + (serviceInfo.service_index + 1) + "/" + service_count + " ]";

            txvCurrentServiceInfo.setText(service_type + "  " + service_index + "  " + serviceInfo.channel_number_display + "  " + serviceInfo.service_name);

            UpdateCurrentNextEpgInfo();
        }
    }

    private void refreshServiceStatus() {
        MW.ts_player_stop_play();

        txvLockStatus.setText("");
        txvCurrentServiceInfo.setText("");

        MW.ts_player_play_current(true, true);

        if (MW.db_get_current_service_info(serviceInfo) == MW.RET_OK) {
            refreshCurrentSerivceStatus();

            String tvCount = "TV : " + MW.db_get_service_count(MW.TV_CH);
            String radioCount = "   Radio : " + MW.db_get_service_count(MW.RADIO_CH);
            txvServiceCount.setText(tvCount + radioCount);
        }
    }

    private void initData() {
        //videoView
        videoView = (VideoView) findViewById(R.id.VideoView);

        if (mSHCallback != null)
            videoView.getHolder().addCallback(mSHCallback);

        Common.videoViewSetFormat(videoView);

        //tuner status
        txvLockStatus = (TextView) findViewById(R.id.lock_status);

        //play status
        txvPlayStatus = (TextView) findViewById(R.id.play_status);

        //dvb date time
        txvDvbTime = (TextView) findViewById(R.id.dvb_date_time);

        //service count
        txvServiceCount = (TextView) findViewById(R.id.service_count);

        //current service info
        txvCurrentServiceInfo = (TextView) findViewById(R.id.current_service_info);

        //pvr recording
        txvPvrRecording = (TextView) findViewById(R.id.pvr_recording);

        //current epg
        txv_infoBar_currentEpg = (TextView) findViewById(R.id.current_epg);

        //next epg
        txv_infoBar_nextEpg = (TextView) findViewById(R.id.next_epg);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!MW.is_system_supportted(MW.MAIN_TUNER_INDEX, currentSystemType)) {
            showPrompt(this, (R.string.CountNoDetectDemod), Toast.LENGTH_SHORT);

            DtvMainActivity.this.finish();
            return;
        }

        //setContentView(R.layout.dtv_main_activity);
        //mContext = this;


        //initData();

        initLayoutDvt();

        //();
    }

    private Handler handler;
    private Runnable r;

    public void showEPG_Info() {


        final View v = findViewById(R.id.boxEpgInfo);

        if (v == null) {
            return;
        }


        v.setVisibility(View.VISIBLE);

        if (handler != null && r != null) {
            handler.removeCallbacks(r);
        }
        handler = new Handler();

        r = new Runnable() {
            public void run() {
                //tv.append("Hello World");
                v.setVisibility(View.GONE);
                handler.removeCallbacks(this);
            }
        };

        handler.postDelayed(r, 4000);

    }

    public void initLayoutDvt() {


        setContentView(R.layout.dtv_main_activity);
        mContext = this;

        String url = "https://www.google.com";

        // WebView webView = (WebView) findViewById(R.id.wevView);
        // webView.getSettings().setJavaScriptEnabled(true);
        // webView.loadUrl(url);


        initData();
    }

    class LOCALMessageHandler extends Handler {
        public LOCALMessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            int msg_type = msg.what;

            switch (msg_type) {
                case LOCAL_MSG_PLAY_BY_CHANNEL_NUMBER: {
//					PlayByChannelNumber();

                    break;
                }

                case LOCAL_MSG_GET_DEVICE_INFO: {
                    if (storageDeviceInfo != null) {
                        storageDeviceInfo.refreshDevice();
                        DeviceItem item = storageDeviceInfo.getDeviceItem(device_no);
                        if (item != null) {
                            if (item.spare != null) {
                                long spareSize = StorageDevice.getSpareSize(item.spare);
                                if ((spareSize != -1) && (spareSize == 0)/*item.spare.equals("0K")||item.spare.equals("0M")||item.spare.equals("0B")*/) {
                                    Common.makeText(DtvMainActivity.this, getResources().getString(R.string.spaceisfull), Toast.LENGTH_SHORT);
                                }
                            }
                        }

                    }
                    break;
                }

                default:
                    break;
            }
        }
    }


    private class MenuOptionDialog extends Dialog {
        private LayoutInflater mInflater = null;
        private Context mContext = null;
        View mLayoutView = null;
        Button mBtnStart = null;
        Button mBtnCancel = null;
        TextView mTitle = null;
        TextView mContent = null;

        TextView current_date_time;
        protected Looper looper = Looper.myLooper();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.menu_info_dialog);
            mInflater = LayoutInflater.from(mContext);


            current_date_time = findViewById(R.id.current_time);


            findViewById(R.id.EPG_info).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bInRecording) {
                        Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                        return;
                    }

                    showEPG_Screen();
                }
            });


            new MWmessageHandler(looper);
        }

        public MenuOptionDialog(Context context, int theme) {
            super(context, theme);
            mContext = context;
        }


        class MWmessageHandler extends Handler {
            public MWmessageHandler(Looper looper) {
                super(looper);
            }

            @Override
            public void handleMessage(Message msg) {
                int event_type = (msg.what >> 16) & 0xFFFF;
                int sub_event_type = msg.what & 0xFFFF;

                switch (event_type) {
                    case MW.EVENT_TYPE_DATE_TIME: {
                        mw_data.date_time paramsInfo = (mw_data.date_time) msg.obj;

                        if (paramsInfo != null) {
                            current_date_time.setText(Common.getDateTimeAdjustSystem(getBaseContext(), paramsInfo, true));

                            //		Log.e(TAG,String.format("%04d-%02d-%02d    %d", date.year, date.month, date.day, date.weekNo));
                            //		Log.e(TAG,(String.format("%04d-%02d-%02d   ", paramsInfo.year, paramsInfo.month, paramsInfo.day)));


                        }
                    }
                    break;

                    default:
                        break;
                }
            }
        }

    }

    private MenuOptionDialog menuOptionDialog = null;

    private void SendDeviceIsFullMsg() {
        localMsgHandler.sendMessageDelayed(localMsgHandler.obtainMessage(LOCAL_MSG_GET_DEVICE_INFO), 2000);
    }

    private void RemoveDeviceIsFullMsg() {
        localMsgHandler.removeMessages(LOCAL_MSG_GET_DEVICE_INFO);

    }

    private void DoRecordFunction(int device_no, int record_time, int service_type, int service_index) {
        Log.e(TAG, "DoRecordFunction record_time = " + record_time);

        if (record_time >= 0) {
            storageDeviceInfo.refreshDevice();
            String spare = storageDeviceInfo.getDeviceItem(device_no).spare;
            if (spare != null) {
                long spareSize = StorageDevice.getSpareSize(spare);
                if ((spareSize != -1) && (spareSize < 1024 * 1024 * 2)/*spare.equals("0K")||spare.equals("0M")||spare.equals("0B")*/) {
                    Common.makeText(DtvMainActivity.this, R.string.spaceisfull, Toast.LENGTH_SHORT);
                    return;
                }
            }

            int ret = MW.pvr_record_start(0, service_type, service_index, record_time, storageDeviceInfo.getDeviceItem(device_no).Path, null);

            if (ret == MW.RET_OK) {
                sCurrentRecordStoreDir = new String(storageDeviceInfo.getDeviceItem(device_no).Path);
                bInRecording = true;
                Common.makeText(DtvMainActivity.this, R.string.start_record, Toast.LENGTH_SHORT);
                SendDeviceIsFullMsg();
                Log.e(TAG, "start recording....");
            } else if (ret == MW.RET_ALREADY_RECORDING) {
                Log.e(TAG, "already recording....");
                bInRecording = true;
                Common.makeText(DtvMainActivity.this, R.string.already_record, Toast.LENGTH_SHORT);
            } else if (ret == MW.RET_NO_THIS_CHANNEL) {
                Log.e(TAG, "record failed, not this channel....");
                Common.makeText(DtvMainActivity.this, R.string.record_failed_no_this_channel, Toast.LENGTH_SHORT);

            } else if (ret == MW.RET_INVALID_OBJECT) {
                Log.e(TAG, "record failed, invalid object....");
                Common.makeText(DtvMainActivity.this, R.string.record_failed_invalid_object, Toast.LENGTH_SHORT);
            }
        }
    }

    public class StorageDeviceDialog extends Dialog {

        private LayoutInflater mInflater = null;
        private CustomListView mListView = null;
        private Context mContext = null;
        View mLayoutView = null;
        private CharSequence[] mListItem;
        private ArrayAdapter mAdapter;
        private int temp_no = 0;
        List<String> items;

        private void refreshStorageDevice() {
            items.clear();
            if (storageDeviceInfo != null) {
                if (storageDeviceInfo.getDeviceCount() > 0) {
                    for (int i = 0; i < storageDeviceInfo.getDeviceCount(); i++) {
                        StorageDevice.DeviceItem item = storageDeviceInfo.getDeviceItem(i);
                        //				Log.e(TAG,"device path: "+item.Path+" device format: "+item.format+"  name: "+item.VolumeName+" spare : "+item.spare+" total: "+item.total);
                        if (item.VolumeName != null)
                            if (item.VolumeName.indexOf("[udisk") == 1)
                                items.add(" " + getResources().getString(R.string.udisk) + " " + (storageDeviceInfo.getDeviceCount() - 1 - i));
                            else
                                items.add(item.VolumeName + "");
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    this.dismiss();
                }
            } else {
                this.dismiss();
            }
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            TextView txv_Title;

            super.onCreate(savedInstanceState);
            setContentView(R.layout.storagedevice_sel_dialog);

            dia_currentDialog = this;

            txv_Title = (TextView) findViewById(R.id.TextView_Title);
            txv_Title.setText(getResources().getString(R.string.SelStorageDevice));

            mListView = (CustomListView) findViewById(R.id.lvListItem);

            mInflater = LayoutInflater.from(mContext);
            mLayoutView = mInflater.inflate(R.layout.single_selection_list_item, null);

            items = new ArrayList<String>();
            items.clear();

            for (int i = 0; i < storageDeviceInfo.getDeviceCount(); i++) {
                StorageDevice.DeviceItem item = storageDeviceInfo.getDeviceItem(i);
//				Log.e(TAG,"device path: "+item.Path+" device format: "+item.format+"  name: "+item.VolumeName+" spare : "+item.spare+" total: "+item.total);
                if (item.VolumeName != null)
                    if (item.VolumeName.indexOf("[udisk") == 1)
                        items.add(" " + getResources().getString(R.string.udisk) + " " + (storageDeviceInfo.getDeviceCount() - 1 - i));
                    else
                        items.add(item.VolumeName + "");
            }
            mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, items);
            mListView.setAdapter(mAdapter);
            mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            mListView.setItemChecked(0, true);
            mListView.setCustomSelection(0);
            mListView.setVisibleItemCount(5);
            mListView.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Log.e(TAG, "mListView position =" + position);
                    temp_no = position;
                    mListView.setItemChecked(position, true);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mListView.setCustomKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_CENTER: {
                                device_no = temp_no;
                                Log.e(TAG, "device_no OK  :" + device_no);
                                if ((MW.db_get_current_service_info(serviceInfo) == MW.RET_OK) && (device_no != -1)) {
                                    DoRecordFunction(device_no, 0, serviceInfo.service_type, serviceInfo.service_index);
                                } else {
                                    Common.makeText(DtvMainActivity.this, R.string.no_service_cant_record, Toast.LENGTH_SHORT);
                                }
                                StorageDeviceDialog.this.dismiss();
                                return true;
                            }

                            default:
                                break;
                        }
                    }
                    return false;
                }
            });

        }

        @Override
        protected void onStop() {
            super.onStop();

            MW.ts_player_require_play_status();

            dia_currentDialog = null;
        }

        public StorageDeviceDialog(Context context, int theme) {
            super(context, theme);
            mContext = context;
        }

        public StorageDeviceDialog(Context context) {
            super(context);
            mContext = context;
        }


    }

    public class AudioInfoDialog extends Dialog {
        private LayoutInflater mInflater = null;
        private CustomListView mListView = null;
        private Context mContext = null;
        int nTrack;
        View mLayoutView = null;
        TextView mAudioTrack = null;
        private CharSequence[] mListItem;
        private ArrayAdapter mAdapter;
        private int audioChannel;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.audio_info_dialog);

            dia_currentDialog = this;

            mListView = (CustomListView) findViewById(R.id.lvListItem);
            mAudioTrack = (TextView) findViewById(R.id.info);

            audioChannel = serviceInfo.audio_channel;

            switch (audioChannel) {
                case MW.AUDIO_CHANNEL_LEFT:
                    mAudioTrack.setText(R.string.left);
                    break;
                case MW.AUDIO_CHANNEL_RIGHT:
                    mAudioTrack.setText(R.string.right);
                    break;
                default:
                case MW.AUDIO_CHANNEL_STEREO:
                    mAudioTrack.setText(R.string.stereo);
                    break;
            }

            mInflater = LayoutInflater.from(mContext);
            mLayoutView = mInflater.inflate(R.layout.single_selection_list_item, null);

            if (serviceInfo.audio_info.length > 0) {
                String audio_type;
                List<String> languages;
                int i;

                languages = new ArrayList<String>();

                languages.clear();
                for (i = 0; i < serviceInfo.audio_info.length; i++) {
                    audio_type = getResources().getStringArray(R.array.audio_type)[serviceInfo.audio_info[i].audio_stream_type];
                    if ((serviceInfo.audio_info[i].ISO_639_language_code.length() > 0) && (serviceInfo.audio_info[i].ISO_639_language_code.charAt(0) != '\0'))
                        languages.add(serviceInfo.audio_info[i].ISO_639_language_code + " (" + audio_type + ")");
                    else
                        languages.add("Audio " + (i + 1) + " (" + audio_type + ")");

                }

                mAdapter = new ArrayAdapter(DtvMainActivity.this, R.layout.single_selection_list_item, R.id.ctvListItem, languages);
                mListView.setAdapter(mAdapter);

                mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                mListView.setItemChecked(serviceInfo.audio_index, true);
                mListView.setCustomSelection(serviceInfo.audio_index);
                mListView.setVisibleItemCount(5);
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MW.ts_player_play_switch_audio_pid(position);
                        AudioInfoDialog.this.dismiss();
                    }
                });
            }

        }

        @Override
        protected void onStop() {
            super.onStop();

            MW.ts_player_require_play_status();

            dia_currentDialog = null;
        }

        public AudioInfoDialog(Context context, int theme) {
            super(context, theme);
            mContext = context;
        }

        public AudioInfoDialog(Context context) {
            super(context);
            mContext = context;
        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_LEFT: {
                    if (audioChannel == MW.AUDIO_CHANNEL_LEFT) {
                        audioChannel = MW.AUDIO_CHANNEL_STEREO;
                        mAudioTrack.setText(R.string.stereo);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_STEREO);
                    } else if (audioChannel == MW.AUDIO_CHANNEL_RIGHT) {
                        audioChannel = MW.AUDIO_CHANNEL_LEFT;
                        mAudioTrack.setText(R.string.left);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_LEFT);
                    } else {
                        audioChannel = MW.AUDIO_CHANNEL_RIGHT;
                        mAudioTrack.setText(R.string.right);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_RIGHT);
                    }
                }
                return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT: {
                    if (audioChannel == MW.AUDIO_CHANNEL_LEFT) {
                        audioChannel = MW.AUDIO_CHANNEL_RIGHT;
                        mAudioTrack.setText(R.string.right);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_RIGHT);
                    } else if (audioChannel == MW.AUDIO_CHANNEL_RIGHT) {
                        audioChannel = MW.AUDIO_CHANNEL_STEREO;
                        mAudioTrack.setText(R.string.stereo);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_STEREO);
                    } else {
                        audioChannel = MW.AUDIO_CHANNEL_LEFT;
                        mAudioTrack.setText(R.string.left);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_LEFT);
                    }
                }
                return true;

                case KeyEvent.KEYCODE_2:
                    this.dismiss();
                    return true;

                default:
                    break;
            }

            return super.onKeyDown(keyCode, event);
        }
    }

    public static void showPrompt(Context context, int resId, int duration) {
        LayoutInflater inflater_toast = LayoutInflater.from(context);
        LinearLayout toast_view = (LinearLayout) inflater_toast.inflate(R.layout.toast_main, null);
        TextView toast_tv = (TextView) toast_view.findViewById(R.id.toast_text);
        toast_tv.setText(resId);

        Toast toast = new Toast(context);
        toast.setView(toast_view);
        toast.setDuration(duration);
        toast.setGravity(Gravity.CENTER, 0, -70);
        toast.show();
    }

    public static void setWindow_Attributes(Dialog dialog) {
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();

        lp.dimAmount = 0.0f;

        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }


    private class MenuOptionNavDialog extends Dialog implements View.OnFocusChangeListener {
        private LayoutInflater mInflater = null;
        private Context mContext = null;
        View mLayoutView = null;
        Button mBtnStart = null;
        Button mBtnCancel = null;
        TextView mTitle = null;
        TextView mContent = null;

        TextView current_date_time;
        protected Looper looper = Looper.myLooper();

        FrameLayout frameLayout;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.menu_tv);
            mInflater = LayoutInflater.from(mContext);

            frameLayout = findViewById(R.id.conteudo_mostrado);

            findViewById(R.id.boxBt1).setOnFocusChangeListener(this);
            findViewById(R.id.boxBt2).setOnFocusChangeListener(this);
            findViewById(R.id.boxBt3).setOnFocusChangeListener(this);
            findViewById(R.id.boxBt4).setOnFocusChangeListener(this);


            /*findViewById(R.id.boxBt2).setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if (hasFocus) {
                        findViewById(R.id.boxMenu2).setVisibility(View.VISIBLE);

                    }
                }
            });*/


            findViewById(R.id.sinAuto).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    autoScan();
                    dismiss();
                }
            });

            findViewById(R.id.sinManual).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    manualScan();
                    dismiss();
                }
            });


            findViewById(R.id.epg).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEPG_Screen();
                }
            });


            findViewById(R.id.vetTV).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchTVRadioBtClick();
                    dismiss();
                }
            });

            findViewById(R.id.ouvirRadio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchTVRadioBtClick();
                    dismiss();
                }
            });


            findViewById(R.id.recordTT).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startStopPvrRecord();
                    dismiss();
                }
            });

            findViewById(R.id.recordList).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pvrRecordedList();
                    dismiss();
                }
            });

            findViewById(R.id.deleteAServices).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteAllServices();
                }
            });

            findViewById(R.id.resetDTVDataK).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetData();
                }
            });


            //current_date_time = findViewById(R.id.current_time);


            /*findViewById(R.id.EPG_info).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bInRecording) {
                        Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                        return;
                    }

                    showEPG_Screen();
                }
            });*/


            //new MWmessageHandler(looper);
        }


        private CustomListView channel_list;
        private int mFirstVisibleItem, mVisibleItemCount;
        private EPGChannelListAdapter epgChannelListAdapter;

        boolean fisttime_enter = true;
        private int current_service_type;
        private int current_service_index;
        private int tmp_current_service_index;


        private void initChannelList() {

            fisttime_enter = true;

            channel_list = (CustomListView) findViewById(R.id.tv_back_channles);
            //channel_list.requestFocus();
            channel_list.setNextFocusLeftId(R.id.boxBt3);
            channel_list.setCustomScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    mFirstVisibleItem = firstVisibleItem;
                    mVisibleItemCount = visibleItemCount;
                }

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }
            });

            channel_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if (MW.db_get_service_info(current_service_type, position, serviceInfo) == MW.RET_OK) {
//					current_program.setText(serviceInfo.service_name);
                        if (!fisttime_enter) {
                            int Ret = MW.ts_player_play(current_service_type, position, CHANGE_CHANNEL_DELAYED_TIME, true);
                        } else {
                            fisttime_enter = false;
                        }
                        current_service_index = serviceInfo.service_index;
                        //epgChannelListAdapter.notifyDataSetChanged();

                        //bNeedShow = true;
                        //UpdateScheduleEpgInfo(true);

                    }

                }
            });

            /*channel_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                    if (MW.db_get_service_info(current_service_type, position, serviceInfo) == MW.RET_OK) {
//					current_program.setText(serviceInfo.service_name);
                        if (!fisttime_enter) {
                            int Ret = MW.ts_player_play(current_service_type, position, CHANGE_CHANNEL_DELAYED_TIME, true);
                        } else {
                            fisttime_enter = false;
                        }
                        current_service_index = serviceInfo.service_index;
                        //epgChannelListAdapter.notifyDataSetChanged();

                        //bNeedShow = true;
                        //UpdateScheduleEpgInfo(true);

                    }
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });*/


            channel_list.setCustomKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
//					System.out.println("EPG channel_list keycode == "+keyCode);

                    } else {

                    }
                    return false;
                }
            });

            epgChannelListAdapter = new EPGChannelListAdapter(getBaseContext());
            channel_list.setAdapter(epgChannelListAdapter);
            channel_list.setVisibleItemCount(12);

            channel_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


            //epgchannel_type = (TextView) findViewById(R.id.channeltype_txt);

            //RefreshShowEpgChannelList();

        }


        private class EPGChannelListAdapter extends BaseAdapter {


            private Context mContext;
            private LayoutInflater mInflater;
            private mw_data.service serviceInfo;


            public EPGChannelListAdapter(Context context) {
                super();
                this.mContext = context;
                mInflater = LayoutInflater.from(context);

                serviceInfo = new mw_data.service();

                if (MW.db_get_current_service_info(serviceInfo) == MW.RET_OK) {
                    current_service_type = serviceInfo.service_type;
                    current_service_index = serviceInfo.service_index;
                    tmp_current_service_index = serviceInfo.service_index;
                }

            }

            @Override
            public int getCount() {
                return MW.db_get_service_count(current_service_type);
            }

            @Override
            public Object getItem(int pos) {
                return pos;
            }

            @Override
            public long getItemId(int pos) {
                return pos;
            }

            @Override
            public View getView(int pos, View convertView, ViewGroup parent) {
                EPGChannelListAdapter.ViewHolder localViewHolder;
//			System.out.println("getView  pos = "+pos);
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.epg_activity_channel_list_item, null);
                    localViewHolder = new EPGChannelListAdapter.ViewHolder();
                    localViewHolder.channel_num = (TextView) convertView.findViewById(R.id.channellist_num_txt);
                    localViewHolder.channel_name = (TextView) convertView.findViewById(R.id.channellist_name_txt);
                    localViewHolder.channel_quality_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_sharp_img);
                    localViewHolder.fav_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_fav_img);
                    convertView.setTag(localViewHolder);
                } else {
                    localViewHolder = (EPGChannelListAdapter.ViewHolder) convertView.getTag();
                }

                Drawable drawable = mContext.getResources().getDrawable(R.drawable.selector_item);
                convertView.setBackgroundDrawable(drawable);

//			System.out.println("current_service_type : "+ current_service_type);
//			System.out.println("pos : "+ pos);


                if (MW.db_get_service_info(current_service_type, pos, serviceInfo) == MW.RET_OK) {
                    localViewHolder.channel_num.setText(Integer.toString(serviceInfo.channel_number_display));
                    localViewHolder.channel_name.setText(serviceInfo.service_name);

                    if (serviceInfo.is_hd == MW.VIDEO_RESOLUTION_TYPE_HD)
                        localViewHolder.channel_quality_icon.setImageResource(R.drawable.imagehd1);
                    else if (serviceInfo.is_hd == MW.VIDEO_RESOLUTION_TYPE_4K)
                        localViewHolder.channel_quality_icon.setImageResource(R.drawable.image4k1);
                    else
                        localViewHolder.channel_quality_icon.setImageResource(Color.TRANSPARENT);

                    if (serviceInfo.is_scrambled)
                        localViewHolder.fav_icon.setImageResource(R.drawable.coin_us_dollar);
                    else
                        localViewHolder.fav_icon.setImageResource(Color.TRANSPARENT);

                }

                return convertView;
            }

            class ViewHolder {
                TextView channel_num;
                TextView channel_name;
                ImageView channel_quality_icon;
                ImageView fav_icon;
            }
        }

        public MenuOptionNavDialog(Context context, int theme) {
            super(context, theme);
            mContext = context;
        }

        private void loadFragment(Object fragment, int id) {


        }


        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (v.getId() == R.id.boxBt2) {
                if (hasFocus) {
                    findViewById(R.id.boxMenu2).setVisibility(View.VISIBLE);
                    findViewById(R.id.boxMenu1).setVisibility(View.GONE);
                    findViewById(R.id.boxMenu4).setVisibility(View.GONE);
                    findViewById(R.id.boxMenu).setVisibility(View.GONE);
                }
            } else {
                if (v.getId() == R.id.boxBt1) {
                    if (hasFocus) {
                        findViewById(R.id.boxMenu2).setVisibility(View.GONE);
                        findViewById(R.id.boxMenu1).setVisibility(View.VISIBLE);
                        //findViewById(R.id.boxMenu).setVisibility(View.VISIBLE);
                        findViewById(R.id.boxMenu4).setVisibility(View.GONE);
                        findViewById(R.id.boxMenu).setVisibility(View.GONE);

                        //
                    }
                } else {

                    if (v.getId() == R.id.boxBt4) {
                        if (hasFocus) {
                            findViewById(R.id.boxMenu2).setVisibility(View.GONE);
                            findViewById(R.id.boxMenu1).setVisibility(View.GONE);
                            findViewById(R.id.boxMenu4).setVisibility(View.VISIBLE);
                            findViewById(R.id.boxMenu).setVisibility(View.GONE);
                        }
                    } else {

                        if (v.getId() == R.id.boxBt3) {
                            if (hasFocus) {
                                findViewById(R.id.boxMenu2).setVisibility(View.GONE);
                                findViewById(R.id.boxMenu1).setVisibility(View.GONE);
                                findViewById(R.id.boxMenu4).setVisibility(View.GONE);
                                findViewById(R.id.boxMenu).setVisibility(View.VISIBLE);

                                initChannelList();
                            }
                        } else {


                        }

                    }

                }
            }
        }
    }

    private MenuOptionNavDialog menuOptionNavDialog = null;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {

            case KeyEvent.KEYCODE_GUIDE:
            case KeyEvent.KEYCODE_MENU:
            case KeyEvent.KEYCODE_BOOKMARK:
                Toast.makeText(getBaseContext(), "Menu2", Toast.LENGTH_LONG).show();

                return true;


            case KeyEvent.KEYCODE_DPAD_CENTER:
                // click enter
            {

                /*menuOptionDialog = new MenuOptionDialog(this, R.style.MyDialog);
                menuOptionDialog.show();*/

                if (MW.db_get_total_service_count() > 0) {
                    menuOptionNavDialog = new MenuOptionNavDialog(this, R.style.MyDialog);
                    menuOptionNavDialog.show();
                    Toast.makeText(getBaseContext(), "sdfsd", Toast.LENGTH_LONG).show();
                }

            }

            return true;

            case KeyEvent.KEYCODE_M: {

                if (MW.db_get_total_service_count() > 0) {
                    menuOptionNavDialog = new MenuOptionNavDialog(this, R.style.MyDialog);
                    menuOptionNavDialog.show();
                    Toast.makeText(getBaseContext(), "sdfsd", Toast.LENGTH_LONG).show();
                }

            }

            return true;

            case KeyEvent.KEYCODE_1:
                //EPG
            {

                showEPG_Screen();
            }
            return true;

            case KeyEvent.KEYCODE_2:
                //Switch audio
                if (bInRecording) {
                    Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                    return true;
                }

                if (MW.db_get_current_service_info(serviceInfo) == MW.RET_OK) {
                    AudioInfoDialog audioInfoDia = new AudioInfoDialog(this, R.style.MyDialog);
                    audioInfoDia.show();
                    setWindow_Attributes(audioInfoDia);
                } else {
                    showPrompt(this, (R.string.NoServiceCanNotSwitchAudio), Toast.LENGTH_SHORT);
                }
                return true;

            case KeyEvent.KEYCODE_3:

                //Switch TV/Radio
                switchTVRadioBtClick();

                return true;

            case KeyEvent.KEYCODE_4:
                //Auto Scan
            {
                autoScan();
            }

            return true;

            case KeyEvent.KEYCODE_5:
                //Manual Scan
            {
                manualScan();
            }

            return true;

            case KeyEvent.KEYCODE_6:
                //Start/Stop PVR Record
            {
                if (event.getRepeatCount() != 0)
                    return super.onKeyDown(keyCode, event);

                startStopPvrRecord();
            }

            return true;

            case KeyEvent.KEYCODE_7:
                //PVR Record List
                pvrRecordedList();

                return true;

            case KeyEvent.KEYCODE_8:
                //Delete all services

                deleteAllServices();

                return true;

            case KeyEvent.KEYCODE_9:
                //Reset DTV data
                resetData();

                return true;

            case KeyEvent.KEYCODE_DPAD_UP:
                //Switch play service up
                if (bInRecording) {
                    Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                    return true;
                }


                if (MW.db_get_total_service_count() > 0) {
                    MW.ts_player_play_up(CHANGE_CHANNEL_DELAYED_TIME, true);

                    refreshCurrentSerivceStatus();
                    showEPG_Info();
                } else {
                    showPrompt(this, (R.string.NoAnyServiceScanFirst), Toast.LENGTH_SHORT);
                }
                return true;

            case KeyEvent.KEYCODE_DPAD_DOWN:
                //Switch play service down
                if (bInRecording) {
                    Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                    return true;
                }


                if (MW.db_get_total_service_count() > 0) {
                    MW.ts_player_play_down(CHANGE_CHANNEL_DELAYED_TIME, true);

                    refreshCurrentSerivceStatus();
                    showEPG_Info();
                } else {
                    showPrompt(this, (R.string.NoAnyServiceScanFirst), Toast.LENGTH_SHORT);
                }
                return true;

            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_LEFT:
                //Switch TV/Radio
                if (bInRecording) {
                    Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                    return true;
                }

                switchTVRadio();
                return true;

            case KeyEvent.KEYCODE_BACK:
                if (bInRecording) {
                    Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
                    return true;
                }

                break;

            default:
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void resetData() {

        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        MW.db_load_default_data();
        refreshServiceStatus();
        showPrompt(this, (R.string.DTVDataReset), Toast.LENGTH_SHORT);

    }

    private void deleteAllServices() {

        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        if (MW.db_delete_all_service())
            showPrompt(this, (R.string.DeleteAllServiceSucceed), Toast.LENGTH_SHORT);
        else
            showPrompt(this, (R.string.DeleteAllServiceFailed), Toast.LENGTH_SHORT);

        refreshServiceStatus();
    }

    private void pvrRecordedList() {

        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        if (storageDeviceInfo != null) {
            if (storageDeviceInfo.getDeviceCount() > 0) {
                Intent in_pvr = new Intent();
                in_pvr.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in_pvr.setClass(this, RecordListActivity.class);
                startActivity(in_pvr);
            } else {
                Common.makeText(DtvMainActivity.this, R.string.no_storage, Toast.LENGTH_SHORT);
            }
        }

    }

    private void startStopPvrRecord() {

        if (!bInRecording) {
            if (MW.db_get_total_service_count() <= 0) {
                return;
            }
            if (storageDeviceInfo != null) {
                if (storageDeviceInfo.getDeviceCount() > 0) {
                    Log.e(TAG, "have device : " + storageDeviceInfo.getDeviceCount());
                    if (storageDeviceInfo.getDeviceCount() > 1) {
                        if ((MW.db_get_current_service_info(serviceInfo) == MW.RET_OK)) {
                            if (enableRecord == true) {
                                DeviceInfoDia = new StorageDeviceDialog(this, R.style.MyDialog);
                                DeviceInfoDia.show();
                                setWindow_Attributes(DeviceInfoDia);
                            } else {
                                Common.makeText(DtvMainActivity.this, R.string.channel_no_play_cant_record, Toast.LENGTH_SHORT);
                            }

                        } else {
                            Common.makeText(DtvMainActivity.this, R.string.no_service_cant_record, Toast.LENGTH_SHORT);
                        }
                    } else {
                        device_no = 0;
                        if ((MW.db_get_current_service_info(serviceInfo) == MW.RET_OK)) {
                            if (enableRecord == true) {
                                DoRecordFunction(device_no, 0, serviceInfo.service_type, serviceInfo.service_index);
                            } else {
                                Common.makeText(DtvMainActivity.this, R.string.channel_no_play_cant_record, Toast.LENGTH_SHORT);
                            }
                        } else {
                            Common.makeText(DtvMainActivity.this, R.string.no_service_cant_record, Toast.LENGTH_SHORT);
                        }
                    }
                } else {
                    Common.makeText(DtvMainActivity.this, R.string.no_storage, Toast.LENGTH_SHORT);
                }
            }
        } else {
            RemoveDeviceIsFullMsg();
            MW.pvr_record_stop(0);
        }
    }

    private void switchTVRadioBtClick() {

        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        switchTVRadio();
    }

    private void manualScan() {

        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        Intent in_ant = new Intent();
        in_ant.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in_ant.setClass(this, Ter_ManualSearchActivity.class);
        in_ant.putExtra("system_type", MW.SYSTEM_TYPE_DVB_T);

        startActivity(in_ant);

    }

    private void autoScan() {
        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        ArrayList<Integer> SelAreaList = new ArrayList();
        ArrayList<Integer> SelTpList = new ArrayList();

        Intent in = new Intent();

        SelAreaList.clear();
        SelAreaList.add(MW.SYSTEM_TYPE_DVB_T);
        SelTpList.clear();
        SelTpList.add(0);

        in.setClass(this, ChannelSearchActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.putExtra("system_type", MW.SYSTEM_TYPE_DVB_T);
        in.putExtra("search_type", MW.SEARCH_TYPE_AUTO);
        in.putIntegerArrayListExtra("search_region_index", SelAreaList);
        in.putIntegerArrayListExtra("search_tp_index", SelTpList);

        startActivity(in);
    }

    private void showEPG_Screen() {

        if (bInRecording) {
            Common.makeText(DtvMainActivity.this, R.string.pvr_recording_stop_first, Toast.LENGTH_SHORT);
            return;
        }

        Intent in = new Intent();
        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.setClass(DtvMainActivity.this, EPGActivity.class);
        startActivity(in);
    }

    private void switchTVRadio() {

        switch (MW.ts_player_play_switch_tv_radio(true)) {
            case MW.RET_NO_AUDIO_CHANNEL:
                showPrompt(this, (R.string.NO_AUDIO_CHANNEL), Toast.LENGTH_SHORT);
                break;

            case MW.RET_NO_VIDEO_CHANNEL:
                showPrompt(this, (R.string.NO_VIDEO_CHANNEL), Toast.LENGTH_SHORT);
                break;

            case MW.RET_NO_CHANNEL:
                showPrompt(this, (R.string.NO_CHANNEL), Toast.LENGTH_SHORT);
                break;

            case MW.RET_OK:
            default:
                refreshCurrentSerivceStatus();
                showEPG_Info();
                break;
        }

    }

    boolean isFirtInit = true;

    @Override
    public void onResume() {
        super.onResume();

        bInRecording = false;

        if (!isFirtInit)
            initLayoutDvt();

        isFirtInit = false;

        mwMsgHandler = new MWmessageHandler(looper);

        enableMwMessageCallback(mwMsgHandler);
        MW.register_event_type(MW.EVENT_TYPE_PLAY_STATUS, true);
        MW.register_event_type(MW.EVENT_TYPE_TUNER_STATUS, true);
        MW.register_event_type(MW.EVENT_TYPE_DATE_TIME, true);
        MW.register_event_type(MW.EVENT_TYPE_EPG_CURRENT_NEXT_UPDATE, true);

        storageDeviceInfo = new StorageDevice(this);
        localMsgHandler = new LOCALMessageHandler(looper);

        storageDevicePlugReceiver = new StorageDevicePlugReceiver();
        IntentFilter filter = new IntentFilter();

        filter.addAction(StorageDevice.ACTION_DEVICE_MOUNTED);
        filter.addAction(StorageDevice.ACTION_DEVICE_REMOVED);
        filter.addAction(PlatformSpecial.ACTION_HDMI_PLUGGED);
        registerReceiver(storageDevicePlugReceiver, filter);

        PlatformSpecial.init(getApplicationContext());

        refreshServiceStatus();
    }

    @Override
    public void onPause() {
        super.onPause();

        enableMwMessageCallback(null);

        if (storageDeviceInfo != null)
            storageDeviceInfo.finish(this);

        if (storageDevicePlugReceiver != null) {
            unregisterReceiver(storageDevicePlugReceiver);
            storageDevicePlugReceiver = null;
        }

        if (bInRecording) {
            bInRecording = false;
            RemoveDeviceIsFullMsg();
            MW.pvr_record_stop(0);
            txvPvrRecording.setText("");
        }

        if (dia_currentDialog != null) {
            dia_currentDialog.dismiss();
            dia_currentDialog = null;
        }

        MW.ts_player_stop_play();
        PlatformSpecial.deinit();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        /*if (!isTaskRoot()) {
            Toast.makeText(getBaseContext(), "asasfaf", Toast.LENGTH_LONG).show();
            super.onBackPressed();
        }else{




            Intent intent = null;
            try {
                intent = new Intent(this,
                        Class.forName("com.riftone.riftonekhadas.ui.MainActivityWithMenuLeft"));
                startActivity(intent);
                finish();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }*/

        /*if(){
            return;
        }*/

    }
}
