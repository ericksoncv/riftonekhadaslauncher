package com.superdtv;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;


import android.os.RemoteException;


import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.media.MediaPlayer;

import java.io.InputStream;
import java.util.HashMap;
import java.io.File;
import java.io.FileInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DtvBaseActivity extends Activity{
	private static final String TAG = "DtvBaseActivity";

    //get the Looper object of the main thread
    protected Looper looper = Looper.myLooper();
    
	protected SurfaceHolder.Callback mSHCallback = null;
    Handler mwMessageHandler;

    protected void enableMwMessageCallback(Handler h) {
    	mwMessageHandler = h;

		if(h != null){
			MW.register_event_cb(this);	
		}
    }

    public void onMwEventCallback(int event_type, int sub_event_type, int param1, int param2, Object obj_clazz) {
//		Log.e(" DtvBaseActivity ", " mwEventCallback : " + event_type + " " + sub_event_type);	
		if(mwMessageHandler != null){
			Message messageSend = mwMessageHandler.obtainMessage();

			messageSend.what = event_type << 16 | sub_event_type;
			messageSend.arg1 = param1;
			messageSend.arg2 = param2;
			messageSend.obj = obj_clazz;
            
			messageSend.sendToTarget();	  	
		}
    }		


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
				
		if( Integer.valueOf(android.os.Build.VERSION.SDK)< 24 ){
			//running on old android version below android 7.0
			mSHCallback = new SurfaceHolder.Callback() {
				public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
					Log.d(TAG, "surfaceChanged");
					try{
					}catch(Exception e){
					}
				}
				public void surfaceCreated(SurfaceHolder holder) {
					Log.d(TAG, "surfaceCreated");
					try{
						initSurface(holder);			
					}catch(Exception e){
					}
				}
				public void surfaceDestroyed(SurfaceHolder holder) {
					Log.d(TAG, "surfaceDestroyed");
					releaseSurface();
				}
			};				
		}
		
        Log.d(" DtvBaseActivity ", "onCreate "); 
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        releaseSurface();		

        Log.d(" DtvBaseActivity ", "onDestroy "); 
    }

	private MediaPlayer mMediaPlayer = null;

	private void initSurface(SurfaceHolder holder) {
		Log.d(TAG, "[initSurface]mSurfaceHolder:" + holder);
		if (holder == null) {
			return;
		}

		if(Integer.valueOf(android.os.Build.VERSION.SDK)>21){
			releaseSurface();

			mMediaPlayer = new MediaPlayer();
			if(mMediaPlayer==null)
				return;

			try{
				mMediaPlayer.setDataSource("tvin:test");
			}catch(Exception e){
				Log.e(TAG, e.toString());
			}
			try{
				mMediaPlayer.prepare();
				mMediaPlayer.start();
				mMediaPlayer.setDisplay(holder);
			}catch(Exception e){
				Log.e(TAG, e.toString());
			}
			Log.d(TAG, "initSurface : player start:"+mMediaPlayer);
		}
		else{
			Canvas c = null;
			try {
				Log.d(TAG, "initSurface");
				c = holder.lockCanvas();
			} finally {
				if (c != null)
					holder.unlockCanvasAndPost(c);
			}
		}
	}

	private void releaseSurface() {
		if(Integer.valueOf(android.os.Build.VERSION.SDK)>21){
			if (mMediaPlayer != null) {
				Log.d(TAG, "releaseSurface : player release:"+mMediaPlayer);
				mMediaPlayer.reset();
				mMediaPlayer.release();
				mMediaPlayer = null;
			}
		}
	}	
}

