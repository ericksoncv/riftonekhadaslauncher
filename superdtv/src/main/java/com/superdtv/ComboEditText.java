package com.superdtv;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
//title textview  + info textview 
public class ComboEditText extends LinearLayout implements OnFocusChangeListener{
	private final static String TAG = "ComboEditText";	
	private Context mContext;
	
	protected TextView mTitleTextView,unit_textview;
	protected DigitsEditText mInfoTextView;
	private int mTitleResId = 0;
	private String mEditString = null; 
	private TextWatcher mListener = null;
	
	public ComboEditText(Context context) 
	{
		this(context, null);
	}

	public ComboEditText(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		mContext = context;
		LayoutInflater layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.combo_edit_text, this);

	}

	@Override
	protected void onFinishInflate() 
	{
		super.onFinishInflate();
		
        mTitleTextView = (TextView)findViewById(R.id.title_textview);
        unit_textview = (TextView)findViewById(R.id.unit_textview);
		mInfoTextView = (DigitsEditText)findViewById(R.id.info_textview);
		mInfoTextView.setOnFocusChangeListener(this);
	}

	public TextView getTitleTextView()
	{
		return mTitleTextView;
	}

	public DigitsEditText getInfoTextView()
	{
		return mInfoTextView;
	}
	
	public void initView(int titleResId, String eidtString, String unit, TextWatcher listener) 
	{
		mTitleResId = titleResId;
		if (eidtString != null)
		{
			mEditString =  eidtString;
		}
		if(listener!=null)
		{
			mListener = listener;
		}
		
		if (mTitleResId > 0)
		{
			mTitleTextView.setText(mTitleResId);
		}
		if (mEditString != null)
		{
			mInfoTextView.setDigitsText(mEditString);
		}
		if((mInfoTextView!= null)&&(mListener!=null))
		{
			mInfoTextView.addTextChangedListener(mListener);
		}
		if(unit!= null)
		{
			unit_textview.setText("  "+unit);
		}
	}


	public void SetInfoTextView(String text)
	{
		mInfoTextView.setDigitsText(text);
	}

	@Override
	public void onFocusChange(View arg0, boolean arg1)
	{	
		if(arg1){
			mInfoTextView.setSelection(mInfoTextView.length());
			this.setBackgroundResource(R.drawable.selector_item);
			mInfoTextView.setTextColor(getResources().getColor(R.color.select_text_color));
			mTitleTextView.setTextColor(getResources().getColor(R.color.select_text_color));
			unit_textview.setTextColor(getResources().getColor(R.color.select_text_color));
		}else
		{
			if(mInfoTextView.length() == 0)
			{
				mInfoTextView.setDigitsText(mEditString);
			}
			this.setBackgroundResource(R.drawable.item_bg_bor);
			mInfoTextView.setTextColor(getResources().getColor(R.color.unselect_text_color));
			mTitleTextView.setTextColor(getResources().getColor(R.color.unselect_text_color));
			unit_textview.setTextColor(getResources().getColor(R.color.unselect_text_color));
		}	
	}
}

