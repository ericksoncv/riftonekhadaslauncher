package com.superdtv;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.KeyEvent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.util.Log;
import android.view.View.OnFocusChangeListener;


import java.util.ArrayList;
import java.util.List;

//title textview + left imageview + info textview + right imageview
public class ComboCusLayout extends LinearLayout implements View.OnClickListener{
	private final static String TAG = "ComboCusLayout";	
	private Context mContext;
	
	protected TextView mTitleTextView, mInfoTextView;
	protected ImageView mLeftImageView, mRightImageView;
	private int mSelectedId = 0;
	private int mTitleResId = 0;
	private int mHighLightIndex = 0;
	private ArrayList<String> mChooseStringList = null; 
	private ArrayList<String> mChooseStringList_Temp = null; 
	
	private AdapterView.OnItemClickListener mOnClickListener;	
	private AdapterView.OnItemClickListener mOnClickListener2 = null;
	private View.OnKeyListener mOnKeyListener = null;
	private Common_CallBack mCb = null;
	
	ChooseListDialog mChooseListDialog;
	
	public ComboCusLayout(Context context) 
	{
		this(context, null);
	}

	public ComboCusLayout(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		mContext = context;
		LayoutInflater layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.combo_cus_layout, this);
	}

	@Override
	protected void onFinishInflate() 
	{
		super.onFinishInflate();
		
 		mTitleTextView = (TextView)findViewById(R.id.title_textview);
		mTitleTextView.setOnClickListener(this);
		mInfoTextView = (TextView)findViewById(R.id.info_textview);
		mInfoTextView.setOnClickListener(this);
    	mLeftImageView = (ImageView)findViewById(R.id.left_imageview);
		mLeftImageView.setOnClickListener(this);			
		mRightImageView = (ImageView)findViewById(R.id.right_imageview);
		mRightImageView.setOnClickListener(this);
		//mLeftImageView.setOnFocusChangeListener(new OnFocusChangeListenerImpl());
	}

	/*private class OnFocusChangeListenerImpl implements OnFocusChangeListener{  
		@Override
		public void onFocusChange(View v, boolean hasFocus)
		{
			switch(v.getId())
			{
				case R.id.mLeftImageView:
					if (hasFocus) {
						mInfoTextView.setTextColor(this.getResources().getColor(R.color.select_text_color));
						mTitleTextView.setTextColor(this.getResources().getColor(R.color.select_text_color));
					} 
					else{
						mInfoTextView.setTextColor(this.getResources().getColor(android.graphics.Color.WHITE));
						mTitleTextView.setTextColor(this.getResources().getColor(android.graphics.Color.WHITE));
					}
					break;
			}
		}
	}*/


	@Override
	public void onClick(View v) 
	{
		if (v == mLeftImageView) 
		{
			if (mChooseStringList != null)
			{
				if (mHighLightIndex > 0)
				{
					mHighLightIndex--;
				}
				else
				{
					mHighLightIndex = mChooseStringList.size()-1; 
				}

				mInfoTextView.setText(getInfoTextStr(mChooseStringList, mHighLightIndex));
				if (mOnClickListener != null)
				{
					mOnClickListener.onItemClick(null, mInfoTextView, mHighLightIndex, 0);
				}
			}
		}
		else if (v == mRightImageView)
		{
			if (mChooseStringList != null)
			{
				if (mHighLightIndex < mChooseStringList.size()-1)
				{
					mHighLightIndex++;
				}
				else
				{
					mHighLightIndex = 0; 
				}

				mInfoTextView.setText(getInfoTextStr(mChooseStringList, mHighLightIndex));
				if (mOnClickListener != null)
				{
					mOnClickListener.onItemClick(null, mInfoTextView, mHighLightIndex, 0);
				}					
			}		
		}
		else if (v == mInfoTextView || v == mTitleTextView)
		{
			ShowInfoDialog();	
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{	
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_DPAD_LEFT:
				if(mLeftImageView.isEnabled()){
					if (mChooseStringList != null)
					{
						if (mHighLightIndex > 0)
						{
							mHighLightIndex--;
						}
						else
						{
							mHighLightIndex = mChooseStringList.size()-1; 
						}
						
						if(mCb != null)
							mChooseStringList = mChooseStringList_Temp;
						
						mInfoTextView.setText(getInfoTextStr(mChooseStringList, mHighLightIndex));
						if (mOnClickListener != null)
						{
							mOnClickListener.onItemClick(null, mInfoTextView, mHighLightIndex, 0);
						}
					}
				}
				return true;
				
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				if(mRightImageView.isEnabled()){
					if (mChooseStringList != null)
					{
						if (mHighLightIndex < mChooseStringList.size()-1)
						{
							mHighLightIndex++;
						}
						else
						{
							mHighLightIndex = 0; 
						}
						
						if(mCb != null)
							mChooseStringList = mChooseStringList_Temp;

						mInfoTextView.setText(getInfoTextStr(mChooseStringList, mHighLightIndex));
						if (mOnClickListener != null)
						{
							mOnClickListener.onItemClick(null, mInfoTextView, mHighLightIndex, 0);
						}					
					}
				}
				return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) 
	{	
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_DPAD_CENTER:
				if(mOnKeyListener != null)
				{
					mOnKeyListener.onKey(mInfoTextView, keyCode, event);	
				}else
				{
					if(mCb != null)
						mCb.DoCallBackEvent();
					ShowInfoDialog();
				}
				return true;
		}
		
		return super.onKeyUp(keyCode, event);
	}
	
	public TextView getTitleTextView()
	{
		return mTitleTextView;
	}

	public TextView getInfoTextView()
	{
		return mInfoTextView;
	}
	
	public ImageView getLeftImageView()
	{
		return mLeftImageView;
	}

	public ImageView getRightImageView()
	{
		return mRightImageView;
	}

	public void setHighlightIndex(int highlightIndex)
	{
		mHighLightIndex = highlightIndex;
		if (mChooseStringList != null)
		{
			mInfoTextView.setText(getInfoTextStr(mChooseStringList, highlightIndex));
		}		
	}
	
	public int getHighlightIndex()
	{
		return mHighLightIndex;
	}
	public void initView(int titleResId, int textResId, int highlightIndex, AdapterView.OnItemClickListener listener) 
	{
		//Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mHighLightIndex = highlightIndex;
		mOnClickListener = listener;

		if (textResId > 0)
		{
			mChooseStringList = new ArrayList<String>();        
			String[] secStrArray = mContext.getResources().getStringArray(textResId);  
			for (String secStr : secStrArray) 
			{  
				mChooseStringList.add(secStr);  
			} 		
		}

		if (mTitleResId > 0)
		{
			mTitleTextView.setText(mTitleResId);
		}
		
		if (mChooseStringList != null)
		{
			mInfoTextView.setText(getInfoTextStr(mChooseStringList, highlightIndex));
		}
	}
	
	public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, View.OnKeyListener keylistener ,
			AdapterView.OnItemClickListener listener2) 
	{
		//Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mChooseStringList = textList;
		mHighLightIndex = highlightIndex;
		mOnClickListener2 = listener2;
		mOnKeyListener = keylistener;
		
		if (mTitleResId > 0)
		{
			mTitleTextView.setText(mTitleResId);
		}
		mInfoTextView.setText(getInfoTextStr(mChooseStringList, highlightIndex));
	}	

	public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, AdapterView.OnItemClickListener listener) 
	{
		//Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mChooseStringList = textList;
		mHighLightIndex = highlightIndex;
		mOnClickListener = listener;
		mOnKeyListener = null;
		
		if (mTitleResId > 0)
		{
			mTitleTextView.setText(mTitleResId);
		}
		mInfoTextView.setText(getInfoTextStr(mChooseStringList, highlightIndex));
	}	
	
	public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, AdapterView.OnItemClickListener listener,Common_CallBack cb ) 
	{
		//Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mChooseStringList = textList;
		mHighLightIndex = highlightIndex;
		mOnClickListener = listener;
		mOnKeyListener = null;
		mCb = cb;
		mChooseStringList_Temp = mChooseStringList;
		if (mTitleResId > 0)
		{
			mTitleTextView.setText(mTitleResId);
		}
		mInfoTextView.setText(getInfoTextStr(mChooseStringList, highlightIndex));
	}	
	
	public void initView(int titleResId, ArrayList<String> textList, int highlightIndex, View.OnKeyListener keylistener) 
	{
		//Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mChooseStringList = textList;
		mHighLightIndex = highlightIndex;
		mOnKeyListener = keylistener;
		mOnClickListener = null;
		
		if (mTitleResId > 0)
		{
			mTitleTextView.setText(mTitleResId);
		}
		mInfoTextView.setText(getInfoTextStr(mChooseStringList, highlightIndex));
	}	
	
	private String getInfoTextStr(List<String> textList, int index)
	{
		if (textList != null && textList.size() > 0)
		{
			if (index >= textList.size())	
				index = 0;

			return textList.get(index);
		}
		else
		{
			return "";
		}
	}

	void ShowInfoDialog()
	{
		if (mChooseStringList != null)
		{
			mChooseListDialog = new ChooseListDialog(mContext, mTitleResId, mChooseStringList,
											mHighLightIndex, new AdapterView.OnItemClickListener() 
			{
				public void onItemClick(AdapterView parent, View v, int position, long id)
				{
					mHighLightIndex = position; 
					
					if (mOnClickListener != null)
					{
						mOnClickListener.onItemClick(parent, v, position, id);
					}
					
					if (mOnClickListener2 != null)
					{
						mOnClickListener2.onItemClick(parent, v, position, id);
					}
					
					if(mCb !=null)
						mChooseStringList = mChooseStringList_Temp;
					
					mInfoTextView.setText(getInfoTextStr(mChooseStringList, mHighLightIndex));					
					mChooseListDialog.dismiss();
				}
			});	
			
			mChooseListDialog.show();
		}
		else
		{
			if (mOnClickListener != null)
			{
				mOnClickListener.onItemClick(null, null, 0, 0);
			}
		}

	}
	
	public void UpdateInfotext(ArrayList<String> textList)
	{
		mChooseStringList = textList;
	}
}

