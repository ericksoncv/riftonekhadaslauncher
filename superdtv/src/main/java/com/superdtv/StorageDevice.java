package com.superdtv;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import android.util.Log;
import java.util.List;
import java.util.ArrayList;
import android.widget.*;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.StatFs;

import android.os.storage.*;

public class StorageDevice
{
    public static final String TAG = "StorageDevice";

    private Context mContext = null;
    ArrayList<DeviceItem> deviceList= null;

    public static String ROOT_PATH="/storage";
    public static String USB_PATH="/storage/external_storage";
    public static String SD_PATH="/storage/external_storage/sdcard1";

    public final static String ACTION_DEVICE_MOUNTED = "com.dtv.DEVICE_MOUNTED";
    public final static String ACTION_DEVICE_REMOVED = "com.dtv.DEVICE_REMOVED";
    public final static String DEVICE_PATH = "device_path";

    private DeviceInfoReceiver receiver;

    public StorageDevice(Context context)
    {
        this.mContext =  context;

        deviceList = new ArrayList<DeviceItem>();

        receiver = new DeviceInfoReceiver();

        IntentFilter filter = new IntentFilter();

        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addDataScheme("file");
        context.registerReceiver(receiver, filter);

        refreshDevice();
    }

    public void finish(Context context)
    {
        if(receiver != null)
        {
            context.unregisterReceiver(receiver);
            receiver = null;
        }
    }

    public int getDeviceCount()
    {
        return deviceList.size();
    }

    public DeviceItem getDeviceItem(int id)
    {
        if((id>=0) && (id< deviceList.size()))
            return deviceList.get(id);
        else return null;
    }

    public static class DeviceItem
    {
        public String Path;
        public String VolumeName;
        public String format; //fat,ntfs,etc..
        public String spare;
        public String total;
        public String used;
        public Bitmap icon;//device icon

        public Boolean bIsSdcard;
        public int deviceIndex;
    }

    public class DeviceInfoReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            Uri uri = intent.getData();

            if(uri.getScheme().equals("file"))
            {
                String path = uri.getPath();

                if(action.equals(Intent.ACTION_MEDIA_MOUNTED))
                {
                    Log.d(TAG, "mount path="+path);
                    refreshDevice();
                    Intent device_intent = new Intent(ACTION_DEVICE_MOUNTED);
                    device_intent.putExtra(DEVICE_PATH, path);
                    context.sendBroadcast(device_intent);
                }
                else if(action.equals(Intent.ACTION_MEDIA_REMOVED)||action.equals(Intent.ACTION_MEDIA_EJECT))
                {
                    Log.d(TAG, "unmount path="+path);
                    refreshDevice();
                    Intent device_intent = new Intent(ACTION_DEVICE_REMOVED);
                    device_intent.putExtra(DEVICE_PATH, path);
                    context.sendBroadcast(device_intent);
                }
            }
        }
    }

    private  String deleteExtraSpace(String str)
    {
        if(str==null)
        {
            return null;
        }
        if(str.length()==0 || str.equals(" "))
        {
            return new String();
        }
        char[] oldStr=str.toCharArray();
        int len=str.length();
        char[] tmpStr=new char[len];
        boolean keepSpace=false;
        int j=0;//the index of new string
        for(int i=0; i<len; i++)
        {
            char tmpChar=oldStr[i];
            if(oldStr[i]!=' ')
            {
                tmpStr[j++]=tmpChar;
                keepSpace=true;
            }
            else if(keepSpace)
            {
                tmpStr[j++]=tmpChar;
                keepSpace=false;
            }
        }

        int newLen=j;
        if(tmpStr[j-1]==' ')
        {
            newLen--;
        }
        char[] newStr=new char[newLen];
        for(int i=0; i<newLen; i++)
        {
            newStr[i]=tmpStr[i];
        }
        return new String(newStr);
    }

    public void refreshDevice()
    {
        PlatformSpecial.getStorageDevices(mContext, deviceList);

        int count = deviceList.size();
        
        for(int i=0 ; i<count ; i++){
            DeviceItem item = deviceList.get(i);

			if(item.VolumeName == null || item.VolumeName.equals("")){
	            if(item.bIsSdcard)
	            {
	                item.VolumeName = "SDCARD";
	            }
	            else
	            {
	                char data = (char)('A' + item.deviceIndex);
	                item.VolumeName = "USB" + "(" + data + ":)" ;
	            }
			}

            readStorageFileSystem(item);
            readStorageDeviceSpace(item);            
        
            Log.d(TAG,"device path: "+item.Path+" device format: "+item.format+" name: "+item.VolumeName);        
        }
    }

    private void readStorageFileSystem(DeviceItem item)
    {
        Runtime runtime = Runtime.getRuntime();
        String tmp;
        String format = null;
        String path = item.Path;

        path = path.replace("/storage/external_storage", "/mnt/media_rw");
        path = path.replace("/storage", "/mnt/media_rw");

        item.format = "Unknown";

        String cmd = "mount";

        try
        {
            File dir = new File(path);
            tmp = dir.getCanonicalPath();
            Log.d(TAG,">>>tmp : "+tmp);

            Process proc = runtime.exec(cmd);
            InputStream input = proc.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(input));
            String strLine;
            while(null != (strLine = br.readLine()))
            {
                Log.d(TAG,">>>"+strLine);

                for(int i=0; i<strLine.length(); i++)
                {
                    if(strLine.regionMatches(i,tmp,0,tmp.length()))
                    {
                        strLine = deleteExtraSpace(strLine);
                        String[] byteStrings = strLine.split(" ");
                        
                        format = byteStrings[4].toUpperCase();
                        
                        if(format.equals("VFAT"))
                            format ="FAT32";
                        else if(format.equals("FUSEBLK"))
                            format ="NTFS";

                        Log.d(TAG,"format : "+format);

                        item.format = format;

                        return;
                    }
                }
            }
        }

        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private void readStorageDeviceSpace(DeviceItem item)
    {
        Runtime runtime = Runtime.getRuntime();

        String cmd = "df -h "+item.Path ;

        try
        {
            Process proc = runtime.exec(cmd);
            InputStream input = proc.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(input));
            String strLine;
            while(null != (strLine = br.readLine()))
            {
                boolean matched = false;
                
                Log.d(TAG,">>>"+strLine);

                if(strLine.indexOf(item.Path)!=-1)
                    matched = true;

                if(matched)
                {
                    strLine = deleteExtraSpace(strLine);
                    String[] byteStrings = strLine.split(" ");
                    Log.d(TAG,"size="+byteStrings[1]+"  used=="+byteStrings[2]+"  free=="+byteStrings[3]);

                    item.total= byteStrings[1];
                    item.used = byteStrings[2];
                    item.spare = byteStrings[3];
                    break;
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static long getSpareSize(String str)
    {
//      System.out.println("getSpareSize >>>>>>>str:  "+str);

        String string  = str;
        String temp = null;
        long  size = -1 ;
        if(str ==null)
            return -1;
        string = string.trim();

		try{
	        if(string.contains("G"))
	        {
	            temp = string.substring(0, string.indexOf("G"));
	            size = (long)(Double.parseDouble(temp)*1024*1024*1024);
	        }
	        else if(string.contains("M"))
	        {
	            temp = string.substring(0, string.indexOf("M"));
	            size = (long)(Double.parseDouble(temp)*1024*1024);
	        }
	        else if(string.contains("K"))
	        {
	            temp = string.substring(0, string.indexOf("K"));
	            size = (long)(Double.parseDouble(temp)*1024);
	        }
	        else if(string.contains("B"))
	        {
	            temp = string.substring(0, string.indexOf("B"));
	            size = (long) Double.parseDouble(temp);
	        }

	        if(temp != null)
	        {
	            System.out.println("getSpareSize>>>>>>>size "+size);
	            return size;
	        }
	        else
	        {
	        	size = (long) Double.parseDouble(string);
	            return size;
	        }
		}
		catch(Exception e){
			return -1;
		}
    }
}
