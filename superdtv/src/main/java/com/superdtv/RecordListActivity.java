package com.superdtv;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.os.UserHandle;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;


public class RecordListActivity extends DtvBaseActivity
{
	public static final String TAG = "RecordListActivity";

	private static final int KEYCODE_AUDIO = KeyEvent.KEYCODE_2;
	private static final int KEYCODE_REPEAT = KeyEvent.KEYCODE_3;
	private static final int KEYCODE_DELETE = KeyEvent.KEYCODE_4;
	private static final int KEYCODE_RENAME = KeyEvent.KEYCODE_5;
	private static final int KEYCODE_SELECT = KeyEvent.KEYCODE_6;

	private CustomListView storage_list;
	private StorageAdapter storageAdapter;
	private int mStorageFirstVisibleItem=0;
	private int mStorageVisibleItemCount=0;


	private CustomListView record_list;
	private RecordListAdapter recordListAdapter;
	private int mRecordFirstVisibleItem =0;
	private int mRecordVisibleItemCount=0;

	private StorageDevicePlugReceiver storageDevicePlugReceiver;
	private StorageDevice storageDeviceInfo;

	private int storageDeviceCount = 0;
	private int storageDeviceIndex = 0;
	private int storage_list_item_pos =0;
	
	private int recordListCount = 0;
	private int record_list_item_pos=0;

	private LinearLayout ll_recordListUI;
	private ImageView left_img,right_img;
	
	private VideoView vView;

	//play view
	private RelativeLayout RL_RecordPlay;
	private ImageView play_status_img;
	private TextView play_bottom_videoName;
	private TextView play_bottom_current_time;
	private TextView play_bottom_duration;
	private TextView play_speed;

	private SeekBar play_bottom_seekbar;

	private boolean bHandlePreNext = false;	
	
	mw_data.pvr_record pvrRecord = new mw_data.pvr_record();

	private Handler mwMsgHandler;
	private Handler localMsgHandler;

	public static final int LOCAL_MSG_CLOSE_INFO_BAR = 0;	
	public static final int INFO_BAR_CLOSE_TIMEOUT = 5000;
	
	private int playback_status = MW.PVR_RECORD_STATUS_ERROR; 
	private int speed = 0;
	private int fbackforward = 0; //0:other 1:forward 2:backward
	private boolean flag = false;
	private boolean initflag = false;
	
	private static final int VIDEO_REPEAT_NONE = 0;
	private static final int VIDEO_REPEAT_SINGLE = 1;
	private static final int VIDEO_REPEAT_ALL = 2;

	private static int video_repeat_status=VIDEO_REPEAT_NONE;

	private Dialog dia_currentDialog;
	private mw_data.pvr_record pvrRecordInfo = new mw_data.pvr_record();
	
	boolean[][] PvrSelStatus = null;
	
	private void initPvrSelStatus() {
		if(storageDeviceInfo != null){
			storageDeviceCount = storageDeviceInfo.getDeviceCount();
		}else
		{
			storageDeviceCount = 0;
		}
		
		int device_count = storageDeviceCount;
	
		if(device_count >0)
		{
			PvrSelStatus = new boolean[device_count][];
			
			System.out.println("PvrSelStatus "+ PvrSelStatus);
			for(int i =0;i<device_count;i++)
			{
				StorageDevice.DeviceItem item = storageDeviceInfo.getDeviceItem(i);
				if(item!= null)
				{
					if(MW.pvr_record_init_record_list(item.Path) == MW.RET_OK)
					{			
//						System.out.println("item.Path +....."+ item.Path);
						int filecount = MW.pvr_record_get_record_count();
//						System.out.println("filecount : "+filecount);
						if(filecount > 0)
						{
							PvrSelStatus[i] = new boolean[filecount];
							for(int j =0;j<filecount;j++)
							{
								PvrSelStatus[i][j] = false;
							}
						}
						else
						{
							PvrSelStatus[i] = null;
						}
					}
				
				}
			}
		}
	}
	
	ArrayList<Integer> SelPvrList = new ArrayList();
	private void getSelPvrFileInfo() {
		SelPvrList.clear();
		{
			if(PvrSelStatus!= null)
			{ 
				for(int j=0;j<recordListCount;j++)
				 {
					if(PvrSelStatus[storageDeviceIndex][j])
					{
						System.out.println("PvrSelStatus file index  : "+j);
						SelPvrList.add(j);
					}
				 }
			}
		}
		
		
		if(SelPvrList.size()==0)
		{
			SelPvrList.add(record_list_item_pos);
		}
	}
	
	private void RefreshPvrSelectImage() {
		if(PvrSelStatus!= null)
		{
			if(PvrSelStatus[storageDeviceIndex]!=null)
			{
				PvrSelStatus[storageDeviceIndex][record_list_item_pos] = !PvrSelStatus[storageDeviceIndex][record_list_item_pos];
			}
		}else
		{
			System.out.println("PvrSelStatus is null");
		}
			
		record_list.setSelectionFromTop(record_list_item_pos, mRecordFirstVisibleItem);
		recordListAdapter.notifyDataSetChanged();

	}
	private void initPlayView() {
		  RL_RecordPlay = (RelativeLayout) findViewById(R.id.RL_RecordPlay);
		  play_status_img = (ImageView) findViewById(R.id.play_status_img);
		  play_bottom_videoName= (TextView) findViewById(R.id.play_bottom_videoName);
		  play_bottom_current_time = (TextView) findViewById(R.id.play_bottom_current_time);
		  play_bottom_duration = (TextView) findViewById(R.id.play_bottom_duration);
		  play_bottom_seekbar= (SeekBar) findViewById(R.id.play_bottom_seekbar);
		  play_speed = (TextView) findViewById(R.id.play_speed);
		  
		  play_bottom_current_time.setText("00:00:00");
		  play_bottom_duration.setText("00:00:00");
		  play_bottom_seekbar.setProgress(0);
  		  play_status_img.setVisibility(View.INVISIBLE);
  		  play_speed.setVisibility(View.INVISIBLE);
  		  fbackforward = 0;

	}
	private void ShowPlayInfoView()
	{
		RL_RecordPlay.setVisibility(View.VISIBLE);

		if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord)==MW.RET_OK)
		{
			if(pvrRecord.service_name!=null)
			{
				if(pvrRecord.service_name.length()>0)
				{
					play_bottom_videoName.setText(pvrRecord.service_name);
				}else
				{
					play_bottom_videoName.setText("Unknow");
				}
				
			}

			{
				play_status_img.setVisibility(View.INVISIBLE);
		  		play_speed.setVisibility(View.INVISIBLE);

				if(bHandlePreNext)
				{
					bHandlePreNext = false;
					play_bottom_current_time.setText("00:00:00");
					play_bottom_seekbar.setProgress(0);
				}

			}
		}
		Log.e("PVR","playback_status =" + playback_status);
//		if(playback_status == MW.PVR_PLAYBACK_STATUS_PLAY)
		if(playback_status != MW.PVR_PLAYBACK_STATUS_PAUSE)
		{
			HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
		}
		
	}
	private void HidePlayInfoView(int delayMS)
	{
		localMsgHandler.removeMessages(LOCAL_MSG_CLOSE_INFO_BAR);

		if(delayMS > 0)
			localMsgHandler.sendMessageDelayed(localMsgHandler.obtainMessage(LOCAL_MSG_CLOSE_INFO_BAR), delayMS);
		else
		{
			RL_RecordPlay.setVisibility(View.INVISIBLE);
		}
	}
	
	class LOCALMessageHandler extends Handler {
        public LOCALMessageHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
        	int msg_type = msg.what;

			switch(msg_type){
				case LOCAL_MSG_CLOSE_INFO_BAR:
				{
					HidePlayInfoView(0);
					break;
				}
				default:
					break;
			}
        }

	}
	class MWmessageHandler extends Handler {
        public MWmessageHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
        	int event_type = (msg.what >> 16) & 0xFFFF;
			int sub_event_type = msg.what & 0xFFFF;

			switch(event_type){
				case MW.EVENT_TYPE_PVR_STATUS:
				{
					switch(sub_event_type){
						case  MW.PVR_PLAYBACK_STATUS_INIT:
						{
							int total_time =msg.arg1;
							int total_hour =0, total_minute=0, total_second=0;
							
							totaltime = total_time;
							total_second = total_time % 60;
							total_minute = (total_time / 60 ) % 60;
							total_hour = total_time / 3600;					
							play_bottom_seekbar.setMax(total_time);
							play_bottom_duration.setText(Common.stringFormat("%02d:%02d:%02d", total_hour, total_minute, total_second));
							play_bottom_current_time.setText("00:00:00");
							Log.e("PVR","timeshift total time : " + total_time);
							Log.e("PVR","INIT");
							initflag = true;
							break;
						}
						case MW.PVR_PLAYBACK_STATUS_PLAY:
						{
							int current_time = msg.arg1;
							int current_hour =0, current_minute=0, current_second=0;
							
							seektime = current_time;
							
							m_second = current_second = current_time % 60;
							m_minute =current_minute = (current_time / 60 ) % 60;
							m_hour =current_hour = current_time / 3600;
							
							play_bottom_current_time.setText(Common.stringFormat("%02d:%02d:%02d", current_hour, current_minute, current_second));
							play_bottom_seekbar.setProgress(current_time);
							
							
		            		play_status_img.setVisibility(View.INVISIBLE);
		            		play_speed.setVisibility(View.INVISIBLE);
		            		if(fbackforward == 0)
		            		{
		            			play_status_img.setImageResource(R.drawable.record);
			            		play_status_img.setVisibility(View.VISIBLE);
		            		}
							if((playback_status == MW.PVR_PLAYBACK_STATUS_FFFB))
							{
								HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
							}
							playback_status = MW.PVR_PLAYBACK_STATUS_PLAY;
							fbackforward = 0;
							speed = 0;
							flag = false;		

							Log.e("PVR", " timeshift update time : " + current_time+ "play_status :"+playback_status);
							Log.e("PVR","PLAY");
							Log.e(TAG,"PVR_PLAYBACK_STATUS_PLAY");

							break;
						}
						case MW.PVR_PLAYBACK_STATUS_PAUSE:
						{
							playback_status = MW.PVR_PLAYBACK_STATUS_PAUSE;
							speed = 0;
							fbackforward =0;
							
							HidePlayInfoView(0);
							Log.e(TAG,"PVR_PLAYBACK_STATUS_PAUSE");
							ShowPlayInfoView();
							
		            		play_speed.setVisibility(View.INVISIBLE);
		            		play_status_img.setImageResource(R.drawable.osd_pause_hl);
		            		play_status_img.setVisibility(View.VISIBLE);
							flag = false;

							Log.e("PVR","pause");
							break;
						}
						case MW.PVR_PLAYBACK_STATUS_FFFB:
						{
							int time = msg.arg1;
							int hours =0,minutes = 0,seconds = 0;
							seconds = time % 60;
							minutes = (time / 60 ) % 60;
							hours = time / 3600;
							playback_status = MW.PVR_PLAYBACK_STATUS_FFFB;
							play_bottom_current_time.setText(Common.stringFormat("%02d:%02d:%02d", hours, minutes, seconds));
							play_bottom_seekbar.setProgress(time);
							
							if(fbackforward == 2)
							{
			            		play_status_img.setImageResource(R.drawable.osd_rewind_hl);
							}
							else if (fbackforward == 1)
							{
			            		play_status_img.setImageResource(R.drawable.osd_forward_hl);
							}else
							{
								play_speed.setVisibility(View.INVISIBLE);
								play_status_img.setVisibility(View.INVISIBLE);
							}
							
							if(flag == false){
								HidePlayInfoView(0);
								ShowPlayInfoView();
								Log.e(TAG,"PVR_PLAYBACK_STATUS_FFFB");
			            		play_speed.setVisibility(View.VISIBLE);
								play_status_img.setVisibility(View.VISIBLE);
							}else{
			            		play_speed.setVisibility(View.INVISIBLE);
								play_status_img.setVisibility(View.VISIBLE);
							}

							Log.e("PVR","SPEED : "+speed);
							
							Log.e("PVR","FFFB");
							break;
						}
						case MW.PVR_PLAYBACK_STATUS_SEARCH_OK:
						{
							Log.e("PVR","SEEK");
							playback_status = MW.PVR_PLAYBACK_STATUS_SEARCH_OK;

							break;
						}
						case MW.PVR_PLAYBACK_STATUS_EXIT:
						{
							if(video_repeat_status == VIDEO_REPEAT_NONE){
								play_status_img.setVisibility(View.INVISIBLE);
								if(ll_recordListUI.getVisibility() == View.GONE){
									MW.pvr_playback_stop();
									ll_recordListUI.setVisibility(View.VISIBLE);
									Showmanline();
									record_list.requestFocus();
									record_list.setCustomSelection(record_list_item_pos);
								}
								if(dialog!= null)
						    	{
						    		if(dialog.isShowing())
						    		{
						        		dialog.dismiss();
									}
						    	}
								
								if(dia_currentDialog!=null)
								{
									dia_currentDialog.dismiss();
								}

								if(initflag == true)
									MW.pvr_playback_stop();
								
								speed = 0;
								playback_status = MW.PVR_PLAYBACK_STATUS_EXIT;
							}
							else {
				            	Log.e(TAG,"recordListCount := "+ recordListCount);
				            	if(ll_recordListUI.getVisibility() == View.VISIBLE)
				            		return ;
				            		
				            	if(recordListCount == 1 )
				            	{
				            		Log.e(TAG,"no previous item");
				            		return ;
				            	}
				            	bHandlePreNext = true;
								
								if(video_repeat_status == VIDEO_REPEAT_ALL){
									if(record_list_item_pos <(recordListCount-1))
									{
										record_list_item_pos++;
									}else
									{
										record_list_item_pos = 0;
									}
								}
				            	Log.e(TAG,"record_list_item_pos  := "+ record_list_item_pos);
				            	
				        		if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
				        		{            	
			        				if(MW.pvr_playback_start(record_list_item_pos)== MW.RET_OK)
			                    	{
			                    		ShowPlayInfoView();
			                    	}else
			                    	{
			                    		Log.e(TAG,"KEYCODE_MEDIA_PREVIOUS pvr_playback_start failed : record_list_item_pos = " +record_list_item_pos);
			                    	}
				        		}
				            	
				            }
							Log.e("PVR","exit");
						}
						default:
							break;
					}
				}
				break;
				
				default:
					break;
			}
        }
    }
	private void initMessageHandle()
	{
		mwMsgHandler = new MWmessageHandler(looper);
		localMsgHandler = new LOCALMessageHandler(looper);
	}
	
	
	private EditText hour;
	private EditText minute;
	private EditText second;
	private TextView tips_txt;
	
	private int seektime = 0;
	private int totaltime = 0;
	private int m_hour =0, m_minute =0, m_second =0;
	private int temp_hour = 0,temp_minute =0, temp_second =0;

	Dialog dialog ;
	Dialog dlg ;

	public class TimeSeek1Dialog extends Dialog 
	{
		private final static String TAG = "TimeSeekDialog"; 
		private Context mContext = null;
		
		private View.OnKeyListener mOnKeyListener = null;
		private EditText mHourEditText= null;
		private EditText mMinEditText= null;
		private EditText mSecEditText= null;
		private boolean mbHandleUpDown = false;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) 
		{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.timeseek_dialog);
			
			this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			
			mHourEditText = (EditText)findViewById(R.id.hour_edit_text);
			mHourEditText.setBackgroundResource(R.drawable.edit_enable_css);
			mHourEditText.setGravity(Gravity.LEFT);

			mMinEditText = (EditText)findViewById(R.id.min_edit_text);
			mMinEditText.setBackgroundResource(R.drawable.edit_enable_css);
			mMinEditText.setGravity(Gravity.LEFT);

			mSecEditText = (EditText)findViewById(R.id.sec_edit_text);
			mSecEditText.setBackgroundResource(R.drawable.edit_enable_css);
			mSecEditText.setGravity(Gravity.LEFT);
			

			temp_second =  seektime % 60;
			temp_minute = (seektime / 60 ) % 60;
			temp_hour = seektime / 3600;
			
			mHourEditText.setText(Common.stringFormat("%02d",m_hour));
			mMinEditText.setText(Common.stringFormat("%02d",m_minute));
			mSecEditText.setText(Common.stringFormat("%02d",m_second));

			if((totaltime/3600)==0){
				mHourEditText.setFocusable(false);
				mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);
			}else
			{
				mHourEditText.setFocusable(true);
			}
			if(((totaltime/3600)==0)&&(((totaltime / 60 ) % 60) == 0)){
				if((totaltime%60)!=0)
				{
					mMinEditText.setFocusable(false);
					mMinEditText.setBackgroundResource(R.drawable.edit_disable_css);
				}
			}else
			{
				mMinEditText.setFocusable(true);
			}
			mSecEditText.requestFocus();
			mSecEditText.setTextColor(getResources().getColor(R.color.select_text_color)); 
			mSecEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);

			mHourEditText.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					try {
						m_hour = Integer.parseInt(mHourEditText.getText().toString());
					} catch (NumberFormatException e) {
						e.printStackTrace();
						m_hour = temp_hour;
					}
				}
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{
					
				}
				@Override
				public void afterTextChanged(Editable s)
				{

				}
			});

			mMinEditText.addTextChangedListener(new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try {
						m_minute = Integer.parseInt(mMinEditText.getText().toString());
					} catch (NumberFormatException e) {
						e.printStackTrace();
						m_minute = temp_minute;
					}

				}
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}
				public void afterTextChanged(Editable s) {

				}
			});
			
			mSecEditText.addTextChangedListener(new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before, int count) {
						try {
							m_second = Integer.parseInt(mSecEditText.getText().toString());
						} catch (NumberFormatException e) {
							e.printStackTrace();
							m_second = temp_second;
						}
				}
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}
				public void afterTextChanged(Editable s) {

				}
			});
			
			this.setOnKeyListener( new DialogInterface.OnKeyListener()
			{
				public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) 
				{
					Log.d(TAG, "=====onKey() "+keyCode+" event.getAction() "+event.getAction());
					if (event.getAction() == KeyEvent.ACTION_DOWN){
						switch(keyCode)
						{
							case KeyEvent.KEYCODE_0:
							case KeyEvent.KEYCODE_1:
							case KeyEvent.KEYCODE_2:
							case KeyEvent.KEYCODE_3:
							case KeyEvent.KEYCODE_4:
							case KeyEvent.KEYCODE_5:
							case KeyEvent.KEYCODE_6:
							case KeyEvent.KEYCODE_7:
							case KeyEvent.KEYCODE_8:
							case KeyEvent.KEYCODE_9:
								int value = 0 ;
								
								if (mSecEditText.isFocused())
								{
									if(mSecEditText.getText().toString().length() > 0)
									{
										value = Integer.valueOf(mSecEditText.getText().toString()).intValue();
									}
									value = value *10 +keyCode - KeyEvent.KEYCODE_0;
									
									if(value >= 100)
									{
										value =value%10;
									}else if (value >=60)
									{
										value =0;
									}
									mSecEditText.setText(Common.stringFormat("%02d",value));
								}	
								else if (mMinEditText.isFocused())
								{
									if(mMinEditText.getText().toString().length() > 0)
									{
										value = Integer.valueOf(mMinEditText.getText().toString()).intValue();
									}
									value = value *10 +keyCode - KeyEvent.KEYCODE_0;
									
									if(value >= 100)
									{
										value =value%10;
									}else if (value >=60)
									{
										value =0;
									}
									mMinEditText.setText(Common.stringFormat("%02d",value));
								}else if (mHourEditText.isFocused())
								{
									if(mHourEditText.getText().toString().length() > 0)
									{
										value = Integer.valueOf(mMinEditText.getText().toString()).intValue();
									}
									value = value *10 +keyCode - KeyEvent.KEYCODE_0;
									
									if(value >= 100)
									{
										value =value%10;
									}else if (value >=60)
									{
										value =0;
									}
									mHourEditText.setText(Common.stringFormat("%02d",value));
								}
								break;
								case KeyEvent.KEYCODE_DPAD_LEFT:
								{
									Log.e(TAG,"focus : "+ mHourEditText.isFocused()+mMinEditText.isFocused()+mSecEditText.isFocused());
									if(mHourEditText.isFocused())
									{
										mSecEditText.requestFocus();
										mSecEditText.setTextColor(getResources().getColor(R.color.select_text_color));
										mMinEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
										mHourEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
										mSecEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
										mMinEditText.setBackgroundResource(R.drawable.edit_enable_css);
										mHourEditText.setBackgroundResource(R.drawable.edit_enable_css);
										return true;

									}else if (mSecEditText.isFocused())
									{
										mSecEditText.clearFocus();
										if(mMinEditText.isFocusable()){
											mMinEditText.requestFocus();
											mMinEditText.setTextColor(getResources().getColor(R.color.select_text_color));
											mSecEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mHourEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mSecEditText.setBackgroundResource(R.drawable.edit_enable_css);
											mMinEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
											
											if(mHourEditText.isFocusable())
												mHourEditText.setBackgroundResource(R.drawable.edit_enable_css);
											else
												mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);
										}else{
											mMinEditText.setBackgroundResource(R.drawable.edit_disable_css);
											mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);
										}

										return true;
									}else if (mMinEditText.isFocused())
									{
										mMinEditText.clearFocus();
										if(mHourEditText.isFocusable())
										{
											mHourEditText.requestFocus();
											mSecEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mMinEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mHourEditText.setTextColor(getResources().getColor(R.color.select_text_color));
											mHourEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
											mSecEditText.setBackgroundResource(R.drawable.edit_enable_css);
											mMinEditText.setBackgroundResource(R.drawable.edit_enable_css);
										}
										else
										{
											mSecEditText.requestFocus();
											mSecEditText.setTextColor(getResources().getColor(R.color.select_text_color));
											mHourEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mMinEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mSecEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
											mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);
											mMinEditText.setBackgroundResource(R.drawable.edit_enable_css);
										}
										return true;
									}
									break;
								}
							
								case KeyEvent.KEYCODE_DPAD_RIGHT:
								{
									if(mSecEditText.isFocused())
									{
										mSecEditText.clearFocus();
										if(mHourEditText.isFocusable())
										{
											mHourEditText.requestFocus();
											mHourEditText.setTextColor(getResources().getColor(R.color.select_text_color));
											mSecEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mMinEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
											mHourEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
											mSecEditText.setBackgroundResource(R.drawable.edit_enable_css);
											mMinEditText.setBackgroundResource(R.drawable.edit_enable_css);
										}
										else
										{
											if(mMinEditText.isFocusable()){
												mMinEditText.requestFocus();
												mMinEditText.setTextColor(getResources().getColor(R.color.select_text_color));
												mSecEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
												mHourEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
												mMinEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
												mSecEditText.setBackgroundResource(R.drawable.edit_enable_css);
												mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);
											}
											else{
												mMinEditText.setBackgroundResource(R.drawable.edit_disable_css);
												mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);
											}
										}
										return true;

									}else if (mHourEditText.isFocused())
									{
										mHourEditText.clearFocus();
										mMinEditText.requestFocus();
										mMinEditText.setTextColor(getResources().getColor(R.color.select_text_color));
										mSecEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
										mHourEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
										mMinEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
										mSecEditText.setBackgroundResource(R.drawable.edit_enable_css);
										mHourEditText.setBackgroundResource(R.drawable.edit_enable_css);
										return true;

									}else if (mMinEditText.isFocused())
									{
										mMinEditText.clearFocus();
										mSecEditText.requestFocus();
										mSecEditText.setTextColor(getResources().getColor(R.color.select_text_color));
										mMinEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
										mHourEditText.setTextColor(getResources().getColor(R.color.edit_bg_color));
										mSecEditText.setBackgroundResource(R.drawable.edit_enable_focus_css);
										mMinEditText.setBackgroundResource(R.drawable.edit_enable_css);
										if(mHourEditText.isFocusable())
											mHourEditText.setBackgroundResource(R.drawable.edit_enable_css);
										else
											mHourEditText.setBackgroundResource(R.drawable.edit_disable_css);

										return true;
									}
									break;
								}
							
								case KeyEvent.KEYCODE_BACK:
								{
									if(playback_status == MW.PVR_PLAYBACK_STATUS_PAUSE)
					            	{
					            		play_status_img.setVisibility(View.INVISIBLE);
					            		MW.pvr_playback_resume();
					        			HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
					            	}
									break;
								}
							
								case KeyEvent.KEYCODE_DPAD_CENTER:
								{
									int m_seektime = 0;

									m_seektime = m_hour*60*60+m_minute*60+m_second;
									
									Log.e(TAG,"m_seektime : "+ m_seektime);
									Log.e(TAG,"m_hour : " +m_hour );
									Log.e(TAG,"m_minute : " +m_minute );
									Log.e(TAG," m_second : "+m_second );
									
									if((m_seektime<0)||(m_seektime>totaltime))
									{
										//tips_txt.setText(getResources().getString(R.string.seekTimeTitletips_invalid));
										//tips_txt.setTextColor(Color.rgb(255, 0, 0));
										Common.makeText(RecordListActivity.this, (R.string.seekTimeTitletips_invalid), Toast.LENGTH_SHORT);
										return true;
									}
									
									if(m_seektime == seektime)
									{
										MW.pvr_playback_resume();
										playback_status = MW.PVR_PLAYBACK_STATUS_PLAY;
					        			HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
										dialog.dismiss();
										return true;
									}
									
									MW.pvr_playback_seek(m_seektime, false);
									
									dialog.dismiss();
				        			HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
									return true;
								}
							}
							
						}
						return false;
					}
			}); 
			
		}
	
		public TimeSeek1Dialog(Context context, int theme, View.OnKeyListener listener) 
		{
			super(context, theme);
			mContext = context;
			mOnKeyListener = listener;
		}
		
		public TimeSeek1Dialog(Context context, View.OnKeyListener listener) 
		{
			super(context);
			mContext = context;
			mOnKeyListener = listener;
		}
	
		public void setHandleUpDown(boolean b) 
		{
			mbHandleUpDown = b;
		}	
	
	}

  	private void ShowPvrSeekTimeSetDialog()
  	{
		dlg = new TimeSeek1Dialog(this, R.style.MyDialog, new View.OnKeyListener()
		{
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if(event.getAction() == KeyEvent.ACTION_UP)
				{
					if (keyCode == KeyEvent.KEYCODE_BACK||keyCode == KeyEvent.KEYCODE_MENU)
					{
						dlg.dismiss();
						return true;
					}
				}

				return false;
			} 
		});
		dlg.show();
		Common.setWindow_Attributes(dlg); 

		((TimeSeek1Dialog) dlg).setHandleUpDown(true);		
	}
  	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean playPauseKeyShowSeekTimeDlg = true;
		
        switch (keyCode) {
			case KEYCODE_AUDIO://Switch Audio
			{
				if(ll_recordListUI.getVisibility() == View.VISIBLE )
	        	{
	        		return true;
	        	}
				
				if(MW.pvr_record_get_record_info(record_list_item_pos,pvrRecordInfo) == MW.RET_OK)
				{
					AudioInfoDialog audioInfoDia = new AudioInfoDialog(this,R.style.MyDialog,record_list_item_pos,recordListCount) ;
					audioInfoDia.show();
					Common.setWindow_Attributes(audioInfoDia);
				}
				
				return true;
			}
			
			case KEYCODE_REPEAT://Switch repeat mode
			{
				if(ll_recordListUI.getVisibility() == View.VISIBLE )
	        	{
	        		return true;
	        	}
				
				if(MW.pvr_record_get_record_info(record_list_item_pos,pvrRecordInfo) == MW.RET_OK)
				{
					PvrRepeatDialog pvrRepeatDia = new PvrRepeatDialog(this,R.style.MyDialog) ;
					pvrRepeatDia.show();
					Common.setWindow_Attributes(pvrRepeatDia);
				}
				
				return true;
			}

            case KeyEvent.KEYCODE_BACK:
            {
        		if((RL_RecordPlay.getVisibility() != View.INVISIBLE )&&(playback_status == MW.PVR_PLAYBACK_STATUS_PLAY))
        		{
        			RL_RecordPlay.setVisibility(View.INVISIBLE);
        			return true;
        		}
            	
            	if(dialog!= null)
            	{
            		if(dialog.isShowing())
            		{
                		dialog.dismiss();
    				}
            	}
        		
        		if(dia_currentDialog!=null)
        		{
        			dia_currentDialog.dismiss();
        		}
        		
        		if(initflag == true)
        			MW.pvr_playback_stop();
        		
            	if(ll_recordListUI.getVisibility() == View.GONE){
					MW.pvr_playback_stop();
					ll_recordListUI.setVisibility(View.VISIBLE);
					Showmanline();
					record_list.requestFocus();
					record_list.setCustomSelection(record_list_item_pos);
					play_status_img.setVisibility(View.INVISIBLE);
				
					playback_status = MW.PVR_PLAYBACK_STATUS_EXIT;
					return true;
				}
				break;
            }
			
            case KeyEvent.KEYCODE_INFO:
            {
            	if(playback_status == MW.PVR_PLAYBACK_STATUS_PLAY)
            	{
                	if(RL_RecordPlay.getVisibility() != View.VISIBLE){
                		ShowPlayInfoView();
    				}
    				else{
    					HidePlayInfoView(0);
    				}
            	}

            	return true;
            }
            
            case KeyEvent.KEYCODE_DPAD_CENTER://play or pause
            {
            	Log.e("PVR","playback_status : " +playback_status);
            	if(playback_status == MW.PVR_PLAYBACK_STATUS_PLAY)
            	{
            		MW.pvr_playback_pause();

					if(playPauseKeyShowSeekTimeDlg)
	            		ShowPvrSeekTimeSetDialog();
            		
            	}
            	else if(playback_status == MW.PVR_PLAYBACK_STATUS_PAUSE)
            	{
            		play_status_img.setVisibility(View.INVISIBLE);
            		MW.pvr_playback_resume();
        			HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
            	}
            	else if(playback_status == MW.PVR_PLAYBACK_STATUS_FFFB)
            	{
					flag = true;
					Log.e(TAG,"fbackforward OK : " + fbackforward);
            		if(fbackforward == 1)
            		{
                		MW.pvr_playback_fast_forward(0);
            		}
            		else 
            		{
                		MW.pvr_playback_fast_backward(0);
            		}
            		
            		play_status_img.setImageResource(R.drawable.record);
            		play_speed.setVisibility(View.INVISIBLE);
        			HidePlayInfoView(INFO_BAR_CLOSE_TIMEOUT);
        			Log.e(TAG, "hide info bar ");
            	}
            	return true;
            }

            case KeyEvent.KEYCODE_DPAD_DOWN://play prev record
            {
            	Log.e(TAG,"recordListCount := "+ recordListCount);
            	if(ll_recordListUI.getVisibility() == View.VISIBLE)
            		return true;
            		
            	if(recordListCount == 1 )
            	{
            		Log.e(TAG,"no previous item");
            		return true;
            	}
            	bHandlePreNext = true;
            	if(record_list_item_pos >0)
            	{
            		record_list_item_pos--;
            	}else
            	{
            		record_list_item_pos = recordListCount-1;
            	}
            	Log.e(TAG,"record_list_item_pos  := "+ record_list_item_pos);
            	
        		if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
        		{            	
    				if(MW.pvr_playback_start(record_list_item_pos)== MW.RET_OK)
                	{
                		ShowPlayInfoView();
                	}else
                	{
                		Log.e(TAG,"pvr_playback_start failed : record_list_item_pos = " +record_list_item_pos);
                	}
        		}
            	
            	return true;
            }
            case KeyEvent.KEYCODE_DPAD_UP://play next record
            {
            	Log.e(TAG,"recordListCount := "+ recordListCount);
            	if(ll_recordListUI.getVisibility() == View.VISIBLE)
            		return true;
            	if(recordListCount == 1)
            	{
            		Log.e(TAG,"no next item");
            		return true;
            	}
            	bHandlePreNext = true;

            	if(record_list_item_pos <(recordListCount-1))
            	{
            		record_list_item_pos++;
            	}else
            	{
            		record_list_item_pos = 0;
            	}
            	Log.e(TAG,"record_list_item_pos NEXT  := "+ record_list_item_pos);
            	
        		if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
        		{            	
                	if(MW.pvr_playback_start(record_list_item_pos)==MW.RET_OK)
                	{
                		ShowPlayInfoView();
                	}else
                	{
                		Log.e(TAG," pvr_playback_start failed : record_list_item_pos = " +record_list_item_pos);
                	}
        		}
            	
            	return true;
            }
            
            case KeyEvent.KEYCODE_DPAD_RIGHT://forward
            {
            	Log.e(TAG," fbackforward  forward : " + fbackforward);
            	
            	if (fbackforward == 2)
            	{
            		speed =0;
            		MW.pvr_playback_fast_forward(speed);
            		return true;
            	}
            	
        		fbackforward = 1;

            	if(speed == 0)
            		speed = 2;
            	else
            		speed = speed*2;
            	
            	if(speed > 32)
            	{
            		speed = 0;
            	}else
            	{
            		play_speed.setText("X"+speed);
            		play_status_img.setImageResource(R.drawable.osd_forward_hl);
            	}
        		MW.pvr_playback_fast_forward(speed);

            	return true;
            }
            
            case KeyEvent.KEYCODE_DPAD_LEFT://backward
            {
            	Log.e(TAG," fbackforward  backward :  " + fbackforward);
            	
            	if (fbackforward == 1)
            	{
            		speed =0;
            		MW.pvr_playback_fast_forward(speed);
            		return true;
            	}
            	
        		fbackforward = 2;

            	if(speed == 0)
            		speed = 2;
            	else
            		speed = speed*2;
            	
            	if(speed >32)
            	{
            		speed =0;
            	}else
            	{
            		play_speed.setText("X"+speed);
            		play_status_img.setImageResource(R.drawable.osd_rewind_hl);
            	}
        		MW.pvr_playback_fast_backward(speed);
            	return true;
            }
            
			default:
				break;
    	}

		return super.onKeyDown(keyCode, event);
	}


	private class StorageDevicePlugReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();

        	if (action.equals(StorageDevice.ACTION_DEVICE_MOUNTED))
			{
        		Log.e(TAG, "StorageDevice plug in :  "+intent.getStringExtra(StorageDevice.DEVICE_PATH));
        		
				refreshStorageDeviceList();
			}
        	else if (action.equals(StorageDevice.ACTION_DEVICE_REMOVED))
			{
        		Log.e(TAG, "StorageDevice plug out :  "+intent.getStringExtra(StorageDevice.DEVICE_PATH));
				
				refreshStorageDeviceList();
			}
		}
    }

	private void refreshStorageDeviceList()
	{
		initPvrSelStatus();

		if(storageDeviceInfo != null){
			storageDeviceCount = storageDeviceInfo.getDeviceCount();

			if(storageDeviceCount>0){
				Log.e(TAG,"have device count : "+storageDeviceCount);
				for(int i = 0;i<storageDeviceCount;i++){
					StorageDevice.DeviceItem item = storageDeviceInfo.getDeviceItem(i);

					if(item != null)
						Log.e(TAG,"device path: "+item.Path+" device format: "+item.format+"  name: "+item.VolumeName+" spare : "+item.spare+" total: "+item.total);
					else
						Log.e(TAG,"get device failed : " + i);
				}
				
				Log.e(TAG,"storageDeviceIndex ："+ storageDeviceIndex + "storageDeviceCount : "+ storageDeviceCount);

			}
			else{
				Log.e(TAG,"not device");
			}
		}
		else{
			storageDeviceCount = 0;
		}

		storageAdapter.notifyDataSetChanged();
		
		{
			storageDeviceIndex=storage_list_item_pos;
			storage_list.requestFocus();
			storage_list.setCustomSelection(storageDeviceIndex);
			if(storage_list.getSelectedView()!= null)
			{
				storage_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
			}
		}	

		refreshRecordList();
	}

	private void refreshRecordList()
	{
		StorageDevice.DeviceItem item = storageDeviceInfo.getDeviceItem(storageDeviceIndex);

		if(record_list.getChildAt(record_list_item_pos)!= null)
		{
			record_list.getChildAt(record_list_item_pos).setBackgroundResource(R.drawable.listview_item_bg_selector);
		}
		record_list_item_pos = 0;
		
		if(item != null){
			if(MW.pvr_record_init_record_list(item.Path) == MW.RET_OK){			
				recordListCount = MW.pvr_record_get_record_count();

				if(recordListCount>0){
					Log.e(TAG,"have record count : "+recordListCount);
					for(int i = 0;i<recordListCount;i++)
					{
						if(MW.pvr_record_get_record_info(i, pvrRecord) == MW.RET_OK)
						{
							Log.e(TAG,"record name : " + pvrRecord.service_name + " **  file size : " + pvrRecord.file_size/1048576 + " MB");
						}	
						else
						{
							Log.e(TAG,"get record failed : " + i);
						}
					}
					
				}
				else{
					Log.e(TAG,"not record");
				}
			}
			else{
				Log.e(TAG,"init record list failed");
			}
		}
		else{
			recordListCount = 0;
		}

		recordListAdapter.notifyDataSetChanged();

        audio_pid_pos = null;
        audio_channel_pos = null;       
		if(recordListCount>0){
		    audio_pid_pos = new int[recordListCount];
		    audio_channel_pos = new int[recordListCount];
		    
			for(int i = 0;i<recordListCount;i++)
			{
				if(MW.pvr_record_get_record_info(i, pvrRecord) == MW.RET_OK)
				{
					audio_pid_pos[i] = pvrRecord.audio_index;
					audio_channel_pos[i] = pvrRecord.audio_channel;
				}	
			}			
		}        
	}

	private void initVideoView() {
		vView = (VideoView)findViewById(R.id.VideoView);

		if(mSHCallback != null)
			vView.getHolder().addCallback(mSHCallback);
		
		Common.videoViewSetFormat(vView);	
	}
	
	private void initView() {
		ll_recordListUI = (LinearLayout) findViewById(R.id.LinearLayout_RecordListUI);
		left_img = (ImageView) findViewById(R.id.left_img);
		right_img = (ImageView) findViewById(R.id.right_img);

		storage_list = (CustomListView) findViewById(R.id.storage_list);
		record_list = (CustomListView) findViewById(R.id.pvr_program_list);
	}

	private void Hidemanline()
	{
		left_img.setVisibility(View.GONE);
		right_img.setVisibility(View.GONE);
	}
	
	private void Showmanline()
	{
		left_img.setVisibility(View.VISIBLE);
		right_img.setVisibility(View.VISIBLE);
	}
	private void initData() {
		storageAdapter = new StorageAdapter(this) ;
		storage_list.setAdapter(storageAdapter);
		storage_list.setVisibleItemCount(9);
		storage_list.setCustomScrollListener(new OnScrollListener(){
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				mStorageFirstVisibleItem = firstVisibleItem;
				mStorageVisibleItemCount = visibleItemCount;
			}
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}
		});
		storage_list.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent , View view,
					int position, long id) {
				Log.e(TAG,"storage list item Position"+position);
				storageDeviceIndex = position;
				storage_list_item_pos = position;
				refreshRecordList();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		recordListAdapter = new RecordListAdapter(this);
		record_list.setAdapter(recordListAdapter);
		record_list.setVisibleItemCount(9);
		record_list.setCustomScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				mRecordFirstVisibleItem = firstVisibleItem;
				mRecordVisibleItemCount = visibleItemCount;
			}
		});
		record_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View v, int position,long id)
			{
				Log.e(TAG,"record list item Position"+position);
				record_list_item_pos = position;
				
				if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
				{
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
				record_list_item_pos = 0;
			}
		});
				
		//play view message handler
		initMessageHandle();		
	}
	
	private void initListener() {
		storage_list.setCustomKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(event.getAction() == KeyEvent.ACTION_DOWN)
				{
					switch (keyCode) {
						case KeyEvent.KEYCODE_DPAD_CENTER:
						case KeyEvent.KEYCODE_DPAD_LEFT:
						case KeyEvent.KEYCODE_DPAD_RIGHT:
						{
							if(record_list.getCount() <=0)
							{
								return true;
							}
							record_list.requestFocus();
							record_list.setSelectionFromTop(record_list_item_pos, mRecordFirstVisibleItem);
							recordListAdapter.notifyDataSetChanged();

							if(record_list.getSelectedView()!= null)
    							record_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);

							{
								int pos = record_list.getSelectedItemPosition();
								if(pos>=0)
								{
								    if(storage_list.getSelectedView()!= null)
    									storage_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
    									
									record_list.requestFocus();

									if(record_list.getSelectedView()!= null)
    									record_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
								}
								else
								{
									
								}
							}
//							record_list.setSelectionFromTop(position, y);
							return true;
						}
						
						case KeyEvent.KEYCODE_DPAD_UP:
						case KeyEvent.KEYCODE_DPAD_DOWN:
						{
							if((storage_list!= null)&&(storage_list.getCount()>1))
							{
								if(record_list.getSelectedView()!= null)
								{
									record_list.getSelectedView().setBackgroundResource(R.drawable.listview_item_bg_selector);
								}
								record_list_item_pos = 0;
								record_list.setCustomSelection(record_list_item_pos);
							}
							
							int currentPos = storage_list.getSelectedItemPosition();

							if(storage_list.getSelectedView()!= null)
    							storage_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
							break;
						}
							
						default:
							break;
					}
				}
				else
				{
					switch(keyCode)
					{
						case KeyEvent.KEYCODE_DPAD_CENTER:
						case KEYCODE_DELETE:
						//disable system Action
						return true;
					}
				}
				return false;
			}
		});	
		
		record_list.setCustomKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View view, int keyCode, KeyEvent event) {
				if(event.getAction() == KeyEvent.ACTION_DOWN)
				{
					switch (keyCode) {
					case KeyEvent.KEYCODE_DPAD_LEFT:
					case KeyEvent.KEYCODE_DPAD_RIGHT:
					{
						storage_list.requestFocus();
						storage_list.setSelectionFromTop(storageDeviceIndex, mStorageFirstVisibleItem);
						storageAdapter.notifyDataSetChanged();

						if(storage_list.getSelectedView()!= null)
    						storage_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);

						int currentPos = record_list.getSelectedItemPosition();
						if(currentPos>=0)
						{
						    if(record_list.getSelectedView()!= null)
    							record_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
						}
						return true;
					}
					
					case KeyEvent.KEYCODE_DPAD_CENTER:
					{
						
						if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
						{
							if(MW.pvr_playback_start(record_list_item_pos) == MW.RET_OK)
							{
								ll_recordListUI.setVisibility(View.GONE);
								Hidemanline();
								ShowPlayInfoView();									
							}							
						}
						
						return true;
					}
					
					case KEYCODE_DELETE://delete
					{
						PvrFile_Delete();
						return true;
					}
					
					case KEYCODE_RENAME://rename
					{
						PvrFile_Rename();
						return true;
					}
					
					case KEYCODE_SELECT://select 
					{
	                 	if(record_list.getVisibility() == View.VISIBLE)
	                 		RefreshPvrSelectImage();
	                 	return true;
					}
					default:
						break;
					}
				}
				else
				{
					switch(keyCode)
					{
						case KeyEvent.KEYCODE_DPAD_CENTER:
						case KEYCODE_DELETE:
						//disable system Action
						return true;
					}
				}

				return false;
			}
		});
	}

	public class DeleteDia extends Dialog {
		
		private LayoutInflater mInflater = null ;
		private Context mContext = null ;
		View mLayoutView = null ;
		Button mBtnStart = null ;
		Button mBtnCancel = null ;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.common_alert_dialog) ;
			mInflater = LayoutInflater.from(mContext) ;
			mLayoutView = mInflater.inflate(R.layout.common_alert_dialog, null) ;
			mBtnStart = (Button) findViewById(R.id.dialog_button_ok) ;
			mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel) ;
			
			mBtnCancel.requestFocus();
			mBtnCancel.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					DeleteDia.this.dismiss() ;
				}
			}) ;
			
			mBtnStart.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					getSelPvrFileInfo();
					int[] select_pvr_index_list = new int[SelPvrList.size()]; 
					for (int i = 0; i < SelPvrList.size(); i++) {
						select_pvr_index_list[i] = SelPvrList.get(i).intValue();
						System.out.println("delete select_pvr_index_list: "+select_pvr_index_list[i]);
					}
					int Ret = MW.pvr_record_delete_record(select_pvr_index_list);
					if(Ret == MW.RET_OK)
					{
						System.out.println("save delete data ");
						{
							initPvrSelStatus();
							refreshRecordList();
						}
						recordListAdapter.notifyDataSetChanged();
						System.out.println("recordListCount : "+recordListCount);
						if(recordListCount == 0)
						{
							storage_list.requestFocus();
							if(storage_list.getSelectedView() != null)
								storage_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
						}
						DeleteDia.this.dismiss() ;
					}else
					{
						ShowToastInformation(getResources().getString(R.string.delete_fail), 0);
						return ;
					}
				}	
			}) ;
			 
		}
	
		public DeleteDia(Context context, int theme) {
			super(context, theme);
			mContext = context ;
		}
		
		public DeleteDia(Context context) {
			super(context);
			mContext = context ;
		}
		
		@Override
		public boolean onKeyUp(int keyCode, KeyEvent event) {
			return super.onKeyUp(keyCode, event);
		}
	}

	public class EditDialog extends Dialog {
		private LayoutInflater mInflater = null ;
		private Context mContext = null ;
		View mLayoutView = null ;
		Button mBtnStart = null ;
		Button mBtnCancel = null ;
		TextView mTitle = null;
		TextView item_no = null;
		EditText mFileName = null;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.pvr_edit_custom_dia) ;
			mInflater = LayoutInflater.from(mContext) ;
			mBtnStart = (Button) findViewById(R.id.dialog_button_ok) ;
			mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel) ;
			mTitle = (TextView) findViewById(R.id.title);
			item_no = (TextView) findViewById(R.id.item_no);
			mFileName = (EditText) findViewById(R.id.edittext_pvr_name);
			
			
			mTitle.setText(getResources().getString(R.string.rename));
			
			if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
			{
				item_no.setText((record_list_item_pos+1)+"");
				mFileName.setText(pvrRecord.service_name);
				mFileName.setSelection(0,mFileName.length());
			}else
			{
				item_no.setText("");
				mFileName.setText("");
			}
			
			
			mBtnCancel.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					EditDialog.this.dismiss() ;
				}

			}) ;
			
			mBtnStart.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
				
					if(mFileName.getText().toString().length() < 1) {
	                    Common.makeText(RecordListActivity.this, (R.string.sat_name_null), Toast.LENGTH_SHORT);
						mFileName.requestFocus();
						mFileName.selectAll();
	                    return ;
	                }
	                
	                pvrRecord.service_name = mFileName.getText().toString();
	                System.out.println("pvrRecord.service_name :"+pvrRecord.service_name);
	                int Ret = MW.pvr_record_change_record_name(record_list_item_pos, pvrRecord.service_name) ;
	                
	                if(Ret == MW.RET_OK)
	                {
	                	System.out.println("save edit data ");
						recordListAdapter.notifyDataSetChanged();
	                    EditDialog.this.dismiss();
	                }else
	                {
	                	System.out.println("Ret == "+Ret);
	                }
	                
				}

			}) ;
			 
		}

		public EditDialog(Context context, int theme) {
			super(context, theme);
			mContext = context ;
		}
		
		public EditDialog(Context context) {
			super(context);
			mContext = context ;
		}

		@Override
		public boolean onKeyUp(int keyCode, KeyEvent event) {
			return super.onKeyUp(keyCode, event);
		}
	}

	private void PvrFile_Delete() {		
		Dialog deleteDialog = new DeleteDia(this,R.style.MyDialog) ;
		deleteDialog.show() ;
		Common.setWindow_Attributes(deleteDialog);		
	}
	
	private void PvrFile_Rename() {
		if(MW.pvr_record_get_record_info(record_list_item_pos, pvrRecord) == MW.RET_OK)
		{
			Dialog editDialog = new EditDialog(this,R.style.MyDialog) ;
			editDialog.setContentView(R.layout.pvr_edit_custom_dia) ;
			editDialog.show() ;
			Common.setWindow_Attributes(editDialog);
		}
	}
	
	class StorageAdapter extends BaseAdapter
	{
		private Context mContext;
		private LayoutInflater mInflater;

		public StorageAdapter(Context context) {
			super();
			this.mContext = context;
			this.mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return storageDeviceCount;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Log.e(TAG, "storage adapter position : "+ position);
			
			ViewHolder localViewHolder;
			if(convertView == null)
			{
				convertView = mInflater.inflate(R.layout.storage_list_item, null);
				localViewHolder = new ViewHolder();
				localViewHolder.stroage_name = (TextView) convertView.findViewById(R.id.storage_name);
				convertView.setTag(localViewHolder);
			}
			else
			{
				localViewHolder = (ViewHolder) convertView.getTag();
			}
			Drawable drawable = mContext.getResources().getDrawable(R.drawable.selector_item);
			convertView.setBackgroundDrawable(drawable);

			StorageDevice.DeviceItem item = storageDeviceInfo.getDeviceItem(position);
			if(item != null){
//				System.out.println(item.VolumeName.indexOf("[udisk")+""+item.VolumeName);
				if(item.VolumeName.indexOf("[udisk") ==1)
				{
					localViewHolder.stroage_name.setText(getResources().getString(R.string.udisk)+" "+(storageDeviceCount-1-position));
				}
				else
					localViewHolder.stroage_name.setText(item.VolumeName);
			}
			else{
				localViewHolder.stroage_name.setText("");
			}

			return convertView;
		}

		class ViewHolder
		{
			TextView stroage_name ;
		}
	}

	class RecordListAdapter extends BaseAdapter
	{
		private Context mContext;
		private LayoutInflater mInflater;

		public RecordListAdapter(Context context) {
			super();
			this.mContext = context;
			this.mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			System.out.println(""+recordListCount);
			return recordListCount;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder localViewHolder;
			if(convertView == null)
			{
				convertView = mInflater.inflate(R.layout.pvr_program_list_item, null);
				localViewHolder = new ViewHolder();
				localViewHolder.num = (TextView) convertView.findViewById(R.id.num);
				localViewHolder.select_img = (ImageView) convertView.findViewById(R.id.select_img);
				localViewHolder.pvr_program_name = (TextView) convertView.findViewById(R.id.pvr_program_name);
				localViewHolder.size = (TextView) convertView.findViewById(R.id.size);
				localViewHolder.time = (TextView) convertView.findViewById(R.id.time);

				convertView.setTag(localViewHolder);
			}
			else
			{
				localViewHolder = (ViewHolder) convertView.getTag();
			}
			Drawable drawable = mContext.getResources().getDrawable(R.drawable.selector_item);
			convertView.setBackgroundDrawable(drawable);

			localViewHolder.num.setText(""+(position+1));

			if(MW.pvr_record_get_record_info(position, pvrRecord) == MW.RET_OK){
				localViewHolder.pvr_program_name.setText(pvrRecord.service_name);
				if(pvrRecord.file_size/1024/1024/1024 >=1)
					localViewHolder.size.setText(Common.stringFormat("%.2f G", (float)(pvrRecord.file_size)/1024/1024/1024));
				else if(pvrRecord.file_size/1024/1024 >= 1)
					localViewHolder.size.setText(Common.stringFormat("%d MB", (pvrRecord.file_size)/1024/1024));
				else if(pvrRecord.file_size/1024 >= 1 )
					localViewHolder.size.setText(Common.stringFormat("%d KB", pvrRecord.file_size/1024));
				else
					localViewHolder.size.setText(Common.stringFormat("%d B",pvrRecord.file_size/1024));

	
				localViewHolder.time.setText(Common.stringFormat("%04d-%02d-%02d", pvrRecord.record_year, pvrRecord.record_month, pvrRecord.record_day));
				
				if(PvrSelStatus!= null)
				{
					System.out.println("not null :"+ position);
					if(PvrSelStatus[storageDeviceIndex]!= null)
					{
						System.out.println("PvrSelStatus[ "+storageDeviceIndex+"]["+position+"]"+ PvrSelStatus[storageDeviceIndex][position]);
						if(PvrSelStatus[storageDeviceIndex][position])
							localViewHolder.select_img.setVisibility(View.VISIBLE);
						else
							localViewHolder.select_img.setVisibility(View.INVISIBLE);
					}else
					{
						localViewHolder.select_img.setVisibility(View.INVISIBLE);
					}
				}else
				{
					System.out.println("null :"+ position);
				}
			}
			else{
				localViewHolder.pvr_program_name.setText("");
				localViewHolder.size.setText("");
				localViewHolder.time.setText("");
			}

			return convertView;
		}

		class ViewHolder
		{
			TextView num ;
			ImageView select_img;
			TextView pvr_program_name ;
			TextView size ;
			TextView time ;
		}

	}
    
    private int[]  audio_pid_pos =null;
    private int[]  audio_channel_pos =null;
    public class AudioInfoDialog extends Dialog {
        private LayoutInflater mInflater = null ;
        private CustomListView mListView = null ;
        private Context mContext = null ;
        int nTrack ;
        View mLayoutView = null ;
        TextView mAudioTrack = null ;
        private CharSequence[] mListItem;
        private ArrayAdapter mAdapter ;
        
        private int prog_num=0;//prog_num infer to  palyback program number 
        private int count =0;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.audio_info_dialog) ;

			dia_currentDialog = this;
			
            mListView = (CustomListView) findViewById(R.id.lvListItem);
            mAudioTrack = (TextView) findViewById(R.id.info);

			switch(audio_channel_pos[prog_num]){
				case MW.AUDIO_CHANNEL_LEFT:
	                mAudioTrack.setText(R.string.left);
					break;
				case MW.AUDIO_CHANNEL_RIGHT:
	                mAudioTrack.setText(R.string.right);
					break;
				case MW.AUDIO_CHANNEL_STEREO:
				default:
	                mAudioTrack.setText(R.string.stereo);
					break;
			}

            mInflater = LayoutInflater.from(mContext) ;
            mLayoutView = mInflater.inflate(R.layout.single_selection_list_item, null) ;

            if(pvrRecordInfo.audio_info.length>0)
            {
            	String audio_type;
            	List<String> languages;
				int i;

				languages = new ArrayList<String>();

				languages.clear();
				for(i=0 ; i<pvrRecordInfo.audio_info.length ; i++){
						audio_type = getResources().getStringArray(R.array.audio_type)[pvrRecordInfo.audio_info[i].audio_stream_type];
						if((pvrRecordInfo.audio_info[i].ISO_639_language_code.length()>0)&&(pvrRecordInfo.audio_info[i].ISO_639_language_code.charAt(0) != '\0'))
							languages.add(pvrRecordInfo.audio_info[i].ISO_639_language_code+" ("+audio_type+")");	
						else
							languages.add("Audio " + (i+1)+" ("+audio_type+")");
				}

                mAdapter = new ArrayAdapter(RecordListActivity.this,R.layout.single_selection_list_item, R.id.ctvListItem, languages) ;
                mListView.setAdapter(mAdapter);

                mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                mListView.setItemChecked(audio_pid_pos[prog_num],true);
                mListView.setCustomSelection(audio_pid_pos[prog_num]);
                mListView.setVisibleItemCount(5);
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(audio_pid_pos[prog_num] != position){
                        	System.out.println("position : "+position);
                        	System.out.println("prog_num : " + prog_num);
                        	audio_pid_pos[prog_num] = position;
                        	MW.pvr_playback_switch_audio(pvrRecordInfo.audio_info[position].audio_pid, pvrRecordInfo.audio_info[position].audio_stream_type);
                        }
                        
                        AudioInfoDialog.this.dismiss();
                    }
                });
            }

        }

		@Override
		protected void onStop() {
			super.onStop();

			dia_currentDialog = null;
		}

        public AudioInfoDialog(Context context, int theme,int i,int count) {
            super(context, theme);
            mContext = context ;
            System.out.println("count : "+ count );
            System.out.println("i : "+ i );

            this.prog_num = i;
            this.count = count;
        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event) {

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
				{
                    if(audio_channel_pos[prog_num] == MW.AUDIO_CHANNEL_LEFT){
                        audio_channel_pos[prog_num] = MW.AUDIO_CHANNEL_STEREO;
                        mAudioTrack.setText(R.string.stereo);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_STEREO);
                    }
					else if(audio_channel_pos[prog_num] == MW.AUDIO_CHANNEL_RIGHT) {
                        audio_channel_pos[prog_num] = MW.AUDIO_CHANNEL_LEFT;
                        mAudioTrack.setText(R.string.left);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_LEFT);
                    }
					else{
                        audio_channel_pos[prog_num] = MW.AUDIO_CHANNEL_RIGHT;
                        mAudioTrack.setText(R.string.right);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_RIGHT);
                    }
            	}
				return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT:
				{
                    if(audio_channel_pos[prog_num] == MW.AUDIO_CHANNEL_LEFT){
                        audio_channel_pos[prog_num] = MW.AUDIO_CHANNEL_RIGHT;
                        mAudioTrack.setText(R.string.right);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_RIGHT);
                    }
					else if(audio_channel_pos[prog_num] == MW.AUDIO_CHANNEL_RIGHT) {
                        audio_channel_pos[prog_num] = MW.AUDIO_CHANNEL_STEREO;
                        mAudioTrack.setText(R.string.stereo);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_STEREO);
                    }
					else{
                        audio_channel_pos[prog_num] = MW.AUDIO_CHANNEL_LEFT;
                        mAudioTrack.setText(R.string.left);
                        MW.ts_player_play_switch_audio_channel(MW.AUDIO_CHANNEL_LEFT);
                    }
                }
                return true;

				case KEYCODE_AUDIO:
					this.dismiss();
					return true;

				default:
					break;
            }

            return super.onKeyDown(keyCode, event);
        }
    }
    
    public class PvrRepeatDialog extends Dialog {
        private LayoutInflater mInflater = null ;
        private CustomListView mListView = null ;
        private Context mContext = null ;
        View mLayoutView = null ;
        private ArrayAdapter mAdapter ;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.video_repeat_dialog) ;

			dia_currentDialog = this;
			
            mListView = (CustomListView) findViewById(R.id.videoListItem);

            mInflater = LayoutInflater.from(mContext) ;
            mLayoutView = mInflater.inflate(R.layout.single_selection_list_item, null) ;

			List<String> repeat_type;
			
			repeat_type = new ArrayList<String>();
			repeat_type.clear();

			repeat_type.add(getString(R.string.None));
			repeat_type.add(getString(R.string.repeat_single));
			repeat_type.add(getString(R.string.repeat_all));
			
			mAdapter = new ArrayAdapter(RecordListActivity.this,R.layout.single_selection_list_item, R.id.ctvListItem, repeat_type) ;
			mListView.setAdapter(mAdapter);
			
			mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mListView.setVisibleItemCount(5);
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					video_repeat_status = position;
					Log.e(TAG,"position : "+position);
					PvrRepeatDialog.this.dismiss();
				}
			});	

			mListView.setCustomSelection(video_repeat_status);				
        }

		@Override
		protected void onStop() {
			super.onStop();

			dia_currentDialog = null;
		}

        public PvrRepeatDialog(Context context, int theme) {
            super(context, theme);
            mContext = context ;
        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event) {

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
				
				return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT:
				
                return true;

				default:
					break;
            }

            return super.onKeyDown(keyCode, event);
        }
    }    
	
	void ShowToastInformation(String text,int duration_mode)
	{
		LayoutInflater inflater_toast = LayoutInflater.from(this);
		LinearLayout toast_view = (LinearLayout)inflater_toast.inflate(R.layout.toast_main,null);
		TextView toast_tv = (TextView) toast_view.findViewById(R.id.toast_text);
		toast_tv.setText(text);
		Toast toast = new Toast(this);
		if(duration_mode==0)
        toast.setDuration(Toast.LENGTH_SHORT);
		else if(duration_mode==1)
			toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(toast_view);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.record_list_activity);
		initView();
		initVideoView();
		initPlayView();
		initData();
		initListener();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if(dialog != null)
		{
			if(dialog.isShowing())
				dialog.dismiss();
		}
		
		enableMwMessageCallback(mwMsgHandler);
		
		if(storageDeviceInfo == null)
			storageDeviceInfo = new StorageDevice(this);

		storageDevicePlugReceiver = new StorageDevicePlugReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(StorageDevice.ACTION_DEVICE_MOUNTED);
		filter.addAction(StorageDevice.ACTION_DEVICE_REMOVED);
		registerReceiver(storageDevicePlugReceiver, filter);

		refreshStorageDeviceList();

		ll_recordListUI.setVisibility(View.VISIBLE);
		Showmanline();
		storage_list.requestFocus();
	}

	@Override
	protected void onPause() {
		super.onPause();

		if(dialog!= null)
    	{
    		if(dialog.isShowing())
    		{
        		dialog.dismiss();
			}
    	}
		
		if(dia_currentDialog!=null)
		{
			dia_currentDialog.dismiss();
		}
		
		if(initflag == true)
			MW.pvr_playback_stop();
			
		storageDeviceInfo.finish(this);

		storageDeviceInfo = null;

		if(storageDevicePlugReceiver != null){
			unregisterReceiver(storageDevicePlugReceiver);
			storageDevicePlugReceiver = null;
		}

		MW.pvr_record_exit_record_list();
		
		enableMwMessageCallback(null);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
