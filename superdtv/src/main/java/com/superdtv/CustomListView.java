package com.superdtv;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.BaseAdapter;
import android.view.KeyEvent;
import android.util.Log;


public class CustomListView extends ListView implements OnKeyListener, OnScrollListener
{
	private final static String TAG = "CustomListView";
	private int mFirstVisibleItem = 0;
	private int mVisibleItemCount = 0, mUserVisibleItemCount = 0;
	private int mCurrentSelectId = 0;
	private OnKeyListener mOnKeyListener = null;
	private OnScrollListener mOnScrollListener = null;
	private boolean mIsUsePageUpDownKey = true;
	private boolean mIsHandleLeftRightAsPageUpDownKey = false;
			
	public CustomListView(Context context) 
	{
		super(context);
		setOnKeyListener(this); 
		setOnScrollListener(this);
	}

	public CustomListView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		setOnKeyListener(this);
		setOnScrollListener(this);
	}

	public CustomListView(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		setOnKeyListener(this); 
		setOnScrollListener(this);
	}

	public void setVisibleItemCount(int count) 
	{
		mVisibleItemCount = count;
		mUserVisibleItemCount = mVisibleItemCount;
	}

	public void setCustomKeyListener(View.OnKeyListener l) 
	{
		mOnKeyListener = l;	
	}

	public void setCustomScrollListener(AbsListView.OnScrollListener l)
	{
		mOnScrollListener = l;	
	}

	public void setCustomSelection(int position)
	{
		super.setSelection(position);
		mCurrentSelectId = position;
	}

	public void setValidPageUpDownKey(boolean IsUse) 
	{
		mIsUsePageUpDownKey = IsUse;
	}
	
	public void setHandleLeftRightAsPageUpDownKey(boolean handle) 
	{
		mIsHandleLeftRightAsPageUpDownKey = handle;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) 
	{
		super.onSizeChanged(w, h, oldw, oldh);
		Log.d(TAG, "======onSizeChanged() mCurrentSelectId "+mCurrentSelectId+" mVisibleItemCount "+mVisibleItemCount+" h "+h);
		if (mCurrentSelectId!=0 && mVisibleItemCount > 0 && h > 0)
		{
			int y = (mCurrentSelectId%mVisibleItemCount)*(h/mVisibleItemCount);
			Log.d(TAG, "======onSizeChanged() "+y);
			setSelectionFromTop(mCurrentSelectId, y);
		}	
	}
	
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
	{
		mFirstVisibleItem = firstVisibleItem;
		mVisibleItemCount = visibleItemCount;     
		mCurrentSelectId = view.getSelectedItemPosition();
		if (mOnScrollListener != null)
			mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) 
	{
		if (mOnScrollListener != null)
			mOnScrollListener.onScrollStateChanged(view, scrollState);
	}

	private void actionPageUpDown(boolean up)
	{
		if(up){
			if (mCurrentSelectId >= getCount() - 1)
			{
				mCurrentSelectId = 0;
			}
			else
			{						
				mCurrentSelectId += mUserVisibleItemCount;
				if (mCurrentSelectId > getCount() - 1) 
				{
					mCurrentSelectId = getCount() - 1;
				}
			}
			setSelection(mCurrentSelectId);
			((BaseAdapter)getAdapter()).notifyDataSetChanged(); //we need to notify here to keeep selected in the middle
		}
		else{
			if (mCurrentSelectId <= 0)
			{
				mCurrentSelectId = getCount() - 1;
			}
			else
			{
				mCurrentSelectId -= mUserVisibleItemCount;
				if (mCurrentSelectId < 0) 
				{
					mCurrentSelectId = 0;
				}
			}
			setSelection(mCurrentSelectId);					
			((BaseAdapter)getAdapter()).notifyDataSetChanged(); //we need to notify here to keeep selected in the middle
		}
	}
		
	public boolean onKey(View v, int keyCode, KeyEvent event) 
	{
		if (event.getAction() == KeyEvent.ACTION_DOWN)
		{
			boolean handleLeftRightAsPageUpDown = false;

			if(mUserVisibleItemCount == 0)
				mUserVisibleItemCount = mVisibleItemCount;
			
			switch(keyCode)
			{
				case KeyEvent.KEYCODE_DPAD_LEFT:
				{
					if(mIsHandleLeftRightAsPageUpDownKey){
						actionPageUpDown(false);
						return true;
					}

					break;
				}
					
				case KeyEvent.KEYCODE_PAGE_UP:
				{
					if(mIsUsePageUpDownKey == true){
						actionPageUpDown(true);
						return true;
					}

					break;
				}

				case KeyEvent.KEYCODE_DPAD_RIGHT:
				{
					if(mIsHandleLeftRightAsPageUpDownKey){
						actionPageUpDown(true);
						return true;
					}

					break;
				}					
				
				case KeyEvent.KEYCODE_PAGE_DOWN:
				{
					if(mIsUsePageUpDownKey == true){
						actionPageUpDown(false);
						return true;
					}

					break;
				}	
				
				case KeyEvent.KEYCODE_DPAD_UP:
				{
//					System.out.println("do you execute ");
					if (mCurrentSelectId <= 0)
					{
						mCurrentSelectId = getCount() - 1;
						setSelection(mCurrentSelectId);
						return true;							
					}	
					else if (mCurrentSelectId == mFirstVisibleItem  && mCurrentSelectId > 0)
					{
						setSelection(mCurrentSelectId-1);
						return true;							
					}						
					else
					{
						mCurrentSelectId = getSelectedItemPosition();		
					}
					
					break;
				}
				
				case KeyEvent.KEYCODE_DPAD_DOWN:
				{
					if (mCurrentSelectId >= getCount() - 1) 
					{
						mCurrentSelectId = 0;
						setSelection(mCurrentSelectId);
						return true;							
					}
					else if ((mCurrentSelectId+1 == mFirstVisibleItem+mVisibleItemCount)  && mCurrentSelectId < (getCount() - 1))
					{
						setSelection(mCurrentSelectId+1);
						return true;								
					}						
					else
					{
						mCurrentSelectId = getSelectedItemPosition();
					}
					break;
				}
			}
		}

		if (mOnKeyListener != null)
			return mOnKeyListener.onKey(v, keyCode, event);
		else
			return false;
	}

}

