package com.superdtv;

import android.app.AlertDialog;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.os.Bundle;
import android.os.SystemClock;
//import android.os.ServiceManager;
import android.os.RemoteException;
//import android.hardware.input.IInputManager;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ImageView;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;

import java.util.ArrayList;
import java.util.List;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;



public class ChooseListDialog extends AlertDialog
{
	private final static String TAG = "ChooseListDialog";
	private Context mContext = null;

	private TextView mTitleTextView;
	private LinearLayout mChooseLinearLayout;
	private CustomListView mChooseList;
	private List<String> mChooseStringList = null;
	private ChooseListAdapter mChooseListAdapter = null;
	private AdapterView.OnItemClickListener mChooseListItemClickListener;
	int mSelectedId = 0;
	int mTitleResId = 0;
	int mTextResId = 0;
	int mHighLightIndex = 0;

	public ChooseListDialog(Context context, int titleResId, int textResId, int highlightIndex, AdapterView.OnItemClickListener listener)
	{
		super(context, R.style.MyDialog);
		mContext = context;

		Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mTextResId = textResId;
		mHighLightIndex = highlightIndex;
		mChooseListItemClickListener = listener;
		if (mChooseListItemClickListener == null)
		{
			mChooseListItemClickListener = new AdapterView.OnItemClickListener ()
			{
				public void onItemClick(AdapterView parent, View v, int position, long id){
					switch (position)
					{
						case 0:
							if (mSelectedId != 0)
							{

							}
							break;

						default:
							break;
					}
				}
			};
		}
	}

	public ChooseListDialog(Context context, int titleResId, ArrayList<String> textList, int highlightIndex, AdapterView.OnItemClickListener listener)
	{
		super(context, R.style.MyDialog);
		mContext = context;

		Log.d(TAG, " highlightIndex "+highlightIndex);
		mTitleResId = titleResId;
		mChooseStringList = textList;
		mHighLightIndex = highlightIndex;
		mChooseListItemClickListener = listener;
		if (mChooseListItemClickListener == null)
		{
			mChooseListItemClickListener = new AdapterView.OnItemClickListener ()
			{
				public void onItemClick(AdapterView parent, View v, int position, long id){
					switch (position)
					{
						case 0:
							if (mSelectedId != 0)
							{

							}
							break;

						default:
							break;
					}
				}
			};
		}
	}

	public ChooseListDialog(Context context)
	{
		super(context);
		mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_list_dialog);

		mTitleTextView = (TextView)findViewById(R.id.choose_title_textview);
		mTitleTextView.setText(mTitleResId);

		mChooseList = (CustomListView)findViewById(R.id.choose_listview);
		if (mTextResId > 0)
		{
			mChooseStringList = new ArrayList<String>();
			String[] secStrArray = mContext.getResources().getStringArray(mTextResId);
			for (String secStr : secStrArray)
			{
				mChooseStringList.add(secStr);
			}
		}

		mChooseListAdapter = new ChooseListAdapter(mContext);
        	mChooseList.setAdapter(mChooseListAdapter);
		mChooseList.setOnItemClickListener(mChooseListItemClickListener);
		mChooseList.setCustomKeyListener(new ChooseListOnKeyListener());
		mChooseList.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
			{
				mChooseListAdapter.setSelectItem(position);
				mSelectedId = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent)
			{
				// ignored
			}
		});

		if (mChooseStringList.size()>0)
		{
			mChooseLinearLayout = (LinearLayout)findViewById(R.id.choose_linearlayout);
			LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) mChooseLinearLayout.getLayoutParams();
			{
	/*		    if(MW.get_system_platform_type() == MW.PLATFORM_GXB)	
    				linearParams.height = mChooseStringList.size() < 10 ? mChooseStringList.size()*70*(SETTINGS.getWindowHeight()/720) : 630*(SETTINGS.getWindowHeight()/720);
    			else	
    				linearParams.height = mChooseStringList.size() < 11 ? mChooseStringList.size()*48*(SETTINGS.getWindowHeight()/720) : 480*(SETTINGS.getWindowHeight()/720);*/
			}
			//linearParams.height = mChooseStringList.size()*50;
			mChooseLinearLayout.setLayoutParams(linearParams);
			mChooseList.setCustomSelection(mHighLightIndex);
		}
	}

	protected void sendKeyEvent(final int keyCode)
	{
		new Thread(new Runnable() {
			@Override
			public void run() {
				Instrumentation mInst = new Instrumentation();
				mInst.sendKeyDownUpSync(keyCode);
			}
		}).start();
	}
	
	private class ChooseListOnKeyListener implements View.OnKeyListener
	{
		public boolean onKey(View arg0, int arg1, KeyEvent arg2)
		{
			if (arg2.getAction() == KeyEvent.ACTION_DOWN)
			{
				switch(arg1)
				{
        			case KeyEvent.KEYCODE_DPAD_LEFT:
        			    sendKeyEvent(KeyEvent.KEYCODE_PAGE_DOWN);
        			    return true;
        			    
        			case KeyEvent.KEYCODE_DPAD_RIGHT:
        			    sendKeyEvent(KeyEvent.KEYCODE_PAGE_UP);
        			    return true;				
				
				}
			}

			return false;
		}
	}

	private  class ChooseListAdapter extends BaseAdapter
	{
		private LayoutInflater mInflater;
		private Context cont;
		private int selectItem;

		class ViewHolder
		{
			TextView text;
			ImageView icon;
		}

		public ChooseListAdapter(Context context)
		{
			super();
			cont = context;
			mInflater=LayoutInflater.from(context);
		}

		public int getCount()
		{
			return mChooseStringList!=null ? mChooseStringList.size() : 0;
		}

		public Object getItem(int position)
		{
			return position;
		}

		public long getItemId(int position)
		{
			return position;
		}

		public void setSelectItem(int position)
		{
			selectItem = position;
		}

		public int getSelectItem()
		{
			return selectItem;
		}

		public View getView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder=null;
			if (convertView == null)
			{
				convertView = mInflater.inflate(R.layout.choose_list_dialog_list_item, null);

				holder = new ViewHolder();

				holder.icon =  (ImageView) convertView.findViewById(R.id.icon);
				holder.text = (TextView) convertView.findViewById(R.id.text);

				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			
			Drawable drawable = mContext.getResources().getDrawable(R.drawable.selector_item);
			convertView.setBackgroundDrawable(drawable);

			holder.text.setText(mChooseStringList.get(position));
			//holder.text.setTextColor(Color.WHITE);
/*
			if (position == mHighLightIndex)
			{
				holder.icon.setImageResource(R.drawable.check_pressed);
			}
			else
			{
				holder.icon.setImageResource(R.drawable.check_default);
			}
*/

			return convertView;
		}
	}
}


