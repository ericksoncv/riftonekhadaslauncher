package com.superdtv;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.view.View.OnFocusChangeListener;


public class Ter_ManualSearchActivity extends DtvBaseActivity
{
	private static final String TAG = "Ter_ManualSearchActivity";
	
	private ComboLayout TpNo = null;
	private ComboLayout Band = null;
	private ComboEditText Freq = null;
	private ComboEditText Symbolrate = null;
	private LinearLayout Band_ll = null;
	private LinearLayout Symbolrate_ll = null;
	
	private TextView txv_signalStrength, txv_signalQuality;
	private ProgressBar prg_signalStrength, prg_signalQuality;
	private ProgressBar prg_signalStrength_unlock, prg_signalQuality_unlock;
	
	private Handler mwMsgHandler;

	private int system_type;
	
	private mw_data.dvb_t_tp tpInfo = new mw_data.dvb_t_tp();
	
	private int currentTpIndex = 0;
	
	boolean fSaveAreaTpFlag = false;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.ter_manual_search_activity);

		system_type = getIntent().getIntExtra("system_type", 0);

		if(system_type == MW.SYSTEM_TYPE_DVB_T){
			((TextView) findViewById(R.id.TextView_SystemType)).setText(getResources().getString(R.string.dvb_t_t2));
		}

		initData();
		initView();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();

		RefreshAllData();
		
		enableMwMessageCallback(mwMsgHandler);	
		MW.register_event_type(MW.EVENT_TYPE_TUNER_STATUS, true);

		LockCurrentTP();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		
		if(MW.db_ter_get_current_tp_info(system_type, tpInfo) == MW.RET_OK){
			if(tpInfo.tp_index != currentTpIndex){
				MW.db_ter_set_current_tp(system_type, currentTpIndex);
			}
		}
		else{
			MW.db_ter_set_current_tp(system_type, 0);
		}
		
		if(fSaveAreaTpFlag){
			fSaveAreaTpFlag = false;

			MW.db_ter_save_area_tp();
		}

		enableMwMessageCallback(null);

		MW.tuner_unlock_tp(MW.MAIN_TUNER_INDEX, system_type, false);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(event.getAction() == KeyEvent.ACTION_DOWN)
		{
			switch (keyCode)
			{
			case KeyEvent.KEYCODE_1:
				{
					ScanAction();
				}
				break;
				
			case KeyEvent.KEYCODE_DPAD_UP:
				if(TpNo.isFocused())
				{	
					if(Band_ll.getVisibility() != View.GONE){
    					Band.requestFocus();
    				}
					else if(Symbolrate_ll.getVisibility() != View.GONE){
    					Symbolrate.getInfoTextView().requestFocus();
    					//Symbolrate.getInfoTextView().setSelection(Symbolrate.getInfoTextView().length());
    					Symbolrate.setBackgroundResource(R.drawable.ui_enable_css);
    				}
    				else{	
    					Freq.getInfoTextView().requestFocus();
    					//Freq.getInfoTextView().setSelection(Freq.getInfoTextView().length());
    					Freq.setBackgroundResource(R.drawable.ui_enable_css);
    				}

    				return true;
				}
			    else if(Band.isFocused())
				{	
					Freq.getInfoTextView().requestFocus();
					//Freq.getInfoTextView().setSelection(Freq.getInfoTextView().length());
					Freq.setBackgroundResource(R.drawable.ui_enable_css);
					return true;
				}
				else if(Freq.getInfoTextView().isFocused())
				{
					if(Freq.getInfoTextView().length()==0)
					{
						Freq.SetInfoTextView(""+tempFreq);
					}
					Freq.getInfoTextView().clearFocus();
					Freq.setBackgroundResource(R.drawable.item_bg_bor);
					TpNo.requestFocus();
					return true;
				}
				else if(Symbolrate.getInfoTextView().isFocused())
				{
					if(Symbolrate.getInfoTextView().length()==0)
					{
						Symbolrate.SetInfoTextView(""+tempSymbolrate);
					}
					Symbolrate.getInfoTextView().clearFocus();
					Symbolrate.setBackgroundResource(R.drawable.item_bg_bor);
					
					Freq.getInfoTextView().requestFocus();
					//Freq.getInfoTextView().setSelection(Freq.getInfoTextView().length());
					Freq.setBackgroundResource(R.drawable.ui_enable_css);				
					return true;
				}
				
				break;
			case KeyEvent.KEYCODE_DPAD_DOWN:
				if(TpNo.isFocused())
				{	
					Freq.getInfoTextView().requestFocus();
					//Freq.getInfoTextView().setSelection(Freq.getInfoTextView().length());
					Freq.setBackgroundResource(R.drawable.ui_enable_css);
					return true;
				}
				else if(Freq.getInfoTextView().isFocused())
				{
					if(Freq.getInfoTextView().length()==0)
					{
						Freq.SetInfoTextView(""+tempFreq);
					}
					Freq.getInfoTextView().clearFocus();
					Freq.setBackgroundResource(R.drawable.item_bg_bor);

					if(Band_ll.getVisibility() != View.GONE){
    					Band.requestFocus();
    				}
    				else if(Symbolrate_ll.getVisibility() != View.GONE){
    					Symbolrate.getInfoTextView().requestFocus();
    					//Symbolrate.getInfoTextView().setSelection(Symbolrate.getInfoTextView().length());
    					Symbolrate.setBackgroundResource(R.drawable.ui_enable_css);
    				}
    				else{
    					TpNo.requestFocus();
    				}
    					
					return true;
				}
				else if(Symbolrate.getInfoTextView().isFocused())
				{
					if(Symbolrate.getInfoTextView().length()==0)
					{
						Symbolrate.SetInfoTextView(""+tempSymbolrate);
					}
					Symbolrate.getInfoTextView().clearFocus();
					Symbolrate.setBackgroundResource(R.drawable.item_bg_bor);

					if(Band_ll.getVisibility() != View.GONE)
    					Band.requestFocus();
    				else	
    					TpNo.requestFocus();
    					
					return true;
				}
			    else if(Band.isFocused())
				{	
					TpNo.requestFocus();
					return true;
				}
				
				break;
			default:
				break;
			}
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	ArrayList<Integer> SelAreaList = new ArrayList();
	ArrayList<Integer> SelTpList = new ArrayList();

	private void ScanAction() {
		Intent in = new Intent();

		SelAreaList.clear();
		SelAreaList.add(system_type);
		
		SelTpList.clear();
		SelTpList.add(currentTpIndex);
		
	    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    in.setClass(Ter_ManualSearchActivity.this, ChannelSearchActivity.class);
	    in.putExtra("system_type", system_type);
	    in.putExtra("search_type", MW.SEARCH_TYPE_MANUAL);
	    in.putIntegerArrayListExtra("search_region_index", SelAreaList);
	    in.putIntegerArrayListExtra("search_tp_index", SelTpList);
	    
	    startActivity(in);
	}
	
	private void FillTpNoItem()
	{
		//tpno
		ArrayList<String> items1 = new ArrayList<String>();
		items1.clear();
		int tp_count = MW.db_ter_get_tp_count(system_type);
		for(int i = 0;i<tp_count;i++)
		{
			if(MW.db_ter_get_tp_info(system_type, -1, i,tpInfo) == MW.RET_OK)
			{
				items1.add(""+tpInfo.channel_number);
			}
		}
		TpNo.initView(R.string.TransponderNo, items1, currentTpIndex, new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id)
			{
				if(currentTpIndex != position){
					currentTpIndex = position;
					updateFreqandBand();

					LockCurrentTP();
				}
			}
			
		},new Common_CallBack()
		{
			@Override
			public void DoCallBackEvent()
			{
				final ArrayList<String> items = new ArrayList<String>();
				items.clear();

				int tp_count = MW.db_ter_get_tp_count(system_type);
				for(int i = 0;i<tp_count;i++)
				{
					if(MW.db_ter_get_tp_info(system_type, -1, i,tpInfo) == MW.RET_OK)
					{
					    if(system_type == MW.SYSTEM_TYPE_DVB_T)
    						items.add("CH - "+tpInfo.channel_number +  "  " + tpInfo.frq + " KHz"+"  "+tpInfo.bandwidth+"  MHz");
					}
				}
				if(MW.db_ter_get_tp_info(system_type, -1, currentTpIndex,tpInfo) == MW.RET_OK)
				{
					
				}
				TpNo.UpdateInfotext(items);
			}
		});
	}
	
	private void FillBandItem()
	{
		ArrayList<String> items = new ArrayList<String>();
		items.clear();
		{
			items.add(""+ 6+"  MHz" );
			items.add(""+ 7+"  MHz" );
			items.add(""+ 8+"  MHz" );
		}
		Band.initView(R.string.Band, items, tpInfo.bandwidth-6, new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
			{
				tpInfo.bandwidth = position+6;
				Log.e(TAG,"band tpInfo.tp_index = "+tpInfo.tp_index);
				Log.e(TAG,"band tpInfo.channel_number = "+tpInfo.channel_number);
				Log.e(TAG,"band tpInfo.frq = "+tpInfo.frq);
				Log.e(TAG,"band tpInfo.bandwidth = "+tpInfo.bandwidth);
				
				MW.db_ter_set_tp_info(tpInfo);

				LockCurrentTP();

				fSaveAreaTpFlag= true;
			}
		});
	}
	
	private int tempFreq = 0;
	private void FillFreqItem()
	{
		String items = null;
		items=(""+ tpInfo.frq);
		tempFreq = tpInfo.frq;
		Freq.initView(R.string.Frequency, items,"KHz", new TextWatcher()
		{
			@Override
			public void afterTextChanged(Editable v)
			{
				int value = 0;
				{
					if(v.length() > 0)
						value = Integer.valueOf(Freq.getInfoTextView().getText().toString()).intValue();
					else
						value = tpInfo.frq;
				}
				tpInfo.frq = value;
				
				MW.db_ter_set_tp_info(tpInfo);
				LockCurrentTP();			
				fSaveAreaTpFlag = true;
			}
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
		});
	}

	private int tempSymbolrate = 0;
	private void FillSymbolrateItem()
	{
		String items = null;
		items=(""+ tpInfo.bandwidth);
		tempSymbolrate = tpInfo.bandwidth;
		Symbolrate.initView(R.string.Symbolrate, items,"kSps", new TextWatcher()
		{
			@Override
			public void afterTextChanged(Editable v)
			{
				int value = 0;
				{
					if(v.length() > 0)
						value = Integer.valueOf(Symbolrate.getInfoTextView().getText().toString()).intValue();
					else
						value = tpInfo.bandwidth;
				}
				tpInfo.bandwidth = value;
				
				MW.db_ter_set_tp_info(tpInfo);
				LockCurrentTP();			
				fSaveAreaTpFlag = true;
			}
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
		});
	}
	
	private void RefreshAllData()
	{
		RefreshView(false);
	}
	
	private void RefreshView(boolean bResetTpIndex)
	{
		if(bResetTpIndex)
			currentTpIndex = 0;
		FillTpNoItem();
		updateFreqandBand();
	}
	
	private void updateFreqandBand()
	{
		if(MW.db_ter_get_tp_info(system_type, -1, currentTpIndex,tpInfo) == MW.RET_OK)
		{
   			Band.getInfoTextView().setText(tpInfo.bandwidth+"  MHz");
    		
			Freq.getInfoTextView().setText(tpInfo.frq+"");
		}
		else
		{
			Log.e(TAG,"db_ter_get_tp_info failed");
		}

		if(system_type == MW.SYSTEM_TYPE_DVB_T)
			FillBandItem();
		
		FillFreqItem();
	}
	
	private void initData()
	{
		if(MW.db_ter_get_current_tp_info(system_type, tpInfo) == MW.RET_OK)
		{
			currentTpIndex = 	tpInfo.tp_index;
		}
		else
		{
			currentTpIndex = 	0;
		}
		
		mwMsgHandler = new MWmessageHandler(looper);
	}
	
	private void initView()
	{
		TpNo = (ComboLayout) findViewById(R.id.TpNo);
		Band = (ComboLayout) findViewById(R.id.Band);
		Freq = (ComboEditText) findViewById(R.id.Freq);
		Symbolrate = (ComboEditText) findViewById(R.id.Symbolrate);
		
		Band_ll = (LinearLayout) findViewById(R.id.Band_item);
		Symbolrate_ll = (LinearLayout) findViewById(R.id.Symbolrate_item);
		
		prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
		prg_signalStrength.setMax(100);
		prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
		prg_signalQuality.setMax(100);
		prg_signalStrength.setProgress(0);
		prg_signalQuality.setProgress(0);

		prg_signalStrength_unlock = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S_unlock);
		prg_signalStrength_unlock.setMax(100);
		prg_signalQuality_unlock = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q_unlock);
		prg_signalQuality_unlock.setMax(100);
		prg_signalStrength_unlock.setProgress(0);
		prg_signalQuality_unlock.setProgress(0);

		txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
		txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);
		txv_signalStrength.setText("0%");
		txv_signalQuality.setText("0%");

	    Symbolrate_ll.setVisibility(View.GONE);

		TpNo.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus) {
					TpNo.getTitleTextView().setTextColor(getResources().getColor(R.color.select_text_color));
					TpNo.getInfoTextView().setTextColor(getResources().getColor(R.color.select_text_color));
				}
				else{
					TpNo.getTitleTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
					TpNo.getInfoTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
				}
			}
		});
		Band.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus) {
					Band.getTitleTextView().setTextColor(getResources().getColor(R.color.select_text_color));
					Band.getInfoTextView().setTextColor(getResources().getColor(R.color.select_text_color));
				}
				else{
					Band.getTitleTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
					Band.getInfoTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
				}
			}
		});
		Freq.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus) {
					Freq.getTitleTextView().setTextColor(getResources().getColor(R.color.select_text_color));
					Freq.getInfoTextView().setTextColor(getResources().getColor(R.color.select_text_color));
				}
				else{
					Freq.getTitleTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
					Freq.getInfoTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
				}
			}
		});
		Symbolrate.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean hasFocus) {
				if (hasFocus) {
					Symbolrate.getTitleTextView().setTextColor(getResources().getColor(R.color.select_text_color));
					Symbolrate.getInfoTextView().setTextColor(getResources().getColor(R.color.select_text_color));
				}
				else{
					Symbolrate.getTitleTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
					Symbolrate.getInfoTextView().setTextColor(getResources().getColor(R.color.unselect_text_color));
				}
			}
		});

		TpNo.requestFocus();
	}
	

	
	private void LockCurrentTP()
	{
		MW.tuner_ter_lock_tp(MW.MAIN_TUNER_INDEX, system_type, currentTpIndex, false);
	}
	
	class MWmessageHandler extends Handler 
	{
        public MWmessageHandler(Looper looper) {
            super(looper);
        }
        
        @Override
        public void handleMessage(Message msg) {
        	int event_type = (msg.what >> 16) & 0xFFFF;
			int sub_event_type = msg.what & 0xFFFF;
			
			switch(event_type){
				case MW.EVENT_TYPE_TUNER_STATUS:
				{
		        	mw_data.tuner_signal_status paramsInfo = (mw_data.tuner_signal_status)msg.obj;
					
					if(paramsInfo != null){
//						Log.e(TAG, " mwEventCallback : MW.EVENT_TYPE_TUNER_STATUS : " + paramsInfo.locked + " " + 
//							 paramsInfo.error + " " + sub_event_type + " " + paramsInfo.strength + " " + paramsInfo.quality);	
						
						if(paramsInfo.locked){
							prg_signalStrength.setVisibility(View.VISIBLE);
							prg_signalQuality.setVisibility(View.VISIBLE);
							prg_signalStrength_unlock.setVisibility(View.GONE);
							prg_signalQuality_unlock.setVisibility(View.GONE);
							prg_signalStrength.setProgress(paramsInfo.strength);
							prg_signalQuality.setProgress(paramsInfo.quality);

						}
						else{
							prg_signalStrength.setVisibility(View.GONE);
							prg_signalQuality.setVisibility(View.GONE);
							prg_signalStrength_unlock.setVisibility(View.VISIBLE);
							prg_signalQuality_unlock.setVisibility(View.VISIBLE);
							prg_signalStrength_unlock.setProgress(paramsInfo.strength);
							prg_signalQuality_unlock.setProgress(paramsInfo.quality);
						}

						if(paramsInfo.error){
							txv_signalStrength.setText("I2C");
							txv_signalQuality.setText("Error");
						}
						else{
							txv_signalStrength.setText(paramsInfo.strength+"%");
							txv_signalQuality.setText(paramsInfo.quality+"%");
						}
					}
				}
				break;
	
				default:
					break;
			}
        }
    }	
}

