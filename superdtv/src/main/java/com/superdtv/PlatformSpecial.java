package com.superdtv;

import android.content.Context;
import android.util.Log;

import java.io.File;
import android.os.IBinder;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import android.os.storage.*;
import android.content.Intent;

import android.content.pm.PackageInstaller;
import android.os.Environment;
import android.os.Bundle;
import android.os.RemoteException;
import java.io.FileFilter;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import android.content.IntentSender;
	
import android.content.IntentFilter;
import android.content.BroadcastReceiver; 
import android.media.AudioManager;

public class PlatformSpecial {
	private static final String TAG = "PlatformSpecial";	

	public static final String ACTION_HDMI_PLUGGED = "android.intent.action.HDMI_PLUGGED";

    public static void getStorageDevices(Context context, ArrayList<StorageDevice.DeviceItem> deviceList)
    {
        File dir;
        int usb_dev_count = 0;
        deviceList.clear();

        StorageManager storageManager = (StorageManager)context.getSystemService(Context.STORAGE_SERVICE);
        try {
            Method getVolumes = StorageManager.class.getDeclaredMethod("getVolumes");
            List<Object> getVolumeInfo = (List<Object>) getVolumes.invoke(storageManager);
            long total = 0L, used = 0L;
            for (Object obj : getVolumeInfo) {
                Field getType = obj.getClass().getField("type");
                int type = getType.getInt(obj);

                //Log.d(TAG, "type: " + type);

                if (type == 1) {//TYPE_PRIVATE
                }
                else if (type == 0) {//TYPE_PUBLIC
                    //External Storage
                    Method isMountedReadable = obj.getClass().getDeclaredMethod("isMountedReadable");
                    boolean readable = (boolean) isMountedReadable.invoke(obj);
                    if (readable) {
                        StorageDevice.DeviceItem item = new StorageDevice.DeviceItem();
                        if(item!= null) {
                            Method file = obj.getClass().getDeclaredMethod("getPath");
                            Object objFile = file.invoke(obj);
                            Method getAbsolutePath = objFile.getClass().getDeclaredMethod("getAbsolutePath");

                            Method diskInfo = obj.getClass().getDeclaredMethod("getDisk");
                            Object objDiskInfo = diskInfo.invoke(obj);
                            Method usb = objDiskInfo.getClass().getDeclaredMethod("isUsb");
                            boolean isUsb = (boolean)usb.invoke(objDiskInfo);

                            item.Path = (String)getAbsolutePath.invoke(objFile);

                            if(isUsb){
                                item.bIsSdcard = false;
                                item.deviceIndex = usb_dev_count++;
                            }
                            else{
                                item.bIsSdcard = true;
                                item.deviceIndex = 0;
                            }

                            deviceList.add(item);
                        }
                    }
                }
                else if (type == 2) {//TYPE_EMULATED
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }	


	private static Context mContext;
	public static void init(Context context)
	{
		mContext = context;
		updateVolume(context);
		registerVolumeChangeReceiver();
	}
	
	public static void deinit()
	{
		unregisterVolumeChangeReceiver();
	}
	
	private static void registerVolumeChangeReceiver() {		
        final IntentFilter filter = new IntentFilter();
        filter.addAction(VOLUME_CHANGED_ACTION);
        filter.addAction(STREAM_MUTE_CHANGED_ACTION);
        mContext.registerReceiver(mVolumeReceiver, filter);		
	}

	private static void unregisterVolumeChangeReceiver() {
        mContext.unregisterReceiver(mVolumeReceiver);		
	}

    private static final BroadcastReceiver mVolumeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleVolumeChange(context, intent);
        }
    };

	private static void updateVolume(Context context)
	{
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//		Log.e(TAG, "updateVolume = " + currentVolume + " / " + maxVolume);
		MW.global_set_int_config(MW.GLOBAL_CONFIG_SET_AUDIO_VOLUME, currentVolume*100/maxVolume);	
	}

	private static final String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    private static final String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";
    private static final String STREAM_MUTE_CHANGED_ACTION = "android.media.STREAM_MUTE_CHANGED_ACTION";

    private static void handleVolumeChange(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action) {
            case VOLUME_CHANGED_ACTION: {
                int streamType = intent.getIntExtra(EXTRA_VOLUME_STREAM_TYPE, -1);
                if (streamType != AudioManager.STREAM_MUSIC) {
                    return;
                }
//                int index = intent.getIntExtra(AudioManager.EXTRA_VOLUME_STREAM_VALUE, 0);
//                Log.e(TAG, "handleVolumeChange : mCurrentIndex = " + index);
				updateVolume(context);
                break;
            }
            case STREAM_MUTE_CHANGED_ACTION: {
                int streamType = intent.getIntExtra(EXTRA_VOLUME_STREAM_TYPE, -1);
                if (streamType != AudioManager.STREAM_MUSIC) {
                    return;
                }
//                Log.e(TAG, "handleVolumeChange : STREAM_MUTE_CHANGED_ACTION");
                updateVolume(context);
                break;
            }
            default:
                Log.e(TAG, "handleVolumeChange : Unrecognized intent: " + intent);
                return;
        }
    }	
 }

