package com.superdtv;

import android.content.Context;
import android.graphics.Rect;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.view.KeyEvent;
import android.util.Log;

//EditText which suppresses IME show up.
public class DigitsEditText extends EditText 
{
	private final static String TAG = "DigitsEditText";
	public DigitsEditText(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		//setInputType(getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		setShowSoftInputOnFocus(false);
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) 
	{
		super.onFocusChanged(focused, direction, previouslyFocusedRect);
		final InputMethodManager imm = ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE));
		if (imm != null && imm.isActive(this)) 
		{
			imm.hideSoftInputFromWindow(getApplicationWindowToken(), 0);
		}

		if(focused)
		    this.setSelection(length());
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		final boolean ret = super.onTouchEvent(event);
		// Must be done after super.onTouchEvent()
		final InputMethodManager imm = ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE));
		if (imm != null && imm.isActive(this)) 
		{
			imm.hideSoftInputFromWindow(getApplicationWindowToken(), 0);
		}
		return ret;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		Log.d(TAG, " onKeyDown "+keyCode);
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_DPAD_LEFT:
				if (getSelectionStart() > 0)
				{
					getText().delete(getSelectionStart()-1, getSelectionStart());
					return true;
				}
				else
				{
					break;
				}

			case KeyEvent.KEYCODE_DPAD_CENTER:
				if(hasSelection())
				{
					setSelection(length());
				}
				else
				{
					selectAll();
				}				
				return true;

			case KeyEvent.KEYCODE_0:
				if (event.getRepeatCount() == 0)
				{
					break;
				}
				else
				{
					//long press
					return true;
				}	

			case KeyEvent.KEYCODE_DPAD_UP:
				this.setSelection(0);
				break;
				
			case KeyEvent.KEYCODE_DPAD_DOWN:
				this.setSelection(length());
				break;
				
		}
		
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) 
	{
		Log.d(TAG, " onKeyUp "+keyCode);
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_DPAD_CENTER:
				return true;
		}
		
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void sendAccessibilityEventUnchecked(AccessibilityEvent event) 
	{	
		if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED) 
		{
			// Since we're replacing the text every time we add or remove a
			// character, only read the difference. (issue 5337550)
			final int added = event.getAddedCount();
			final int removed = event.getRemovedCount();
			final int length = event.getBeforeText().length();
			if (added > removed)
			{
				event.setRemovedCount(0);
				event.setAddedCount(1);
				event.setFromIndex(length);
			}
			else if (removed > added) 
			{
				event.setRemovedCount(1);
				event.setAddedCount(0);
				event.setFromIndex(length - 1);
			} 
			else 
			{
				return;
			}
		}
		else if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_FOCUSED) 
		{
			// The parent EditText class lets tts read "edit box" when this View has a focus, which
			// confuses users on app launch (issue 5275935).
			return;
		}
		super.sendAccessibilityEventUnchecked(event);
	}
	
	public void setDigitsText(CharSequence text)
	{
		super.setText(text);
		this.setSelection(length());
	}
}
