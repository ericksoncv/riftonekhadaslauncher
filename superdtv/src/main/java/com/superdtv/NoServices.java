package com.superdtv;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.superdtv.StorageDevice.DeviceItem;

import java.util.ArrayList;
import java.util.List;

public class NoServices extends DtvBaseActivity {
    private static final String TAG = "DtvMainActivity";


    private void noServiceLayout() {

        setContentView(R.layout.teste);

        findViewById(R.id.manual).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in_ant = new Intent();
                // in_ant.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in_ant.setClass(getBaseContext(), Ter_ManualSearchActivity.class);
                in_ant.putExtra("system_type", MW.SYSTEM_TYPE_DVB_T);

                startActivity(in_ant);
                finish();

            }
        });

        findViewById(R.id.auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ArrayList<Integer> SelAreaList = new ArrayList();
                ArrayList<Integer> SelTpList = new ArrayList();

                Intent in = new Intent();

                SelAreaList.clear();
                SelAreaList.add(MW.SYSTEM_TYPE_DVB_T);
                SelTpList.clear();
                SelTpList.add(0);

                in.setClass(getBaseContext(), ChannelSearchActivity.class);
                // in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("system_type", MW.SYSTEM_TYPE_DVB_T);
                in.putExtra("search_type", MW.SEARCH_TYPE_AUTO);
                in.putIntegerArrayListExtra("search_region_index", SelAreaList);
                in.putIntegerArrayListExtra("search_tp_index", SelTpList);

                startActivity(in);


                finish();
            }
        });


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        noServiceLayout();

        //();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
