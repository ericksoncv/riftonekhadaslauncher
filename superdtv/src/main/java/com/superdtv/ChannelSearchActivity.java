package com.superdtv;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ChannelSearchActivity extends DtvBaseActivity {
    private static final String TAG = "ChannelSearchActivity";
    private static final int LIST_MAX_CHANNEL_COUNT_SHOW = 5;

    TextView txv_signalStrength, txv_signalQuality, txv_progress, txv_status;
    ProgressBar prg_Proress, prg_signalStrength, prg_signalQuality;
    ProgressBar prg_Proress_unlock, prg_signalStrength_unlock, prg_signalQuality_unlock;

    List<TextView> txv_tvChannels = new ArrayList<TextView>();
    List<TextView> txv_radioChannels = new ArrayList<TextView>();

    int system_type;
    int search_type;

    private Handler mwMsgHandler;

    boolean bUserExit = false;

    SearchStatusDia searchStatusDig = null;
    private DyWTEDia dywteDig = null;

    private static final int MSG_BACK = 1;

    private boolean bOKKey = false;

    private class DyWTEDia extends Dialog {
        private LayoutInflater mInflater = null;
        private Context mContext = null;
        View mLayoutView = null;
        Button mBtnStart = null;
        Button mBtnCancel = null;
        TextView mTitle = null;
        TextView mContent = null;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.common_alert_dialog);
            mInflater = LayoutInflater.from(mContext);
            mLayoutView = mInflater.inflate(R.layout.common_alert_dialog, null);
            mBtnStart = (Button) findViewById(R.id.dialog_button_ok);
            mBtnCancel = (Button) findViewById(R.id.dialog_button_cancel);
            mTitle = (TextView) findViewById(R.id.deldia_title);
            mContent = (TextView) findViewById(R.id.delete_content);

            mTitle.setText(R.string.tips);
            mContent.setText(R.string.dywte);
            mBtnCancel.requestFocus();

            mBtnCancel.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bUserExit = false;
                    DyWTEDia.this.dismiss();
                }
            });

            mBtnStart.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    {
                        bUserExit = true;
                        DyWTEDia.this.dismiss();
                        MW.search_ter_stop(system_type);//send quit search message to middleware & wait for search end message
                    }
                }
            });
        }

        public DyWTEDia(Context context, int theme) {
            super(context, theme);
            mContext = context;
        }

    }

    private void ShowExitDialog() {
        dywteDig = new DyWTEDia(this, R.style.MyDialog);
        dywteDig.show();
        DtvMainActivity.setWindow_Attributes(dywteDig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    ShowExitDialog();
                    break;
                default:
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class MWmessageHandler extends Handler {

        int search_sat_count = 1;

        public MWmessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            int event_type = (msg.what >> 16) & 0xFFFF;
            int sub_event_type = msg.what & 0xFFFF;

            switch (event_type) {
                case MW.EVENT_TYPE_SEARCH_STATUS: {
                    switch (sub_event_type) {
                        case MW.SEARCH_STATUS_SEARCH_START: {
                            RelativeLayout loadingLayout = (RelativeLayout) findViewById(R.id.RelativeLayout_Loading);
                            loadingLayout.setVisibility(View.INVISIBLE);

                            Log.d(TAG, " mwEventCallback : MW.SEARCH_STATUS_SEARCH_START : " + event_type + " " + sub_event_type);
                        }
                        break;

                        case MW.SEARCH_STATUS_UPDATE_CHANNEL_LIST: {
                            mw_data.search_status_update_channel_list paramsInfo = (mw_data.search_status_update_channel_list) msg.obj;

                            Log.d(TAG, " mwEventCallback : MW.SEARCH_STATUS_UPDATE_CHANNEL_LIST : " + event_type + " " + sub_event_type);

                            if (paramsInfo != null) {
                                int i, channel_number, flag_hd, flag_4k, flag_scrambled;

                                for (i = 0; i < paramsInfo.tv_channels_name.length; i++) {
                                    flag_scrambled = (paramsInfo.tv_channels_number[i] >> 31) & 0x00000001;
                                    flag_hd = (paramsInfo.tv_channels_number[i] >> 30) & 0x00000001;
                                    flag_4k = (paramsInfo.tv_channels_number[i] >> 29) & 0x00000001;
                                    channel_number = paramsInfo.tv_channels_number[i] & 0x000fffff;

                                    ((TextView) txv_tvChannels.get(i)).setText(String.format("%03d.   ", channel_number) + paramsInfo.tv_channels_name[i]);
                                }

                                for (i = 0; i < paramsInfo.radio_channels_name.length; i++) {
                                    flag_scrambled = (paramsInfo.radio_channels_number[i] >> 31) & 0x00000001;
                                    flag_hd = (paramsInfo.radio_channels_number[i] >> 30) & 0x00000001;
                                    flag_4k = (paramsInfo.radio_channels_number[i] >> 29) & 0x00000001;
                                    channel_number = paramsInfo.radio_channels_number[i] & 0x000fffff;

                                    ((TextView) txv_radioChannels.get(i)).setText(String.format("%03d.   ", channel_number) + paramsInfo.radio_channels_name[i]);
                                }
                            }
                        }
                        break;

                        case MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO: {
                            mw_data.search_status_dvbt_params paramsInfo = (mw_data.search_status_dvbt_params) msg.obj;

                            Log.d(TAG, " mwEventCallback : MW.SEARCH_STATUS_UPDATE_DVB_T_PARAMS_INFO : " + event_type + " " + sub_event_type);

                            if (paramsInfo != null) {
                                if (paramsInfo.system_type == MW.SYSTEM_TYPE_DVB_T) {
                                    txv_status.setText("   CH - " + paramsInfo.channel_number +
                                            String.format("  / %d KHz / %d MHz  %d/%d  %s", paramsInfo.frq,
                                                    paramsInfo.bandwidth, paramsInfo.search_tp_pos + 1, paramsInfo.search_tp_count, (paramsInfo.front_end_type == MW.FRONT_END_TYPE_DVBT_2 ? ("(" + "PLP :" + paramsInfo.dvb_t2_plp_id + ")") : ""))
                                    );
                                }

                                System.out.println("paramsInfo.progress:" + paramsInfo.progress);
                                System.out.println("paramsInfo.progress_total:" + paramsInfo.progress_total);
                                System.out.println("paramsInfo.search_area_pos:" + paramsInfo.search_area_pos);
                                System.out.println("paramsInfo.search_area_count:" + paramsInfo.search_area_count);
                                if (search_type == MW.SEARCH_TYPE_AUTO) {
                                    prg_Proress.setVisibility(View.GONE);
                                    prg_Proress_unlock.setVisibility(View.VISIBLE);
                                    if (paramsInfo.search_area_count > 1) {
                                        prg_Proress.setProgress(paramsInfo.progress_total);
                                        prg_Proress_unlock.setProgress(paramsInfo.progress_total);
                                        txv_progress.setText(paramsInfo.progress_total + "%");
                                    } else {
                                        prg_Proress.setProgress(paramsInfo.progress);
                                        prg_Proress_unlock.setProgress(paramsInfo.progress);
                                        txv_progress.setText(paramsInfo.progress + "%");

                                    }
                                }

                                if (search_type == MW.SEARCH_TYPE_MANUAL) {
                                    prg_Proress.setVisibility(View.GONE);
                                    prg_Proress_unlock.setVisibility(View.VISIBLE);

                                    if (paramsInfo.search_tp_count > 1) {
                                        prg_Proress.setProgress(paramsInfo.progress_total);
                                        prg_Proress_unlock.setProgress(paramsInfo.progress_total);
                                        txv_progress.setText(paramsInfo.progress_total + "%");
                                    } else {
                                        prg_Proress.setProgress(paramsInfo.progress);
                                        prg_Proress_unlock.setProgress(paramsInfo.progress);
                                        txv_progress.setText(paramsInfo.progress + "%");
                                    }
                                }
                            }
                        }
                        break;

                        case MW.SEARCH_STATUS_SAVE_DATA_START: {
                            Log.d(TAG, " mwEventCallback : MW.SEARCH_STATUS_SAVE_DATA_START : " + event_type + " " + sub_event_type);

//							txv_status.setText("Saving Data....");
                            SearchStatus(sub_event_type);
                        }
                        break;

                        case MW.SEARCH_STATUS_SAVE_DATA_END: {
                            Log.d(TAG, " mwEventCallback : MW.SEARCH_STATUS_SAVE_DATA_END : " + event_type + " " + sub_event_type);
                            SearchStatus(sub_event_type);
//							txv_status.setText("Save Data End....");
                        }
                        break;

                        case MW.SEARCH_STATUS_SEARCH_END: {
                            mw_data.search_status_result paramsInfo = (mw_data.search_status_result) msg.obj;

                            Log.d(TAG, " mwEventCallback : MW.SEARCH_STATUS_SEARCH_END : " + event_type + " " + sub_event_type);

                            Toast.makeText(getBaseContext(), "End Searck", Toast.LENGTH_LONG).show();

                            prg_Proress.setProgress(100);
                            prg_Proress_unlock.setProgress(100);
                            txv_progress.setText(100 + "%");

                            if (paramsInfo != null) {
                                if (!bUserExit) {
                                    if (searchStatusDig == null) {
                                        searchStatusDig = new SearchStatusDia(ChannelSearchActivity.this, R.style.MyDialog);
                                        searchStatusDig.show();
                                        WindowManager.LayoutParams lp = searchStatusDig.getWindow().getAttributes();
                                        lp.dimAmount = 0.0f;
                                        searchStatusDig.getWindow().setAttributes(lp);
                                        searchStatusDig.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                                    }
                                    searchStatusDig.setMyTiTle(getResources().getString(R.string.search_result));
                                    searchStatusDig.showBtn(true);
                                    if ((paramsInfo.tv_service_count == 0) && (paramsInfo.radio_service_count == 0)) {
                                        searchStatusDig.setMyText(getResources().getString(R.string.no_program));
                                    } else {
                                        String result = (String.format(getResources().getString(R.string.TV) + "  :  %d       " + getResources().getString(R.string.Radio) + "  :  %d", paramsInfo.tv_service_count, paramsInfo.radio_service_count));
                                        searchStatusDig.setMyText(result);
                                    }
                                    autoClose(3000, paramsInfo);
                                } else {
                                    //ChannelSearchActivity.this.finish();
									Intent intent = new Intent(getBaseContext(), DtvMainActivity.class);
									startActivity(intent);
									finish();
                                }
                            }

                        }
                        break;

                        default:
                            break;
                    }
                }
                break;

                case MW.EVENT_TYPE_TUNER_STATUS: {
                    mw_data.tuner_signal_status paramsInfo = (mw_data.tuner_signal_status) msg.obj;

                    if (paramsInfo != null) {
                        Log.d(TAG, " mwEventCallback : MW.EVENT_TYPE_TUNER_STATUS : " + paramsInfo.locked + " " +
                                paramsInfo.error + " " + sub_event_type + " " + paramsInfo.strength + " " + paramsInfo.quality);

                        if (paramsInfo.locked) {
                            if (search_type == MW.SEARCH_TYPE_MANUAL) {
                                prg_Proress.setVisibility(View.VISIBLE);
                                prg_Proress_unlock.setVisibility(View.GONE);
                                prg_Proress.setProgress(0);
                            }
                            prg_signalStrength.setVisibility(View.VISIBLE);
                            prg_signalQuality.setVisibility(View.VISIBLE);
                            prg_signalStrength_unlock.setVisibility(View.GONE);
                            prg_signalQuality_unlock.setVisibility(View.GONE);
                            prg_signalStrength.setProgress(paramsInfo.strength);
                            prg_signalQuality.setProgress(paramsInfo.quality);
                        } else {
                            if (search_type == MW.SEARCH_TYPE_MANUAL) {
                                prg_Proress.setVisibility(View.GONE);
                                prg_Proress_unlock.setVisibility(View.VISIBLE);
                                prg_Proress_unlock.setProgress(0);
                            }

                            prg_signalStrength.setVisibility(View.GONE);
                            prg_signalQuality.setVisibility(View.GONE);
                            prg_signalStrength_unlock.setVisibility(View.VISIBLE);
                            prg_signalQuality_unlock.setVisibility(View.VISIBLE);
                            prg_signalStrength_unlock.setProgress(paramsInfo.strength);
                            prg_signalQuality_unlock.setProgress(paramsInfo.quality);
                        }

                        if (paramsInfo.error) {
                            txv_signalStrength.setText("I2C");
                            txv_signalQuality.setText("Error");
                        } else {
                            txv_signalStrength.setText(paramsInfo.strength + "%");
                            txv_signalQuality.setText(paramsInfo.quality + "%");
                        }
                    }
                }
                break;

                default:
                    break;
            }
        }

    }

    public class SearchStatusDia extends Dialog {
        private LayoutInflater mInflater = null;
        private Context mContext = null;
        View mLayoutView = null;

        TextView mTextView = null;
        TextView deldia_title = null;

        RelativeLayout rl_btn = null;
        Button dialog_button_ok = null;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.channel_search_activity_search_status);
            mTextView = (TextView) findViewById(R.id.delete_content);
            deldia_title = (TextView) findViewById(R.id.deldia_title);

            deldia_title.setText(getResources().getString(R.string.save));
            String ellipsis = getResources().getString(R.string.ellipsis);
            mTextView.setText(getResources().getString(R.string.savedata) + ellipsis);

            {
                rl_btn = (RelativeLayout) findViewById(R.id.rl_btn);
                dialog_button_ok = (Button) findViewById(R.id.dialog_button_ok);
                dialog_button_ok.requestFocus();
            }
            dialog_button_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bOKKey = true;
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_BACK), 100);
                }
            });
            if (dywteDig != null)
                dywteDig.dismiss();
        }

        public void showBtn(boolean isShow) {
            if (isShow)
                rl_btn.setVisibility(View.VISIBLE);
            else
                rl_btn.setVisibility(View.GONE);
        }

        public void setMyText(String string) {
            mTextView.setText(string);
        }

        public void setMyTiTle(String string) {
            deldia_title.setText(string);
        }

        public SearchStatusDia(Context context, int theme) {
            super(context, theme);
            mContext = context;
        }

        public SearchStatusDia(Context context) {
            super(context);
            mContext = context;
        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event) {
            return false;
        }

    }

    private void SearchStatus(int Event) {
        if (searchStatusDig == null) {
            searchStatusDig = new SearchStatusDia(ChannelSearchActivity.this, R.style.MyDialog);
            searchStatusDig.show();
            WindowManager.LayoutParams lp = searchStatusDig.getWindow().getAttributes();
            lp.dimAmount = 0.0f;
            searchStatusDig.getWindow().setAttributes(lp);
            searchStatusDig.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        {
            if (Event == MW.SEARCH_STATUS_SAVE_DATA_START) {
                String ellipsis = getResources().getString(R.string.ellipsis);
                searchStatusDig.setMyText(getResources().getString(R.string.savedata) + ellipsis);
            } else if (Event == MW.SEARCH_STATUS_SAVE_DATA_END) {
                searchStatusDig.setMyText(getResources().getString(R.string.saveend));
            }
        }

    }

    public void autoClose(int time, mw_data.search_status_result result) {
        Timer timer = new Timer();
        timer.schedule(new MyTask(result), time);
    }

    class MyTask extends TimerTask {
        private mw_data.search_status_result result;

        public MyTask(mw_data.search_status_result result) {
            this.result = result;
        }

        @Override
        public void run() {
            if (bOKKey == false) {
                mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_BACK), 100);
            }
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_BACK:
                    System.out.println("MSG_BACK");
                    if ((searchStatusDig != null) && (searchStatusDig.isShowing())) {
                        searchStatusDig.dismiss();
                        searchStatusDig = null;
                    }
                    //ChannelSearchActivity.this.finish();
                    Intent intent = new Intent(getBaseContext(), DtvMainActivity.class);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.channel_search_activity);

        system_type = getIntent().getIntExtra("system_type", 0);
        search_type = getIntent().getIntExtra("search_type", 0);

        if (system_type == MW.SYSTEM_TYPE_DVB_T) {
            ((TextView) findViewById(R.id.TextView_SystemType)).setText(getResources().getString(R.string.dvb_t_t2));
        }

        txv_status = (TextView) findViewById(R.id.TextView_Status);

        prg_signalStrength = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S);
        prg_signalStrength.setMax(100);
        prg_signalStrength.setProgress(0);


        prg_signalQuality = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q);
        prg_signalQuality.setMax(100);
        prg_signalQuality.setProgress(0);

        prg_signalStrength_unlock = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_S_unlock);
        prg_signalStrength_unlock.setMax(100);
        prg_signalStrength_unlock.setProgress(0);


        prg_signalQuality_unlock = (ProgressBar) findViewById(R.id.ProgressBar_TunerStatus_Q_unlock);
        prg_signalQuality_unlock.setMax(100);
        prg_signalQuality_unlock.setProgress(0);


        prg_Proress = (ProgressBar) findViewById(R.id.ProgressBar_Progress);
        prg_Proress.setMax(100);
        prg_Proress.setProgress(0);

        prg_Proress_unlock = (ProgressBar) findViewById(R.id.ProgressBar_Progress_unlock);
        prg_Proress_unlock.setMax(100);
        prg_Proress_unlock.setProgress(0);


        txv_signalStrength = (TextView) findViewById(R.id.TextView_TunerStatus_S_Percent);
        txv_signalQuality = (TextView) findViewById(R.id.TextView_TunerStatus_Q_Percent);

        txv_progress = (TextView) findViewById(R.id.TextView_Progress_Percent);

        txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_1));
        txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_2));
        txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_3));
        txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_4));
        txv_tvChannels.add((TextView) findViewById(R.id.TextView_tvChannel_5));

        txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_1));
        txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_2));
        txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_3));
        txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_4));
        txv_radioChannels.add((TextView) findViewById(R.id.TextView_radioChannel_5));

        mwMsgHandler = new MWmessageHandler(looper);

        bUserExit = false;
        Log.d(TAG, " onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, " onStart");
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, " onNewIntent");
    }

    @Override
    protected void onPause() {
        super.onPause();

        MW.search_ter_stop(system_type);

        if (searchStatusDig != null)
            searchStatusDig.dismiss();

        enableMwMessageCallback(null);

        Log.d(TAG, " onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, " onRestart");
    }

    @Override
    protected void onResume() {
        ArrayList<Integer> search_region_list = null;
        ArrayList<Integer> search_tp_list = null;

        Log.d(TAG, " onResume start");

        super.onResume();
        enableMwMessageCallback(mwMsgHandler);

        search_region_list = new ArrayList<Integer>();
        search_region_list = getIntent().getIntegerArrayListExtra("search_region_index");

        search_tp_list = new ArrayList<Integer>();
        search_tp_list = getIntent().getIntegerArrayListExtra("search_tp_index");

        RelativeLayout loadingLayout = (RelativeLayout) findViewById(R.id.RelativeLayout_Loading);
        loadingLayout.setVisibility(View.VISIBLE);

        System.out.println("Region Count : " + search_region_list.size());
        int[] search_region_index_list = new int[search_region_list.size()];
        for (int i = 0; i < search_region_list.size(); i++) {
            search_region_index_list[i] = search_region_list.get(i).intValue();
            System.out.println("search_region_index_list: " + search_region_index_list[i]);
        }

        System.out.println("Tp Count : " + search_tp_list.size());
        int[] search_tp_index_list = new int[search_tp_list.size()];
        for (int i = 0; i < search_tp_list.size(); i++) {
            search_tp_index_list[i] = search_tp_list.get(i).intValue();
            System.out.println("search_tp_index_list: " + search_tp_index_list[i]);
        }

        System.out.println("search_type: " + search_type);

        MW.search_ter_start(search_type, search_region_index_list, search_tp_index_list, LIST_MAX_CHANNEL_COUNT_SHOW);

        if (search_type == MW.SEARCH_TYPE_MANUAL) {
            prg_Proress.setProgress(0);
            prg_Proress_unlock.setProgress(0);
            txv_progress.setText("0%");
            txv_signalStrength.setText("0%");
            txv_signalQuality.setText("0%");
        } else {
            prg_Proress.setProgress(0);
            prg_Proress_unlock.setProgress(0);
            txv_progress.setText("0%");
            txv_signalStrength.setText("0%");
            txv_signalQuality.setText("0%");
        }

        Log.d(TAG, " onResume end");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, " onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, " onDestroy");

    }

}
