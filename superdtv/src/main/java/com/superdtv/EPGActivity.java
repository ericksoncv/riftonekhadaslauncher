package com.superdtv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.text.format.DateFormat;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.AdapterView;
import android.util.Log;
import android.os.UserHandle;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import java.text.SimpleDateFormat;
import android.provider.Settings;

public class EPGActivity extends DtvBaseActivity{

	private static final String TAG = "EPGActivity";

	public static final int CHANGE_CHANNEL_DELAYED_TIME = 50;

	private Handler localMsgHandler;
    private Handler mwMsgHandler;

    private CustomListView channel_list;
	private EPGChannelListAdapter epgChannelListAdapter;
    private TextView epgchannel_type;
	private int mFirstVisibleItem = 0;
	private int mVisibleItemCount = 8;

//    private TextView current_program;
	private TextView current_date_time;
	private TextView epg_event_text;

	private CustomListView epgInfo_list;
	private EpgInfoAdapter epgInfo_Adapter;
	private int mEpgFirstVisibleItem =0;
	private int mEpgVisibleItemCount=8;

	private int epgInfo_item_pos=0;
	private LinearLayout progress_ll;


	private RadioGroup weekdays;
	private TextView pos0;
	private TextView pos1;
	private TextView pos2;
	private TextView pos3;
	private TextView pos4;
	private TextView pos5;
	private TextView pos6;
	TextView[] text = {
			pos0,
			pos1,
			pos2,
			pos3,
			pos4,
			pos5,
			pos6,
	};
	
	int[] weekStringID = {
			R.string.monday,
			R.string.tuesday,
			R.string.wednesday,
			R.string.thursday,
			R.string.friday,
			R.string.saturday,
			R.string.sunday,
	};
	private LinearLayout epg_bg;//t


	//bottom tips
	private LinearLayout detail_ll;
	private LinearLayout ll_tvradio_list;



	private TextView play_status;

	private int current_service_type;
	private int current_service_index;
	private int tmp_current_service_index;
	private mw_data.service serviceInfo = new mw_data.service();
	private mw_data.epg_extended_event epgExtendedEvent = new mw_data.epg_extended_event();
	private mw_data.epg_schedule_event epgScheduleEvent = new mw_data.epg_schedule_event();

 	private int epgScheduleDayoffset = 0;
 	private int epgScheduleEventCount = 0;
	private int[] epgScheduleEventIds;

	private boolean disableUpdate = false;
	private mw_data.date_time dateTime = new mw_data.date_time();
	private mw_data.date date = new mw_data.date();
	private mw_data.date dateTmp = new mw_data.date();
	private mw_data.date datePre = new mw_data.date();//for previous date .

	private mw_data.date_time tmp_dateTime = new mw_data.date_time();
	private static int tmp_durationtime = 0;

	private boolean fisttime_enter = true;

	public static final int LOCAL_MSG_CLOSE_PROGRESSBAR = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.epg_activity);
		initMessagehandle();

		initView();
		initData();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		MW.ts_player_stop_play();
	
		localMsgHandler.removeMessages(LOCAL_MSG_CLOSE_PROGRESSBAR);
		progress_ll.setVisibility(View.INVISIBLE);
		videoView.setVisibility(View.INVISIBLE);

		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		videoView.setVisibility(View.VISIBLE);

		if(MW.db_get_current_service_info(serviceInfo) == MW.RET_OK){
			current_service_type = serviceInfo.service_type;
			current_service_index = serviceInfo.service_index;
			tmp_current_service_index = serviceInfo.service_index;
		}

		enableMwMessageCallback(mwMsgHandler);
		MW.register_event_type(MW.EVENT_TYPE_DATE_TIME, true);
		MW.register_event_type(MW.EVENT_TYPE_EPG_SCHEDULE_UPDATE, true);

		MW.epg_set_schedule_event_day_offset(epgScheduleDayoffset);

		if(MW.get_date_time(dateTime) == MW.RET_OK){

/*			current_date_time.setText(String.format("%04d-%02d-%02d %02d:%02d:%02d", dateTime.year, dateTime.month, dateTime.day,
									dateTime.hour, dateTime.minute, dateTime.second)+SETTINGS.getWeekOfDate(EPGActivity.this, date));
*/	
			current_date_time.setText(Common.getDateTimeAdjustSystem(this, dateTime, true));

		}

		MW.ts_player_play_current(true, true);
		
		if(epgInfo_Adapter != null)
			epgInfo_Adapter.notifyDataSetChanged();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
			case KeyEvent.KEYCODE_GUIDE:
			case KeyEvent.KEYCODE_MENU:
				gotoPlayWindow();					
				return true;							

			default:
				break;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		default:
			break;
		}
		return super.onKeyUp(keyCode, event);
	}
	
	public void gotoPlayWindow() {
		Intent intent = new Intent(this, DtvMainActivity.class);
		//intent.setComponent(new ComponentName("com.superdtv", "com.superdtv.DtvMainActivity"));
		//intent.setComponent(new ComponentName("com.superdtv", "com.superdtv.DtvMainActivity"));
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void initData() {
		if(MW.db_get_current_service_info(serviceInfo) == MW.RET_OK){
			current_service_type = serviceInfo.service_type;
			current_service_index = serviceInfo.service_index;
		}
	}

	private void initEpgInfoProgress() {
//		loading = (ProgressBar) findViewById(R.id.loading);
	}

	private void UpdateDetailView(int pos)
	{
		if(epgScheduleEventIds!=null)
		{
			if(MW.epg_get_schedule_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[pos], serviceInfo, date, epgScheduleEvent) == MW.RET_OK){
				//epg_event_text.setText(epgScheduleEvent.event_text);

				if(epgScheduleEvent.has_extended_event_info){
					detail_ll.setVisibility(View.VISIBLE);
				}else{
					detail_ll.setVisibility(View.INVISIBLE);
				}
			}
			else{
				//epg_event_text.setText("");
				detail_ll.setVisibility(View.INVISIBLE);
			}
		}else
		{
			detail_ll.setVisibility(View.INVISIBLE);
		}
	}

	private void initProgressBar() {
		progress_ll = (LinearLayout) findViewById(R.id.progress_ll);
	}

	private boolean bNeedShow = true;
	private void ShowProgressBar(int delayinS)
	{
		localMsgHandler.removeMessages(LOCAL_MSG_CLOSE_PROGRESSBAR);
		if(delayinS > 0)
		{	
			progress_ll.setVisibility(View.INVISIBLE);
			localMsgHandler.sendMessageDelayed(localMsgHandler.obtainMessage(LOCAL_MSG_CLOSE_PROGRESSBAR), delayinS*1000);
		}
	}

	private void HideProgressBar()
	{
		progress_ll.setVisibility(View.INVISIBLE);
	}

	private void initView() {
		detail_ll = (LinearLayout) findViewById(R.id.detail_ll);
		initProgressBar();
		initRadioGroupView();
		initVideoView();
		initCurrentProgramView();
		initEpgInfoList();
		initChannelList();
		initPlayStatusView();
		initEpgInfoProgress();

		UpdateDetailView(epgInfo_item_pos);

		setEpgBackgroundAlpha(240);
	}

	private void setEpgBackgroundAlpha(int transparency) {
		ll_tvradio_list = (LinearLayout) findViewById(R.id.ll_tvradio_list);
		epg_bg = (LinearLayout) findViewById(R.id.epg_bg);
		epg_bg.getBackground().setAlpha(transparency);
		ll_tvradio_list.getBackground().setAlpha(240);
		epgInfo_list.getBackground().setAlpha(50);
	}

	private void initCurrentProgramView() {
//		current_program = (TextView) findViewById(R.id.tv_back_current_channel);
		current_date_time = (TextView) findViewById(R.id.current_time);
		//epg_event_text = (TextView) findViewById(R.id.epg_info_content);

	}

	public class DetailDia extends Dialog {
		private LayoutInflater mInflater = null ;
		private Context mContext = null ;
		View mLayoutView = null ;
		Button mBtnStart = null ;
		Button mBtnCancel = null ;
		TextView txtTitle = null;
		TextView mcontent = null;
		
		String content = null;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.epg_activity_epg_detail_dialog) ;
			mInflater = LayoutInflater.from(mContext) ;
			mLayoutView = mInflater.inflate(R.layout.epg_activity_epg_detail_dialog, null) ;
			txtTitle = (TextView) findViewById(R.id.deldia_title);
			mcontent = (TextView) findViewById(R.id.delete_content);
		
			txtTitle.setText(getResources().getString(R.string.Detail));
			mcontent.setGravity(Gravity.LEFT);
			mcontent.setSingleLine(false);
			mcontent.setText(content+"");

			mcontent.setFocusable(true);
			mcontent.requestFocus();
			mcontent.setMovementMethod(ScrollingMovementMethod.getInstance());
		}
			@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
				if(event.getAction() == KeyEvent.ACTION_DOWN)
				{
					switch (keyCode) {
					case KeyEvent.KEYCODE_INFO:
					{
						DetailDia.this.dismiss();
						return true;
					}

					default:
						break;
					}
				}
				
			return super.onKeyDown(keyCode, event);
		}
		public DetailDia(Context context, int theme) {
			super(context, theme);
			mContext = context ;
		}
		public DetailDia(Context context, int theme,String string) {
			super(context, theme);
			mContext = context ;
			content = string;
		}
		public DetailDia(Context context) {
			super(context);
			mContext = context ;
		}
		
		@Override
		public boolean onKeyUp(int keyCode, KeyEvent event) {
			return super.onKeyUp(keyCode, event);
		}
	}
	
	public static void setWindow_Attributes(Dialog dialog)
	{
		WindowManager.LayoutParams lp=dialog.getWindow().getAttributes();

		lp.dimAmount=0.0f;
		
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); 
	}
	
	private void showDialog(String detail_txt) {	
		Dialog DetailDialog = new DetailDia(this,R.style.MyDialog,detail_txt) ;
		DetailDialog.show() ;
		setWindow_Attributes(DetailDialog);
    }

	private void RefreshEpgInfoItemBack(int currentPos){

	}

	private void initEpgInfoList() {
		epgInfo_list = (CustomListView) findViewById(R.id.tv_back_videos);
		epgInfo_Adapter = new EpgInfoAdapter(this);
		epgInfo_list.setAdapter(epgInfo_Adapter);
		epgInfo_list.setVisibleItemCount(9);
		
		epgInfo_list.setCustomScrollListener(new OnScrollListener(){
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				mEpgFirstVisibleItem = firstVisibleItem;
				mEpgVisibleItemCount = visibleItemCount;
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}
		});

		epgInfo_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View v, int position,long id)
			{
				epgInfo_item_pos = position;
				UpdateDetailView(epgInfo_item_pos);

				if(epgScheduleEventIds != null)
					UpdateScheduleExtendedEpgInfo(epgScheduleEventIds[position]);
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});

		epgInfo_list.setCustomKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(event.getAction() ==  KeyEvent.ACTION_DOWN)
				{
					switch (keyCode) {
						case KeyEvent.KEYCODE_DPAD_CENTER:
							return true;

						case KeyEvent.KEYCODE_DPAD_LEFT:
						{
							if(((RadioButton)weekdays.getChildAt(0)).isChecked())
							{
								channel_list.requestFocus();
								channel_list.setSelectionFromTop(current_service_index,mFirstVisibleItem);
								epgChannelListAdapter.notifyDataSetChanged();

								{
									if(channel_list.getSelectedView() != null)
										channel_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
									int currentPos = epgInfo_list.getSelectedItemPosition();
									if(currentPos>=0)
									{
										if(epgInfo_list.getSelectedView()!=null)
											epgInfo_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
									}
								}

							}else
							{
//								epgInfo_item_pos =0;
								UpdateScheduleEpgInfo_PrevDay();
								{
//									RefreshEpgInfoItemBack(epgInfo_item_pos);
//									epgInfo_list.setSelection(epgInfo_item_pos);
								}

								if(epgInfo_list.getCount() == 0){
									if(channel_list.getSelectedView() != null)
										channel_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
//									System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!");
								}else{
									if(channel_list.getSelectedView() != null)
										channel_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
									epgInfo_list.requestFocus();
									epgInfo_list.setCustomSelection(epgInfo_item_pos);
//									System.out.println("XXXXXX");
								}
								return true;
							}


							return true;
						}



						case KeyEvent.KEYCODE_DPAD_RIGHT:
						{
//							epgInfo_item_pos =0;

							UpdateScheduleEpgInfo_NextDay();
							{
//								RefreshEpgInfoItemBack(epgInfo_item_pos);
//								epgInfo_list.setSelection(epgInfo_item_pos);

							}
							if(epgInfo_list.getCount() == 0){
								if(channel_list.getSelectedView() != null)
									channel_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
//								System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!");
							}else{
								if(channel_list.getSelectedView() != null)
									channel_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
								epgInfo_list.requestFocus();
								epgInfo_list.setCustomSelection(epgInfo_item_pos);
//								System.out.println("XXXXXX");
							}
							return true;
						}

						case KeyEvent.KEYCODE_INFO:
						{
							if(epgScheduleEventIds!=null)
							{
								if(MW.epg_get_schedule_extended_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[epgInfo_item_pos], serviceInfo, date, epgExtendedEvent) == MW.RET_OK)
								{
									if(MW.epg_get_schedule_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[epgInfo_item_pos], serviceInfo, date, epgScheduleEvent) == MW.RET_OK)
									{
										if(((epgExtendedEvent.event_text!=null)&&(epgExtendedEvent.event_text.length()>0)) || epgScheduleEvent.has_extended_event_info)
										{
											if((epgScheduleEvent.event_text!=null)&&(epgScheduleEvent.event_text.length()>0))
												showDialog(epgScheduleEvent.event_text + epgExtendedEvent.event_text);
											else
												showDialog(epgExtendedEvent.event_text);
										}
									}
								}
							}
							return true;
						}

						default:
							break;
					}
				}
				else
				{
					switch(keyCode)
					{
						case KeyEvent.KEYCODE_DPAD_CENTER:
						case KeyEvent.KEYCODE_DEL:
							//disable system Action
							return true;
					}
				}
				return false;

			}
		});
	}
	
	private void RefreshRadioGroupView()
	{
		int curDay = 0;

		if(MW.get_date(0, null, date) == MW.RET_OK){
			curDay = date.weekNo;
		}

		for(int i=0 ; i<7 ; i++){
			((RadioButton)weekdays.getChildAt(i)).setBackgroundResource(R.drawable.tv_back_friday_selector);/*(weekID[curDay++]);*/
			
			if(curDay >= 7)
				curDay = 0;

			if(MW.get_date(i, date, dateTmp) == MW.RET_OK){
				((RadioButton)weekdays.getChildAt(i)).setText(Common.getDateAdjustSystem(this, dateTmp,false));
				text[i].setText(getResources().getString(weekStringID[dateTmp.weekNo]));
			}
		}

		((RadioButton)weekdays.getChildAt(epgScheduleDayoffset)).setChecked(true);
	}
	private void initRadioGroupView() {
		weekdays = (RadioGroup) findViewById(R.id.tv_back_weekdays);
		{
			pos0 = (TextView) findViewById(R.id.pos0);
			pos1 = (TextView) findViewById(R.id.pos1);
			pos2 = (TextView) findViewById(R.id.pos2);
			pos3 = (TextView) findViewById(R.id.pos3);
			pos4 = (TextView) findViewById(R.id.pos4);
			pos5 = (TextView) findViewById(R.id.pos5);
			pos6 = (TextView) findViewById(R.id.pos6);
		}
		
		int curDay = 0;

		if(MW.get_date(0, null, date) == MW.RET_OK){
			curDay = date.weekNo;
		}

		text[0] = pos0;
		text[1] = pos1;
		text[2] = pos2;
		text[3] = pos3;
		text[4] = pos4;
		text[5] = pos5;
		text[6] = pos6;

		for(int i=0 ; i<7 ; i++){
			((RadioButton)weekdays.getChildAt(i)).setBackgroundResource(R.drawable.tv_back_friday_selector);/*(weekID[curDay++]);*/
			
			if(curDay >= 7)
				curDay = 0;

			if(MW.get_date(i, date, dateTmp) == MW.RET_OK){
				((RadioButton)weekdays.getChildAt(i)).setText(Common.getDateAdjustSystem(this, dateTmp,false));
				text[i].setText(getResources().getString(weekStringID[dateTmp.weekNo]));
			}
		}

		((RadioButton)weekdays.getChildAt(epgScheduleDayoffset)).setChecked(true);


		weekdays.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {

			}
		});
	}

	private void initChannelList() {
		channel_list = (CustomListView) findViewById(R.id.tv_back_channles);
		channel_list.requestFocus();
		channel_list.setCustomScrollListener(new OnScrollListener(){
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				mFirstVisibleItem = firstVisibleItem;
				mVisibleItemCount = visibleItemCount;
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}
		});

		channel_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View v, int position,long id)
			{
				if(MW.db_get_service_info(current_service_type, position, serviceInfo) == MW.RET_OK){
//					current_program.setText(serviceInfo.service_name);
					if(!fisttime_enter)
					{
						int Ret = MW.ts_player_play(current_service_type, position, CHANGE_CHANNEL_DELAYED_TIME, true);
					}else
					{
						fisttime_enter = false;
					}
					current_service_index = serviceInfo.service_index;
					epgChannelListAdapter.notifyDataSetChanged();

					bNeedShow = true;
					UpdateScheduleEpgInfo(true);
					
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});


		channel_list.setCustomKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(event.getAction() ==  KeyEvent.ACTION_DOWN)
				{
//					System.out.println("EPG channel_list keycode == "+keyCode);
					switch (keyCode) {

						case KeyEvent.KEYCODE_DPAD_LEFT:
						{
							if(MW.db_get_current_service_info(serviceInfo)== MW.RET_OK)
							{
								epgInfo_list.requestFocus();
								epgInfo_list.setSelectionFromTop(epgInfo_item_pos, mEpgFirstVisibleItem);
//								RefreshEpgInfoItemBack(epgInfo_item_pos);

								int pos = epgInfo_list.getSelectedItemPosition();
								if(pos>=0)
								{
									if(channel_list.getSelectedView() != null)
										channel_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
								}
								else
								{
//									epgInfo_item_pos =0;
									UpdateScheduleEpgInfo_PrevDay();
									{
//										RefreshEpgInfoItemBack(epgInfo_item_pos);
//										epgInfo_list.setSelection(epgInfo_item_pos);
									}

									if(epgInfo_list.getCount() == 0){
										if(channel_list.getSelectedView() != null)
											channel_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
//										System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!");
									}else{
										if(channel_list.getSelectedView() != null)
											channel_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
										epgInfo_list.requestFocus();
										epgInfo_list.setSelectionFromTop(epgInfo_item_pos, mEpgFirstVisibleItem);
//										System.out.println("XXXXXX");
									}
								}
							}

							epgInfo_Adapter.notifyDataSetChanged();
							return true;
						}

						case KeyEvent.KEYCODE_DPAD_RIGHT:
						{
							if(MW.db_get_current_service_info(serviceInfo)== MW.RET_OK)
							{
								epgInfo_list.requestFocus();
								epgInfo_list.setSelectionFromTop(epgInfo_item_pos, mEpgFirstVisibleItem);
								epgInfo_Adapter.notifyDataSetChanged();
//								RefreshEpgInfoItemBack(epgInfo_item_pos);

								int pos = epgInfo_list.getSelectedItemPosition();
								if(pos>=0)
								{
									if(channel_list.getSelectedView() != null)
										channel_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
//									System.out.println("epgInfo_item_pos   " +epgInfo_item_pos+  "mEpgFirstVisibleItem"+mEpgFirstVisibleItem);

								}
								else{
//									epgInfo_item_pos =0;
									UpdateScheduleEpgInfo_NextDay();
									{
//										RefreshEpgInfoItemBack(epgInfo_item_pos);
//										epgInfo_list.setSelection(epgInfo_item_pos);
//										System.out.println("epgInfo_item_pos   " +epgInfo_item_pos+  "mEpgFirstVisibleItem"+mEpgFirstVisibleItem);
//										System.out.println("OOOOOOOO");

									}

									if(epgInfo_list.getCount() == 0){
										if(channel_list.getSelectedView() != null)
											channel_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);//ui_enable_css
//										System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!");
									}else{
										if(channel_list.getSelectedView() != null)
											channel_list.getSelectedView().setBackgroundResource(R.drawable.ui_disable_css);
										epgInfo_list.requestFocus();
//										epgInfo_list.setSelection(epgInfo_item_pos);
//										System.out.println("epgInfo_item_pos   " +epgInfo_item_pos+  "mEpgFirstVisibleItem"+mEpgFirstVisibleItem);
										epgInfo_list.setSelectionFromTop(epgInfo_item_pos, mEpgFirstVisibleItem);
//										System.out.println("XXXXXX");
									}
								}
							}

							return true;
						}

						case KeyEvent.KEYCODE_INFO:
						{
							if(epgScheduleEventIds!=null)
							{
								if(MW.epg_get_schedule_extended_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[epgInfo_item_pos], serviceInfo, date, epgExtendedEvent) == MW.RET_OK)
								{
									if(MW.epg_get_schedule_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[epgInfo_item_pos], serviceInfo, date, epgScheduleEvent) == MW.RET_OK)
									{
										if(((epgExtendedEvent.event_text!=null)&&(epgExtendedEvent.event_text.length()>0)) || epgScheduleEvent.has_extended_event_info)
										{
											if((epgScheduleEvent.event_text!=null)&&(epgScheduleEvent.event_text.length()>0))
												showDialog(epgScheduleEvent.event_text + epgExtendedEvent.event_text);
											else
												showDialog(epgExtendedEvent.event_text);
										}
									}
								}
							}
							return true;
						}
						
						
						case KeyEvent.KEYCODE_DPAD_UP:
						case KeyEvent.KEYCODE_DPAD_DOWN:
						case KeyEvent.KEYCODE_PAGE_UP:
						case KeyEvent.KEYCODE_PAGE_DOWN:
						{
							epgInfo_item_pos = 0;
							epgInfo_list.setSelection(epgInfo_item_pos);
							
							int currentPos = channel_list.getSelectedItemPosition();
							if(channel_list.getSelectedView() != null)
								channel_list.getSelectedView().setBackgroundResource(R.drawable.selector_item);
							
							break;
						}

						case KeyEvent.KEYCODE_3:
							switch(MW.ts_player_play_switch_tv_radio(true))
							{
								case MW.RET_OK:
									RefreshShowEpgChannelList();
									epgInfo_item_pos = 0;
									epgInfo_list.setCustomSelection(epgInfo_item_pos);
									{
										RefreshEpgInfoItemBack(epgInfo_item_pos);
									}
									break;

								case MW.RET_NO_AUDIO_CHANNEL:
//									ShowTmpPlayStatus(getString(R.string.NO_AUDIO_CHANNEL));
									Common.makeText(EPGActivity.this, R.string.NO_AUDIO_CHANNEL, Toast.LENGTH_SHORT);
									break;

								case MW.RET_NO_VIDEO_CHANNEL:
//									ShowTmpPlayStatus(getString(R.string.NO_VIDEO_CHANNEL));
									Common.makeText(EPGActivity.this, R.string.NO_VIDEO_CHANNEL, Toast.LENGTH_SHORT);
									break;

								case MW.RET_NO_CHANNEL:
//									ShowTmpPlayStatus(getString(R.string.NO_CHANNEL));
									Common.makeText(EPGActivity.this, R.string.NO_CHANNEL, Toast.LENGTH_SHORT);
									break;
								default:
									break;
							}
							return true;

						case KeyEvent.KEYCODE_DPAD_CENTER:
						{
							gotoPlayWindow();
							return true;
						}

						default:
							break;
					}
				}
				else
				{
					switch(keyCode)
					{
						case KeyEvent.KEYCODE_DPAD_CENTER:
						case KeyEvent.KEYCODE_DEL:
							//disable system Action
							return true;
					}
				}
				return false;
			}
		});

		epgChannelListAdapter = new EPGChannelListAdapter(this);
		channel_list.setAdapter(epgChannelListAdapter);
		channel_list.setVisibleItemCount(7);
		
		channel_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		epgchannel_type = (TextView) findViewById(R.id.channeltype_txt);

		RefreshShowEpgChannelList();

	}

	private void RefreshShowEpgChannelList() {
		if(MW.db_get_current_service_info(serviceInfo) == MW.RET_OK){
			current_service_type = serviceInfo.service_type;
			current_service_index = serviceInfo.service_index;

			if(current_service_type == MW.TV_CH)
				epgchannel_type.setText(R.string.TvChannelList);
			else
				epgchannel_type.setText(R.string.RadioChannelList);

			mFirstVisibleItem = current_service_index;
			channel_list.setFocusableInTouchMode(true);
			channel_list.requestFocus();
			channel_list.requestFocusFromTouch();
			channel_list.setSelection(mFirstVisibleItem);
			
			epgChannelListAdapter.notifyDataSetChanged();
			
			bNeedShow = true;
			UpdateScheduleEpgInfo(true);
    	}
	}

	private void UpdateScheduleExtendedEpgInfo(int event_id)
	{
/*
		Log.e("EPG", "");
		Log.e("EPG", "UpdateScheduleExtendedEpgInfo");

		if(MW.db_get_current_service_info(serviceInfo) == MW.RET_OK){
			if(MW.epg_get_schedule_extended_event_by_event_id(epgScheduleDayoffset, event_id, serviceInfo, date, epgExtendedEvent) == MW.RET_OK){
				Log.e("EPG", "detail text = " + epgExtendedEvent.event_text);
				Log.e("EPG", "item count = " + epgExtendedEvent.items_text.length);

			}
		}

		Log.e("EPG", "");
*/
	}

	private void UpdateScheduleEpgInfo(boolean forceSelectFirstItem)
	{
		int i, ret;
//		Log.e("EPG", "UpdateScheduleEpgInfo");

		//epg_event_text.setText("");
		ret = MW.db_get_current_service_info(serviceInfo);
		
		if(ret == MW.RET_OK){
			epgScheduleEventCount = MW.epg_lock_get_schedule_event_count(epgScheduleDayoffset, serviceInfo, date);

			if(epgScheduleEventCount > 0){

				epgScheduleEventIds = new int[epgScheduleEventCount];

//				Log.e("EPG", "epg schedule count = " + epgScheduleEventCount + " dayoffset = " + epgScheduleDayoffset);

				for(i=0; i<epgScheduleEventCount ; i++){
					epgScheduleEventIds[i] = MW.epg_lock_get_schedule_event_id_by_index(epgScheduleDayoffset, i, serviceInfo, date);
				}

				if(forceSelectFirstItem || epgInfo_item_pos >= epgScheduleEventCount)
				{
					epgInfo_item_pos =0;
					epgInfo_list.setCustomSelection(epgInfo_item_pos);
				}
				
				UpdateScheduleExtendedEpgInfo(epgScheduleEventIds[epgInfo_item_pos]);
				UpdateDetailView(epgInfo_item_pos);
				HideProgressBar();
			}
			else{
				epgScheduleEventIds = null;
				
				detail_ll.setVisibility(View.INVISIBLE);
				if(bNeedShow)
				{
					bNeedShow = false;
					ShowProgressBar(30);
				}
//				Log.e("EPG", "no epg schedule information");
			}

			MW.epg_unlock_schedule_event();


/*
			for(i=0; i<epgScheduleEventCount ; i++){
				if(MW.epg_get_schedule_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[i], serviceInfo, date, epgScheduleEvent) == MW.RET_OK){
					Log.e("EPG", "  " + String.format("%04d-%02d-%02d %02d:%02d - %04d-%02d-%02d %02d:%02d  ", epgScheduleEvent.start_year, epgScheduleEvent.start_month,
						epgScheduleEvent.start_day,	epgScheduleEvent.start_hour, epgScheduleEvent.start_minute, epgScheduleEvent.end_year, epgScheduleEvent.end_month,
							epgScheduleEvent.end_day, epgScheduleEvent.end_hour, epgScheduleEvent.end_minute) + epgScheduleEvent.event_name + " ## " + (epgScheduleEvent.event_text.length() > 0 ? "text" : "") + " ## " + (epgScheduleEvent.has_extended_event_info ? "detail" : ""));

					if(epgScheduleEvent.playing_status == MW.EPG_EVENT_PLAYING_STATUS_NEW)
						Log.e("EPG", "  " + String.format("playing status : new, bookable"));
					else if(epgScheduleEvent.playing_status == MW.EPG_EVENT_PLAYING_STATUS_PLAYING)
						Log.e("EPG", "  " + String.format("playing status : playing"));
					else
						Log.e("EPG", "  " + String.format("playing status : expired"));

					Log.e("EPG", "  " + String.format("event text : ") + epgScheduleEvent.event_text);
				}
			}
*/


			epgInfo_Adapter.notifyDataSetChanged();
		}
	}

	private void UpdateScheduleEpgInfo_NextDay()
	{
//		System.out.println("green key epgInfo_item_pos = "+epgInfo_item_pos);

//		epgInfo_list.setSelection(epgInfo_item_pos);
		epgInfo_list.setSelectionFromTop(epgInfo_item_pos, mEpgFirstVisibleItem);


		epgScheduleDayoffset ++;
		if(epgScheduleDayoffset >= 7)
			epgScheduleDayoffset = 0;

		((RadioButton)weekdays.getChildAt(epgScheduleDayoffset)).setChecked(true);

		MW.epg_set_schedule_event_day_offset(epgScheduleDayoffset);
		
		bNeedShow = true;

		UpdateScheduleEpgInfo(true);
	}

	private void UpdateScheduleEpgInfo_PrevDay()
	{
//		System.out.println("red key epgInfo_item_pos = "+epgInfo_item_pos);
		epgInfo_list.setCustomSelection(epgInfo_item_pos);

		epgScheduleDayoffset --;
		if(epgScheduleDayoffset < 0)
			epgScheduleDayoffset = 6;

		((RadioButton)weekdays.getChildAt(epgScheduleDayoffset)).setChecked(true);

		MW.epg_set_schedule_event_day_offset(epgScheduleDayoffset);

		bNeedShow = true;
		
		UpdateScheduleEpgInfo(true);
	}

	private class EPGChannelListAdapter extends BaseAdapter{

		private  Context mContext;
		private LayoutInflater mInflater;

		public EPGChannelListAdapter(Context context) {
			super();
			this.mContext = context;
			mInflater = LayoutInflater.from(context);

		}

		@Override
		public int getCount() {
			return MW.db_get_service_count(current_service_type);
		}

		@Override
		public Object getItem(int pos) {
			return pos;
		}

		@Override
		public long getItemId(int pos) {
			return pos;
		}

		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			ViewHolder localViewHolder;
//			System.out.println("getView  pos = "+pos);
			if(convertView == null)
			{
				convertView = mInflater.inflate(R.layout.epg_activity_channel_list_item, null);
				localViewHolder = new ViewHolder();
				localViewHolder.channel_num = (TextView) convertView.findViewById(R.id.channellist_num_txt);
				localViewHolder.channel_name = (TextView) convertView.findViewById(R.id.channellist_name_txt);
				localViewHolder.channel_quality_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_sharp_img);
				localViewHolder.fav_icon = (ImageView) convertView.findViewById(R.id.live_channellist_item_fav_img);
				convertView.setTag(localViewHolder);
			}
			else
			{
				localViewHolder = (ViewHolder) convertView.getTag();
			}

			Drawable drawable = mContext.getResources().getDrawable(R.drawable.selector_item);
			convertView.setBackgroundDrawable(drawable);

//			System.out.println("current_service_type : "+ current_service_type);
//			System.out.println("pos : "+ pos);


			if(MW.db_get_service_info(current_service_type, pos, serviceInfo) == MW.RET_OK){
   				localViewHolder.channel_num.setText(Integer.toString(serviceInfo.channel_number_display));
				localViewHolder.channel_name.setText(serviceInfo.service_name);

				if(serviceInfo.is_hd == MW.VIDEO_RESOLUTION_TYPE_HD)
					localViewHolder.channel_quality_icon.setImageResource(R.drawable.imagehd1);
				else if(serviceInfo.is_hd == MW.VIDEO_RESOLUTION_TYPE_4K)
					localViewHolder.channel_quality_icon.setImageResource(R.drawable.image4k1);
				else
					localViewHolder.channel_quality_icon.setImageResource(Color.TRANSPARENT);

				if(serviceInfo.is_scrambled)
					localViewHolder.fav_icon.setImageResource(R.drawable.coin_us_dollar);
				else
					localViewHolder.fav_icon.setImageResource(Color.TRANSPARENT);

			}

			return convertView;
		}

		class ViewHolder
		{
			TextView channel_num ;
			TextView channel_name;
			ImageView channel_quality_icon;
			ImageView fav_icon;
		}
	}


	private class EpgInfoAdapter extends BaseAdapter
	{
		private Context mcontext;
		private LayoutInflater mInflater;

		public EpgInfoAdapter(Context context) {
			super();
			this.mcontext = context;
			this.mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
//			System.out.println("epgScheduleEventCount     <<<<<<<<<<<<<<<<<<<<<<<"+epgScheduleEventCount);
				return epgScheduleEventCount;
		}

		@Override
		public Object getItem(int pos) {
			return pos;
		}

		@Override
		public long getItemId(int pos) {
			return pos;
		}

		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			ViewHolder localViewHolder;
			System.out.println("epg info pos = "+pos+"convertView = " +convertView);
			if(convertView == null)
			{
				convertView = mInflater.inflate(R.layout.epg_activity_epg_info_item, null);
				localViewHolder = new ViewHolder();
				localViewHolder.epg_time = (TextView) convertView.findViewById(R.id.tv_back_time);
				localViewHolder.epg_name = (TextView) convertView.findViewById(R.id.tv_back_name);
				localViewHolder.epg_detail = (TextView) convertView.findViewById(R.id.epg_detail_log);
				convertView.setTag(localViewHolder);
			}
			else
			{
				localViewHolder = (ViewHolder) convertView.getTag();
			}
			Drawable drawable = mcontext.getResources().getDrawable(R.drawable.selector_item);
			convertView.setBackgroundDrawable(drawable);

//			System.out.println("epg info pos = " +pos + " : "  + epgScheduleDayoffset + " : "  + epgScheduleEventIds[pos]);
//			MW.get_date(0, null, date);
//			System.out.println("epgScheduleDayoffset"+epgScheduleDayoffset);
//			System.out.println("epgScheduleEventIds[pos]"+epgScheduleEventIds[pos]+"    pos :"+pos);
//			System.out.println("date "+date.year+"_"+date.month+"_"+date.day);
//			System.out.println("serviceInfo.service_name  >>>"+serviceInfo.service_name);
			if(MW.db_get_service_info(current_service_type,current_service_index,serviceInfo) == MW.RET_OK){
//				System.out.println("11111111111111serviceInfo.service_name  >>>"+serviceInfo.service_name);

			
			if(MW.epg_get_schedule_event_by_event_id(epgScheduleDayoffset, epgScheduleEventIds[pos], serviceInfo, date, epgScheduleEvent) == MW.RET_OK){
				localViewHolder.epg_time.setText(String.format("%02d:%02d - %02d:%02d", epgScheduleEvent.start_hour, epgScheduleEvent.start_minute,
					epgScheduleEvent.end_hour, epgScheduleEvent.end_minute));

				localViewHolder.epg_name.setText(epgScheduleEvent.event_name);

			    if(((epgScheduleEvent.event_text!=null)&&(epgScheduleEvent.event_text.length()>0)) ||(epgScheduleEvent.has_extended_event_info)){
			    	localViewHolder.epg_detail.setVisibility(View.VISIBLE);
			    }
				else{
			    	localViewHolder.epg_detail.setVisibility(View.INVISIBLE);
			    }

//			    System.out.println("epgScheduleEvent.playing_status"+epgScheduleEvent.playing_status);
			    
			    if(epgScheduleEvent.playing_status == MW.EPG_EVENT_PLAYING_STATUS_NEW){
			    	convertView.setBackgroundResource(R.drawable.selector_item);
			    	
			    }
			    else if(epgScheduleEvent.playing_status == MW.EPG_EVENT_PLAYING_STATUS_PLAYING){
			    	convertView.setBackgroundResource(R.drawable.selector_item);
			    	
			    }

				//if(pos == epgInfo_item_pos && channel_list.isFocused())
			    //	convertView.setBackgroundResource(R.drawable.list_446_49_sele_expired);
			}
			else{
				System.out.println("epg info pos = " +pos+"  failed");
				localViewHolder.epg_time.setText("");
				localViewHolder.epg_name.setText("");

				localViewHolder.epg_detail.setVisibility(View.INVISIBLE);
			}
			}



			return convertView;
		}

		class ViewHolder
		{
			TextView epg_time ;
			TextView epg_name;
			TextView epg_detail;
		}
	}
	VideoView videoView = null;
	private void initVideoView() {
		videoView = (VideoView) findViewById(R.id.tv_back_video);
		videoView.setFocusable(false);
		videoView.setClickable(false);

		if(mSHCallback != null)
			videoView.getHolder().addCallback(mSHCallback);
		
		Common.videoViewSetFormat(videoView);	
	}

	private void initPlayStatusView()
	{
		play_status = (TextView) findViewById(R.id.tv_status);
		play_status.setVisibility(View.INVISIBLE);
	}
	private void ShowTmpPlayStatus(String s)
	{
		play_status.setText(s);
		play_status.setVisibility(View.INVISIBLE);//change to invisible for tvradio key down
	}

	
	class LOCALMessageHandler extends Handler {
        public LOCALMessageHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
        	int msg_type = msg.what;

			switch(msg_type){
				case LOCAL_MSG_CLOSE_PROGRESSBAR:
				{
					HideProgressBar();
					break;
				}

				default:
					break;
			}
        }
	}

	class MWmessageHandler extends Handler {
	    public MWmessageHandler(Looper looper) {
	        super(looper);
	    }
	    @Override
	    public void handleMessage(Message msg) {
        	int event_type = (msg.what >> 16) & 0xFFFF;
			int sub_event_type = msg.what & 0xFFFF;

			switch(event_type){
				case MW.EVENT_TYPE_DATE_TIME:
				{
		        	mw_data.date_time paramsInfo = (mw_data.date_time)msg.obj;

					if(paramsInfo != null){
						current_date_time.setText(Common.getDateTimeAdjustSystem(EPGActivity.this, paramsInfo, true));

				//		Log.e(TAG,String.format("%04d-%02d-%02d    %d", date.year, date.month, date.day, date.weekNo));
				//		Log.e(TAG,(String.format("%04d-%02d-%02d   ", paramsInfo.year, paramsInfo.month, paramsInfo.day)));
						
						{
							datePre.day = paramsInfo.day;
							datePre.month = paramsInfo.month;
							datePre.year = paramsInfo.year;
						}
						if(!((RadioButton)weekdays.getChildAt(0)).getText().equals(Common.getDateAdjustSystem(EPGActivity.this, datePre,false)))
						//if( date.day != paramsInfo.day || date.month != paramsInfo.month || date.year != paramsInfo.year){
						{
							//date changed, refresh epg info list

							RefreshRadioGroupView();
							bNeedShow = true;
							UpdateScheduleEpgInfo(true);
						}
						else if(dateTime.hour != paramsInfo.hour || dateTime.minute != paramsInfo.minute){
							dateTime.year = paramsInfo.year;
							dateTime.month = paramsInfo.month;
							dateTime.day = paramsInfo.day;
							dateTime.hour = paramsInfo.hour;
							dateTime.minute = paramsInfo.minute;
							dateTime.day = paramsInfo.day;
							
							UpdateScheduleEpgInfo(false);
						}
					}
				}
				break;

				case MW.EVENT_TYPE_EPG_SCHEDULE_UPDATE:
				{
					bNeedShow = true;
					UpdateScheduleEpgInfo(true);
				}
				break;
				
				default:
					break;
			}
	    }
	}

	private void initMessagehandle()
	{
	    mwMsgHandler = new MWmessageHandler(looper);
	    localMsgHandler = new LOCALMessageHandler(looper);
	}
}
