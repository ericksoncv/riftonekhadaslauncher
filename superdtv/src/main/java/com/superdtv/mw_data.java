//////////////////////////////////////////////////////////////////////////
/////
// DTV MIDDLE WARE DEFINES, PLEASE DO NOT MAKE ANY CHANGE OF THIS FILE
/////
//////////////////////////////////////////////////////////////////////////

package com.superdtv;

public class mw_data {
	public static class dvb_t_tp {
		public	int system_type;  /* read only item */
		public	int tp_index;  /* read only item */		
		
		public	int frq;
		public	int bandwidth;

		public	boolean selected;
		
		public	String channel_number;  /* read only item */	
	}

	public static class aud_info {
		public	String ISO_639_language_code;
		public	int audio_pid;
		public	int audio_stream_type;
	}		
	
	public static class ttx_info {
		public	String ISO_639_language_code;
		public	int teletext_type;
		public	int teletext_magazine_number;
		public	int teletext_page_number;
		public	int pid;
	}		
	
	public static class sub_info {
		public	String ISO_639_language_code;
		public	int subtitling_type;
		public	int composition_page_id;
		public	int ancillary_page_id;
		public  int pid;
	}		
	
	public static class service {
		public int service_index;
		
		public 	String service_name;
		public 	int ts_id;
		public	int service_id;
		public	int on_id; //used for 'source_id' when ATSC mode
		public	int region_index;//for DVB S, it's sat index, for DVB T, ISDB T, ATSC, it's area index
		public	int tp_index;
		public	int video_pid;
		public	boolean is_scrambled;
		public	boolean is_locked;
		public	boolean is_skipped;
		public	int favorite;
		public	int video_stream_type;
		public	int audio_index;
		public	int audio_channel;
		public	int subtitle_index;
		public	aud_info[] audio_info;
		public	ttx_info[] teletext_info;
		public	sub_info[] subtitle_info;
		public	int pcr_pid;
		public	int pmt_pid;
		public 	int service_type;
		public	int is_hd;
		public	int video_width;
		public	int video_height;
		public	int system_type;
		public 	int channel_number_display;
		public	int front_end_type;
		public	int dvb_profile; //valid only when system_type == SYSTEM_TYPE_DVB_T or front_end_type = FRONT_END_TYPE_DVBS_MIS
		public	int dvb_plp_id; //valid only when front_end_type == FRONT_END_TYPE_DVBT_2 or front_end_type = FRONT_END_TYPE_DVBS_MIS
		
		public	int other_defines_1;
		public	int other_defines_2;
		
		public 	String extended_service_name; //valid only when system_type == MW.SYSTEM_TYPE_ATSC
                                              //call func MW.epg_get_atsc_channel_ett will override this field value
		
		public  int major_channel_number; //valid only when system_type == MW.SYSTEM_TYPE_ATSC
		public  int minor_channel_number; //valid only when system_type == MW.SYSTEM_TYPE_ATSC
		
		public	int[] ca_system_id;
		
		public	int extend_frequency;
		public	int extend_symbolrate;

		public	int sub_area_index;//valid when system_type == TER system

		public 	String cw_key;
	} 
	
	public static class tuner_signal_status {
		public	boolean error;
		public	boolean locked;
		public	int strength;
		public	int quality;
		public	int modulation;
		public	int front_end_type;
		public	long ber;

		public	float cn;
		public	float pwr;
	
		public	int  fec;

		public	int[]  user_defined;
	}
		
	public static class date_time {
		public int year;
		public int month;
		public int day;
		public int hour;
		public int minute;
		public int second;
		public int weekNo;
	}
		
	public static class date {
		public int year;
		public int month;
		public int day;
		public int weekNo;
	}

	public static class pvr_record {
		public String service_name;
		public String event_name;

		public long file_size;
		
		public int record_year;
		public int record_month;
		public int record_day;
		public int record_hour;
		public int record_minute;
		public int record_second;

		public	int video_pid;
		public	int video_stream_type;
		public	int audio_index;
		public	int audio_channel;
		public	int subtitle_index;
		public	aud_info[] audio_info;
		public	ttx_info[] teletext_info;
		public	sub_info[] subtitle_info;
		public int service_type;
		public int system_type;
		public	int is_hd;
		public	boolean is_locked;
		public int parental_rating;
	}	

// epg		
	public static class epg_pf_event {
		public String event_name;

		public int play_progress;
		public int parental_rating;

		public int start_year;
		public int start_month;
		public int start_day;
		public int start_hour;
		public int start_minute;

		public int end_year;
		public int end_month;
		public int end_day;
		public int end_hour;
		public int end_minute;
	}

	public static class epg_schedule_event {
		public String event_name;
		public String event_text;
		
		public int event_id;
		public int playing_status;
		public int parental_rating;

		public int start_year;
		public int start_month;
		public int start_day;
		public int start_hour;
		public int start_minute;

		public int end_year;
		public int end_month;
		public int end_day;
		public int end_hour;
		public int end_minute;
		
		public int book_status;

		public boolean has_extended_event_info;
	}

	public static class epg_extended_event {
		public String event_text;

		public String[] items_descriptor;
		public String[] items_text;
	}	
// search status	
	public static class search_status_update_channel_list {
		public	String[] tv_channels_name;
		public	String[] radio_channels_name;

		public	int[] tv_channels_number;
		public	int[] radio_channels_number;
	}		

	public static class search_status_dvbt_params {
		public	int search_area_count;
		public	int search_area_pos; /* from 0 to search_area_count */
		public	int search_area_index;
		public	int search_tp_count;
		public	int search_tp_pos; /* from 0 to search_tp_count */
		
		public	String channel_number;
		public	int frq;
		public	int bandwidth;	

		public	int system_type;
		
		public	int front_end_type;
		public	int ter_profile; 
		public	int dvb_t2_plp_id; //valid only front_end_type == FRONT_END_TYPE_DVBT_2
		public	int modulation_mode;
		
		public	int progress;
		public	int progress_total;
	}	
	
	public static class search_status_result {
		public	int tv_service_count;
		public	int radio_service_count;	
	}		
} 

//////////////////////////////////////////////////////////////////////////
/////
// DTV MIDDLE WARE DEFINES, PLEASE DO NOT MAKE ANY CHANGE OF THIS FILE
/////
//////////////////////////////////////////////////////////////////////////

