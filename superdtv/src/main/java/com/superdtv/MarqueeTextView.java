package com.superdtv;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

public class MarqueeTextView extends TextView
{
  public MarqueeTextView(Context paramContext)
  {
    super(paramContext);
  }

  public MarqueeTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public boolean isFocused()
  {
    return true;
  }

  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
  {
    if (!paramBoolean)
      return;
    super.onFocusChanged(paramBoolean, paramInt, paramRect);
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    if (!paramBoolean)
      return;
    super.onWindowFocusChanged(paramBoolean);
  }
}
