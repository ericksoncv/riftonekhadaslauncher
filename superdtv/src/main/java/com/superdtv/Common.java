package com.superdtv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.text.format.DateFormat;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.AdapterView;
import android.util.Log;
import android.os.UserHandle;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import java.text.SimpleDateFormat;
import android.provider.Settings;
import java.util.Locale;

public class Common{

	private static final String TAG = "Common";

	static Toast toast;

	private static void _makeText(Context context, String resId,  int duration)
	{
		LayoutInflater inflater_toast = LayoutInflater.from(context);
		LinearLayout toast_view = (LinearLayout)inflater_toast.inflate(R.layout.toast_main,null);
		TextView toast_tv = (TextView) toast_view.findViewById(R.id.toast_text);
		toast_tv.setText(resId);
		if(toast != null)
			toast_tv.setText(resId);
		else
		{
			toast = new Toast(context);
		}
		toast.setView(toast_view);
		toast.setDuration(duration);
		toast.setGravity(Gravity.CENTER, 0, -70);
		toast.show();	
	}
	
	public static void makeText(Context context, int resId,  int duration)
	{
		_makeText(context, context.getString(resId), duration);
	}	
	
	public static void makeText(Context context, String resId,  int duration)
	{
		_makeText(context, resId, duration);
	}		

	public static void setVideoViewWindow(final VideoView videoView)
	{
		videoView.post(new Runnable() { 
		    @Override 

		    public void run() { 
				int x,y,w,h;

				int[] location = new int[2];  
				videoView.getLocationOnScreen(location);
				x = location[0];
				y = location[1];
				w = videoView.getWidth();
				h = videoView.getHeight();		
				
				MW.ts_player_set_position(x, y, w, h);
		    } 
		}); 	
	}

	public static void videoViewSetFormat(VideoView videoView)
	{
		setVideoViewWindow(videoView);
	
		videoView.getHolder().setFormat(PixelFormat.RGBA_8888);
	}

    private static String getTimeFormat(Context context) {
        String TimeFormat=" ";
       	boolean is24Hour = DateFormat.is24HourFormat(context);

		if(is24Hour)
			TimeFormat ="24";
		else
			TimeFormat ="12";
		
        return TimeFormat;
    }
	
    private static String getDateFormat(Context context, boolean needWeek) {
       String DateFormat =  Settings.System.getString(context.getContentResolver(),
                Settings.System.DATE_FORMAT);
       if(DateFormat == null)
       {
       		if(needWeek == true)
				DateFormat = "yyyy-MM-dd- EEEE";
			else
    	   		DateFormat = "yyyy-MM-dd";
       }
       return DateFormat;
    }
	
	public static String getDateTimeAdjustSystem(Context context, mw_data.date_time datetime,boolean needSecond/*for infobar*/){
		String string_datetime = "";
		String TimeFormat = "";
		
		if(datetime == null)
			return "";
		
		System.out.println("getDateFormat "+getDateFormat(context, false));
		System.out.println("getTimeFormat "+getTimeFormat(context));
		if(getTimeFormat(context).equals("24"))
		{
			if(needSecond)
				TimeFormat = "HH:mm:ss";
			else
				TimeFormat = "HH:mm";
		}
		else if (getTimeFormat(context).equals("12"))
		{	
			if(needSecond)
			{
				if((context.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("CN"))
					||context.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("TW"))
					TimeFormat = " a hh:mm:ss ";
				else
					TimeFormat = "hh:mm:ss a";
			}
			else
			{
				if((context.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("CN"))
						||context.getResources().getConfiguration().locale.getCountry().toUpperCase().equals("TW"))
					TimeFormat = "a hh:mm ";
				else
					TimeFormat = "hh:mm a";
			}
				
		}
		SimpleDateFormat   sDateFormat   =  new SimpleDateFormat(getDateFormat(context, false)+" "+TimeFormat);
		Date mDate = new Date(datetime.year-1900, datetime.month-1, datetime.day, datetime.hour, datetime.minute, datetime.second);
		string_datetime = sDateFormat.format(mDate);
		if(string_datetime == null)
			return "";
		return string_datetime;
	}
	
	public static String getDateAdjustSystem(Context context, mw_data.date date,boolean needWeek){
		String string_date = "";
		
		if(date == null)
			return "";
		
		SimpleDateFormat   sDateFormat   = new SimpleDateFormat(getDateFormat(context, needWeek));
		Date mDate = new Date(date.year-1900, date.month-1, date.day);
		string_date = sDateFormat.format(mDate);		
		
		if(string_date == null)
			return "";
		return string_date;
	}

	public static String stringFormat(String format, Object... args)
	{
		return String.format(Locale.US, format, args);
	}	

	public static void setWindow_Attributes(Dialog dialog)
	{
		WindowManager.LayoutParams lp=dialog.getWindow().getAttributes();

		lp.dimAmount=0.0f;
		
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND); 
	}	
}
